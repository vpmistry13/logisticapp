@extends('admin_section.layouts.master')

@section('content')
<style>
    .modal-dialog {
  width: 100%;
  height: 90%;
  margin: 0;
  padding: 25px;
}

.modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
.modal-body {
    height: auto;
    overflow-y: auto;
    padding-top: 2%;
    word-wrap: break-word;
}


</style>
<style>
/* The container */
.container_check {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  padding-top: 1px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container_check input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container_check:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container_check input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container_check input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container_check .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}



</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">User Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List of Truck Owner
                    <div class="pull-right"><a class="btn btn-default btn-xs" href="{{url('admin/driver/addAccount')}}">Add Truck Owner Account</a></div> 
                </div>
                
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <label class="container_check">Select All
                                <input class="form-control-sm" type="checkbox" name="select_all" id="select-all">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="driver_managment">
                        <thead>
                            <tr class="design_table">
                                <!--<th>Driver ID</th>-->
                                <!--<th>Profile Picture</th>-->
                                <th><small>Name</small></th>
                                <th><small>Company Name</small></th>
                                <th><small>Email Address</small></th>
                                <th><small>Address</small></th>
                                <th><small>Mobile Number</small></th>                                
                                <!--<th><small>Mobile (o)</small></th>-->                                
                                <th><small>Truck Type</small></th>
                                <th><small>Total Truck</small></th>
                                <th><small>CDL#</small></th>
                                <th><small>Truck Insurance</small></th>
                                <th><small>Driver’s License</small></th>
                                <!--<th>Driver Average Rating</th>-->
                                <th><small>W-9 Form</small></th>
                                <th><small>Action</small></th>
                            </tr>
                        </thead>                       
                    </table>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-12">
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#create_group"><strong>Create Group</strong></button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#add_group"><strong>Add to Group</strong></button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#send_message"><strong>Send Message</strong></button>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>

<!-- Modal -->
<div id="create_group" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Create Group</strong></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4"><label>Selected Users: </label></div>
                        <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                        <div id="selected_ids" class="col-md-12"></div>
                    </div>
                    <div class="col-lg-12 justify-content-center">
                        <div class="col-md-8"><input type="text" class="form-control" id="group_name" name="group_name" placeholder="Enter Group Name"></div>
                        <div class="col-md-4"><button type="button" id="saveGroup"  class="btn btn-primary">Save Group</button></div>
                    </div>
                </div>
            </div>
            <!--      <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>-->
        </div>

    </div>
</div>
<div id="add_group" class="modal fade" role="dialog">
  <div class="modal-dialog main">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><strong>Add to Group</strong></h4>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-lg-12">
    <div class="col-md-4"><label>Selected Users: </label></div>
    <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                  <div id="add_to_existing" class="col-md-12"></div>
              </div>
              <div class="col-lg-12 justify-content-center">
                    <div class="col-lg-12">
                            <hr>
                            <div class="col-md-4"><label>Group List: </label></div>

                            <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                            
                            <div class="col-md-4"><ul id="group_list"></ul></div>
                        </div>
                    <div class="col-md-4"><button type="button" id="add_to_existing_group"  class="btn btn-primary">Save Group</button></div>
              </div>
              <div class="models_list"></div>
              </div>
      </div>
<!--      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>
<div id="send_message" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Send Message</strong></h4>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4"><label>Selected Users: </label></div>
                        <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                        <div id="send_bulk_msg" class="col-md-12"></div>
                    </div>
                    <div class="col-lg-12">                       
                        <div class="col-md-4"><label>Group List: </label></div>
                        <div class="col-md-4"><ul id="group_list_message"></ul></div>
                    </div>
                    <div class="col-lg-12 justify-content-center">                   
                        <hr>
                        <div class="col-md-12">
                            <label>Message Subject </label>
                            <input type="text" class="form-control" name="message_subject" id="message_subject"/>
                        </div>
                    </div>
                    <div class="col-lg-12 justify-content-center">

                        <hr>
                        <div class="col-md-12">
                            <label>Message Body </label>
                            <textarea class="form-control" name="message_body" id="message_body"></textarea>
                        </div>

                        <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                    </div>
                    <div class="col-lg-12 justify-content-center">
                        <hr>
                        <div class="col-md-5">
                            <label> Notification Type: </label>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" id="notification_type" name="notification_type">
                                <option value="1">Both</option>
                                <option value="2">Email</option>
                                <option value="3">SMS</option>
                            </select>
                        </div>
                    </div>
                    <div class="models_list_sms"></div>
                    <div class="col-lg-12 justify-content-center">
                        <hr>
                        <div class="col-md-4"><button type="button" id="send_notification_bulk"  class="btn btn-primary">Send</button></div>

                    </div>

                </div>
            </div>
            <!--      <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>-->
        </div>

    </div>
</div>
<div style="margin-top: 50px;"></div>
<!-- /#page-wrapper -->
@endsection