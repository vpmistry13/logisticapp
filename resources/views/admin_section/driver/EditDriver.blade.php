@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Truck Owner Account</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="post_data_edit" name="post_data_edit" method="post" role="form">
                            {{ csrf_field() }}
                            <div class="col-lg-12">
                                <div class="row">
                                <input type="hidden" name="user_type" value="2"/>
                                <input type="hidden" name="user_id" value="{{$user_id}}"/>
                                <div class="col-md-4">
                                    <label>First Name</label>
                                    <input class="form-control" id="first_name" type="text" name="first_name" placeholder="First Name" value="{{$customer_data[0]->first_name}}" required="">
                                    <span id="name-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>Last Name</label>
                                    <input class="form-control" id="last_name" type="text" name="last_name" placeholder="full name" value="{{$customer_data[0]->last_name}}" required="">
                                    <span id="name-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>Company Name</label>
                                    <input class="form-control" id="company_name" type="text" name="company_name" placeholder="company name" value="{{$customer_data[0]->company_name}}" required="">
                                    <span id="company_name-error"></span>
                                </div>                                
                                <div class="col-md-4">
                                    <label>Email</label>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" value="{{$customer_data[0]->email}}" required="">
                                    <span id="email-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>Mobile Number</label>
                                    <input class="form-control" id="mobile_number" type="number" name="mobile_number" placeholder="Mobile Number" value="{{$customer_data[0]->mobile_number}}" required="">
                                    <span id="mobile_number-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>Office Number</label>
                                    <input class="form-control" id="office_number" type="number" name="office_number" placeholder="Office Number" value="{{$customer_data[0]->office_number}}" required="">
                                    <span id="office_number-error"></span>
                                </div>
                                <div class="col-md-12">
                                    <label>Address</label>
                                    <textarea class="form-control" id="address"  name="address" placeholder="Address" required="">{{$customer_data[0]->address}}</textarea>
                                    <span id="city-error"></span>
                                </div>
                                
                                </div>
                                <div class="row">
                                    <hr>
<!--                                <div class="col-md-12">
                                    <label>Gender</label>
                                    <div class="col-md-3">
                                        <label>
                                            <input type="radio" name="user_gender" required=""  value="male" <?php
                                            if ($customer_data[0]->user_gender == 'male') {
                                                echo 'checked';
                                            }
                                            ?>>Male
                                        </label>
                                        <label>
                                            <input type="radio" name="user_gender" required=""  value="female" <?php
                                            if ($customer_data[0]->user_gender == 'female') {
                                                echo 'checked';
                                            }
                                            ?>>Female
                                        </label>
                                    </div>                                    
                                    <span id="gender-error"></span>
                                </div>-->
                                
                                
                                <div class="col-md-4">
                                    <label>Type</label>
                                    <select class="form-control" name="type[]" multiple required="" >
                                        <?php foreach($type_of_truck as $v){
                                            if(in_array($v->id, explode(',',$customer_data[0]->type))){
                                                echo '<option value="'.$v->id.'" selected>'.$v->name.'</option>';
                                            }else{
                                                echo '<option value="'.$v->id.'">'.$v->name.'</option>'; 
                                            }
                                        } ?>
                                    </select>
                                    <span id="type-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>No of Truck</label>
                                    <input class="form-control" id="no_of_truck" type="number"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="3" name="no_of_truck" placeholder="No of Truck" value="{{$customer_data[0]->no_of_truck}}" required="">
                                    <span id="no_of_truck-error"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>CDL Number</label>
                                    <input class="form-control" id="vehicle_number" type="text" name="cdl" placeholder="cdl Number" value="{{$customer_data[0]->cdl}}" required="">
                                    <span id="email-error"></span>
                                </div>
<!--                                <div class="col-md-4">
                                    <label class="">User Type</label>
                                    <select class="form-control" name="user_type">
                                        <option value="1" <?php
                                                if ($customer_data[0]->user_type == 1) {
                                                    echo 'selected';
                                                }
                                                ?>>Customer</option>
                                        <option value="2" <?php
                                                if ($customer_data[0]->user_type == 2) {
                                                    echo 'selected';
                                                }
                                                ?>>Truck Owner</option>
                                    </select>
                                </div>-->
                                </div>
                                <div class="row">
                                    <hr>
                                    
                                <div class="col-md-6" >
                                    <label class="form-group">Profile Image</label>
                                    <div id='profile-upload' class="form-group pull-right" style="background-image: url('{{$customer_data[0]->avatar}}')">                                
                                        <div class="hvr-profile-img"><input type="file" name="image_icon" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>
                                </div>
                                    <div class="col-md-6">
                                        <label>Driving Licence</label>

                                    <div id='document_1' class="form-group pull-right doucuments-upload" style="background-image: url('{{$customer_data[0]->driving_licence_image}}')">                                
                                        <div class="hvr-profile-img"><input type="file" name="driving_licence_image" id='doucuments_1_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>                                 
       
                                @if($customer_data[0]->driving_licence_image == 'empty' || $customer_data[0]->driving_licence_image == null)
                                <div class="row">
                                    <span class="btn btn-danger"><small>driving licence image  not uploaded</small></span>
                                </div>
                                @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label>Insurance Image</label>

                                    <div id='document_2' class="form-group pull-right doucuments-upload" style="background-image: url('{{$customer_data[0]->insurance_image}}')">                                
                                        <div class="hvr-profile-img"><input type="file" name="insurance_image" id='doucuments_2_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>                                 
       
                                @if($customer_data[0]->insurance_image == 'empty' || $customer_data[0]->insurance_image == null)
                                <div class="row">
                                    <span class="btn btn-danger"><small>Insurance image  not uploaded</small></span>
                                </div>
                                @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label>W-9 Form</label>

                                    <div id='document_3' class="form-group pull-right doucuments-upload" style="background-image: url('{{$customer_data[0]->wq_image}}')">                                
                                        <div class="hvr-profile-img"><input type="file" name="wq_image" id='doucuments_3_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>    
                                    @if($customer_data[0]->wq_image == 'empty' || $customer_data[0]->wq_image == null)
                                    <div class="row">
                                        <span class="btn btn-danger"><small>WQ image  not uploaded</small></span>
                                    </div>
                                    @endif
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-4 pull-left">
                                    <button type="button" class="btn btn-primary" value="1" id='change_password'>Change Password</button>  
                                    <div id="password_changing"></div>
                                </div>
                                </div>


                            </div>                            
                         
                            <div class="col-lg-12 row">
                                <hr>
                            <div class="col-lg-12 text-center">
                                <button type="button" id="click_to_edit" class="btn btn-primary">Update</button>                                
                            </div>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection