@extends('admin_section.layouts.master')

@section('content')

<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Truck Owner Account</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Truck Owner 
                </div>
                <div class="panel-body">
                    <div class="row"></div>
                    <div class="row">
                        <form id="post_data" name="post_data" method="post" role="form" >
                             {{ csrf_field() }}
                            <div class="col-lg-12">
                                <div class="row">
                                <input type="hidden" id="user_type" name="user_type" value="2"/>
                                <div class="col-sm-4">
                                    <label>First Name</label>
                                    <input class="form-control" id="first_name" type="text" name="first_name" placeholder="First Name" required="">
                                    <span id="first_name-error"></span>
                                </div> 
                                <div class="col-sm-4">
                                    <label>Last Name</label>
                                    <input class="form-control" id="last_name" type="text" name="last_name" placeholder="Last Name" required="">
                                    <span id="last_name-error"></span>
                                </div> 
                                <div class="col-sm-4">
                                    <label>Company Name</label>
                                    <input class="form-control" id="company_name" type="text" name="company_name" placeholder="Company Name" required="">
                                    <span id="company_name-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Email</label>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" required="">
                                    <span id="email-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Mobile Number</label>
                                    <input class="form-control" id="mobile_number" type="number" name="mobile_number" placeholder="Mobile Number" required="">
                                    <span id="mobile_number-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Office Number</label>
                                    <input class="form-control" id="office_number" type="number" name="office_number" placeholder="Office Number" required="">
                                    <span id="office_number-error"></span>
                                </div>
                                <div class="col-sm-12">
                                    <label>Address</label>
                                    <textarea class="form-control" id="address" type="text" name="address" placeholder="Address" required=""></textarea>
                                    <span id="address-error"></span>
                                </div>
                            </div>
<div class="row">
    <hr>
                                <div class="col-sm-4">
                                    <label>CDL Number</label>
                                    <input class="form-control" id="cdl" type="text" name="cdl" placeholder="CDL Number" required="">
                                    <span id="vehicle-error"></span>
                                </div>
<!--                                <div class="col-sm-12">
                                    <label>Gender</label>
                                    <div class="col-sm-6">
                                      
                                            <input type="radio" name="user_gender" required=""  value="Male" checked>Male
                                         
                                    </div>
                                    <div class="col-sm-6">
                                         
                                            <input type="radio" name="user_gender" required=""  value="Female">Female
                                        
                                    </div>
                                    <span id="gender-error"></span>

                                </div>-->

                                <div class="col-sm-4">
                                        <label>Password</label>
                                        <input class="form-control" id="password" pattern="/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/" name="password" type="password" placeholder="Password" required="">
                                        <span id="password-error-first"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Confirm Password</label>
                                        <input class="form-control" id="confirm_password" type="password" placeholder="Confirm Password" required="">
                                        <span id="password_error"></span>
                                    </div>      
</div>
<div class="row">
                               <div class="col-sm-6">
                                    <label>Truck Type</label>
                                    <select class="form-control" name="type" multiple required="">
                                        <?php foreach($type_of_truck as $v){
                                            echo '<option value="'.$v->id.'">'.$v->name.'</option>';
                                        } ?>
                                    </select>
                                    
                                    <span id="type-error"></span>
                                </div>
                                
                               
                                    <div class="col-sm-6">
                                    <label>No of Truck</label>
                                    <input class="form-control" id="no_of_truck" type="number"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="3" name="no_of_truck" placeholder="No of Truck" required="">
                                    <span id="no_of_truck-error"></span>
                                </div>
</div>
                                <div class="row">
                                    <hr>
                                <div class="col-sm-6">
                                   
                                    <label>Profile Image</label>
                                    <div id='profile-upload' class=" pull-right" style="background-image: url('')">                                
                                    <div class="hvr-profile-img"><input type="file" name="image_icon" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                      <i class="fa fa-camera"></i>  
                                    </div>
                             
                                    </div>
                                
                                <div class="col-sm-6">
                                
                                <label>Driver’s	License</label>
                                
                                    <div id='document_1' class="col-sm-6 pull-right doucuments-upload" style="background-image: url('')">                                
                                        <div class="hvr-profile-img"><input type="file" name="driving_licence_image" id='doucuments_1_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>                                 
                                </div>
                                </div>
                                <div class="row">
                                    <hr>
                                <div class="col-sm-6">
                                    
                                <label>Truck Insurance</label>

                                    <div id='document_2' class="col-sm-6 pull-right doucuments-upload" style="background-image: url('')">                                
                                        <div class="hvr-profile-img"><input type="file" name="insurance_image" id='doucuments_2_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>                                 
                                </div>

                                <div class="col-sm-6">
                                    
                                <label>W-9 Form</label>

                                    <div id='document_3' class="col-sm-6 pull-right doucuments-upload" style="background-image: url('')">                                
                                        <div class="hvr-profile-img"><input type="file" name="wq_image" id='doucuments_3_change'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                        <i class="fa fa-camera"></i>  
                                    </div>                                 
                                </div>
                                </div>
                                
                        


                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            
                            <div class="col-lg-12 text-center">
                                <hr>
                                <button type="button" id="click_to_submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-primary">Reset</button>
                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection
