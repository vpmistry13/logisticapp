@extends('admin_section.layouts.master')

@section('content')
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/user.png')}}" width="115px"/>
                                </div>
                                <div class="col-xs-6 text-right pull-right">
                                    <div class="huge">{{$total_users}}</div>
                                    <div>All Customers</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/customer/list')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/driver_icon.png')}}" width="100px"/>
                                </div>
                                <div class="col-xs-6 text-right pull-right">
                                    <div class="huge">{{$total_drivers}}</div>
                                    <div>All Truck Drivers / Owners</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/driver/list')}}">
                        <!--<a href="#">-->
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/vehicle_icon.png')}}" height="100px"/>
                                </div>
                                <div class="col-xs-4 text-right pull-right">
                                    <div class="huge">{{$total_all}}</div>
                                    <div>All Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/shiping/list_all')}}">
                            <div class="panel-footer" >
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/vehicle_icon.png')}}" width="100px"/>
                                </div>
                                <div class="col-xs-4 text-right pull-right">
                                    <div class="huge">{{$total_pending}}</div>
                                    <div>New Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/shiping/list_pending')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!--                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/vehicle_icon.png')}}" height="100px"/>
                                </div>
                                <div class="col-xs-4 text-right pull-right">
                                    <div class="huge">{{$total_active}}</div>
                                    <div>Active Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/shiping/list_active')}}">
                            <div class="panel-footer" >
                                <span class="pull-left" style="color:black;">View Details</span>
                                <span class="pull-right" style="color:black;"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>-->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-orange">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/truck-animation.gif')}}" height="100px"/>
                                </div>
                                <div class="col-xs-4 text-right pull-right">
                                    <div class="huge">{{$total_live}}</div>
                                    <div class="pull">Ongoing Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/shiping/list_live')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <img src="{{asset('images/default/vehicle_icon.png')}}" width="100px"/>
                                </div>
                                <div class="col-xs-5 text-right pull-right">
                                    <div class="huge">{{$total_complete}}</div>
                                    <div>Completed Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('admin/shiping/list_complete')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<!--            <div class="row" id="map_load">
                <div class="col-lg-6">
                    <div class="panel panel-default">                        
                        <div class="panel-body">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>   
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">                        
                        <div class="panel-body">
                            <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>   
                </div>
                
            </div>-->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
        @endsection