@extends('admin_section.layouts.master')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Group Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <style>
        .text-wrap{
    white-space:normal;
}
.width-200{
    width:200px;
}
    </style>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                     List of Truck Owner Group
                     
                </div>
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="group_managment">
                        <thead>
                            <tr class="design_table">
                                <!--<th>Driver ID</th>-->
                                <!--<th>Profile Picture</th>-->
                                <th><small>Group Name</small></th>
                                <!--<th><small>Users in Group</small></th>-->
                                <th><small>Action</small></th>
                            </tr>
                        </thead>                       
                    </table>
                    <!-- /.table-responsive -->
                  
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
       
    </div>
   
</div>

<!-- /#page-wrapper -->
@endsection