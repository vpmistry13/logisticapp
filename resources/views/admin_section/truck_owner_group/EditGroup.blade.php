@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<style>
     #button ul {
  list-style: none;
  margin: 0;
  padding: 0;
  border: none;
  }
  
 #button li {
  border-bottom: 1px solid #90bade;
  margin: 10px;
  }
    
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Group</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="group_data_edit" name="group_data_edit" method="post" role="form">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$group_id}}"/>
                            <div class="col-lg-12">
                                <div class="row">
                                <div class="col-md-4">
                                    <label>Group Name</label>
                                    <input class="form-control" id="group_name" type="text" name="group_name" placeholder="Group Name" value="{{$group_data->group_name}}" required="">
                                    <span id="name-error"></span>
                                </div>
                                    <?php if($users_in_group): ?>                                   
                                    <div class="col-md-6" id="button"> 
                                        <table class="table table-striped table-bordered">
                                            <thead><th>Company Name</th><th>Name</th><th>Email</th><th>Remove</th></thead>
                                        
                                            <tbody id="users_group">
                                            <?php foreach($users_in_group as $v):
                                             echo '<tr>';
                                             echo "<td>".$v->company_name."</td><td>".$v->first_name." ".$v->last_name."</td><td>".$v->email."<input type='hidden' name='users_ids[]' value='".$v->id."'/></td><td><a  class='clearitem'><i class='fa fa-remove'></i></a></td>";
                                             echo '</tr>';
                                             endforeach;
                                             ?>
                                             </tbody>
                                        
                                            </table>
                                    </div>
                                    <?php endif; ?>                                
                                </div>
                            </div>                            
                            <div class="col-lg-12 row">
                                <hr>
                            <div class="col-lg-12 text-center">
                                <button type="button" id="click_to_submit_group" class="btn btn-primary">Save</button>                                
                            </div>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection