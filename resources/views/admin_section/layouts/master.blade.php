<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>JBCtruckloads ! Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="{{ asset('admin/vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{ asset('admin/dist/css/sb-admin-2.css')}}" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="{{ asset('admin/vendor/morrisjs/morris.css')}}" rel="stylesheet">
        <!-- DataTables CSS -->
        <link href="{{ asset('admin/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="{{ asset('admin/vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">
        <!--<link href="{{url('admin/vendor/datatables/css/dataTables.editor.css')}}" rel="stylesheet">-->
        <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css" rel="stylesheet">
        
        <!-- Custom Fonts -->
        <link href="{{ asset('admin/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<!--        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('admin/favicon/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('admin/favicon/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('admin/favicon/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('admin/favicon/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('admin/favicon/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('admin/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('admin/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('admin/favicon/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('admin/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('admin/favicon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('admin/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('admin/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('admin/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('admin/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{asset('admin/favicon/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">-->
        <link href="{{asset('admin/dist/toast/toast.css')}}" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/jquery.raty.min.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
<!--                    <a class="navbar-brand" href="{{url('admin/dashboard')}}">Logistic App</a>-->
<img src="{{url('logo-png-min.png')}}" width="250px" height="65px" />
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!--                    <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-messages">
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <strong>John Smith</strong>
                                                            <span class="pull-right text-muted">
                                                                <em>Yesterday</em>
                                                            </span>
                                                        </div>
                                                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <strong>John Smith</strong>
                                                            <span class="pull-right text-muted">
                                                                <em>Yesterday</em>
                                                            </span>
                                                        </div>
                                                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <strong>John Smith</strong>
                                                            <span class="pull-right text-muted">
                                                                <em>Yesterday</em>
                                                            </span>
                                                        </div>
                                                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a class="text-center" href="#">
                                                        <strong>Read All Messages</strong>
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                             /.dropdown-messages 
                                        </li>
                                         /.dropdown 
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-tasks">
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <p>
                                                                <strong>Task 1</strong>
                                                                <span class="pull-right text-muted">40% Complete</span>
                                                            </p>
                                                            <div class="progress progress-striped active">
                                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                                    <span class="sr-only">40% Complete (success)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <p>
                                                                <strong>Task 2</strong>
                                                                <span class="pull-right text-muted">20% Complete</span>
                                                            </p>
                                                            <div class="progress progress-striped active">
                                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                                    <span class="sr-only">20% Complete</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <p>
                                                                <strong>Task 3</strong>
                                                                <span class="pull-right text-muted">60% Complete</span>
                                                            </p>
                                                            <div class="progress progress-striped active">
                                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                                    <span class="sr-only">60% Complete (warning)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <p>
                                                                <strong>Task 4</strong>
                                                                <span class="pull-right text-muted">80% Complete</span>
                                                            </p>
                                                            <div class="progress progress-striped active">
                                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                                    <span class="sr-only">80% Complete (danger)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a class="text-center" href="#">
                                                        <strong>See All Tasks</strong>
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                             /.dropdown-tasks 
                                        </li>
                                         /.dropdown 
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-alerts">
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <i class="fa fa-comment fa-fw"></i> New Comment
                                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                                            <span class="pull-right text-muted small">12 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <i class="fa fa-envelope fa-fw"></i> Message Sent
                                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <i class="fa fa-tasks fa-fw"></i> New Task
                                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                                            <span class="pull-right text-muted small">4 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a class="text-center" href="#">
                                                        <strong>See All Alerts</strong>
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                             /.dropdown-alerts 
                                        </li>-->
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw fa-2x"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{url('admin/User_Profile/edit')}}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="{{url('admin/General_setting/list')}}"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out fa-fw"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_type" value="3"/>
                                </form>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <!--                        <li class="sidebar-search">
                                                        <div class="input-group custom-search-form">
                                                            <input type="text" class="form-control" placeholder="Search...">
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button">
                                                                <i class="fa fa-search"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                         /input-group 
                                                    </li>-->
                            <li>
                                <a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard fa-fw fa-2x"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw fa-2x"></i> Users Management<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">                                
                                    <li>
                                        <a href="{{url('admin/driver/list')}}"><span class="sub_menu">Truck Owners</span><span class="fa arrow"></span></a>
<!--                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="{{url('admin/driver/addAccount')}}">Add Account</a>
                                            </li>
                                            <li>
                                                <a href="{{url('admin/driver/list')}}">List of Account</a>
                                            </li>                                        
                                        </ul>-->
                                         <!--/.nav-third-level--> 
                                    </li>
                                    <li>
                                        <a href="{{url('admin/customer/list')}}"><span class="sub_menu">Customers</span> <span class="fa arrow"></span></a>
<!--                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="{{url('admin/customer/addAccount')}}">Add Account</a>
                                            </li>
                                            <li>
                                                <a href="{{url('admin/customer/list')}}">List of Account</a>
                                            </li>                                        
                                        </ul>-->
                                        <!-- /.nav-third-level -->
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                 <a href="{{url('admin/truck_owner_group')}}"><i class="fa fa-users fa-fw fa-2x"></i> Truck Owner Group</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-bus fa-fw fa-2x"></i> Jobs Management<span class="fa arrow"></span></a>                            
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('admin/shiping/create')}}"><span class="sub_menu">Create Job</span></a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/shiping/list_all')}}"><span class="sub_menu">All Jobs</span></a>
                                    </li>                                        
                                    <li>
                                        <a href="{{url('admin/shiping/list_complete')}}"><span class="sub_menu">Completed Jobs</span></a>
                                    </li>                                        
<!--                                    <li>
                                        <a href="{{url('admin/shiping/list_active')}}"><span class="sub_menu">Active Jobs</span></a>
                                    </li>                                        -->
                                    <li>
                                        <a href="{{url('admin/shiping/list_live')}}"><span class="sub_menu">Ongoing jobs</span></a>
                                    </li>                                        
                                    <li>
                                        <a href="{{url('admin/shiping/list_pending')}}"><span class="sub_menu">New Jobs</span></a>
                                    </li>                                        
<!--                                    <li>
                                        <a href="{{url('admin/shiping/list_rejects')}}"><span class="sub_menu">Rejects Jobs</span></a>
                                    </li>                                        -->
                                </ul>
                                                         
                            </li>
                            
                   <!--         <li>
                                <a href="#"><i class="fa fa-bus"></i> Manage Vehicle<span class="fa arrow"></span></a>                            
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('admin/vehicle/addVehicle')}}">Add Vehicle</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/vehicle/list')}}">List of Vehicle</a>
                                    </li>                                        
                                </ul>
                                                      
                            </li>
                                <a href="#"><i class="fa fa-car fa-fw"></i> Manage Body Types<span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('admin/vehicle_body/addBody')}}">Add Body Types</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/vehicle_body/list')}}">List of Body Types</a>
                                    </li>                                        
                                </ul>
                                 /.nav-third-level 

                            </li>
                            <li>
                                <a href="#"><i class="fa fa-cube fa-fw"></i> Manage Goods Types<span class="fa arrow"></span></a>                            
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('admin/goods_type/addGoodsType')}}">Add Goods Types</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/goods_type/list')}}">List of Goods Types</a>
                                    </li>                                        
                                </ul>
                                 /.nav-third-level 

                            </li>-->
<!--                            <li>
                                <a href="#"><i class="fa fa-bell fa-fw fa-2x"></i> Manage Notification<span class="fa arrow"></span></a>                            
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{url('admin/notification/list')}}"><span class="sub_menu">List of Notification</span></a>
                                    </li>                                        
                                </ul>
                                 /.nav-third-level 
                            </li>-->
                            <li>
                                <a href="{{url('admin/report')}}"><i class="fa fa-file fa-fw fa-2x"></i> Reports</a>
                            </li>
                            <li>
                                <a href="{{url('admin/General_setting/list')}}"><i class="fa fa-gear fa-fw fa-2x"></i> General Settings</a>
                            </li>
<!--                            <li>
                                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="flot.html">Flot Charts</a>
                                    </li>
                                    <li>
                                        <a href="morris.html">Morris.js Charts</a>
                                    </li>
                                </ul>
                                 /.nav-second-level 
                            </li>
                            <li>
                                <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                            </li>
                            <li>
                                <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="panels-wells.html">Panels and Wells</a>
                                    </li>
                                    <li>
                                        <a href="buttons.html">Buttons</a>
                                    </li>
                                    <li>
                                        <a href="notifications.html">Notifications</a>
                                    </li>
                                    <li>
                                        <a href="typography.html">Typography</a>
                                    </li>
                                    <li>
                                        <a href="icons.html"> Icons</a>
                                    </li>
                                    <li>
                                        <a href="grid.html">Grid</a>
                                    </li>
                                </ul>
                                 /.nav-second-level 
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="#">Second Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Second Level Item</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level <span class="fa arrow"></span></a>
                                        <ul class="nav nav-third-level">
                                            <li>
                                                <a href="#">Third Level Item</a>
                                            </li>
                                            <li>
                                                <a href="#">Third Level Item</a>
                                            </li>
                                            <li>
                                                <a href="#">Third Level Item</a>
                                            </li>
                                            <li>
                                                <a href="#">Third Level Item</a>
                                            </li>
                                        </ul>
                                         /.nav-third-level 
                                    </li>
                                </ul>
                                 /.nav-second-level 
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="blank.html">Blank Page</a>
                                    </li>
                                    <li>
                                        <a href="login.html">Login Page</a>
                                    </li>
                                </ul>
                                 /.nav-second-level 
                            </li>-->
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            @yield('content')
            <footer class="sticky-footer">
                <div class="container">
                    <div class="text-center">
                        <small>Copyright © JBCtruckloads 2019</small> 
                    </div>
                </div>
            </footer>
        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{asset('admin/vendor/metisMenu/metisMenu.min.js')}}"></script>

        <!--     Morris Charts JavaScript 
            <script src="{{url('admin/vendor/raphael/raphael.min.js')}}"></script>
            <script src="{{url('admin/vendor/morrisjs/morris.min.js')}}"></script>
            <script src="{{url('admin/data/morris-data.js')}}"></script>-->

        

        <!-- DataTables JavaScript -->
        <script src="{{asset('admin/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('admin/vendor/datatables-plugins/dataTables.bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>       
        <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
        <!--<script src="{{url('admin/vendor/datatables/js/dataTables.editor.js')}}"></script>-->
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/jquery.raty.min.js"></script>
        <!-- toast -->
        <script src="{{asset('admin/dist/toast/toast.js')}}"></script>

        
        <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
        </script>
        <!-- Custom Theme JavaScript -->
        
        <script src="{{asset('admin/dist/js/sb-admin-2.js')}}"></script>
        <script src="{{asset('admin/js/master.js')}}"></script>
        <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
        <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#loading_start_time').datetimepicker({format: 'YYYY-MM-DD hh:mm'});
            });
            $(function () {
                $('#job_start_time').datetimepicker({format: 'YYYY-MM-DD'});
            });
            $(function () {
                $('#loading_end_time').datetimepicker({format: 'YYYY-MM-DD hh:mm'});
            });
            
//            For No of truck
            function get_select_vehicle_type(e){
                $('#multiple_no_of_truck').html('');
                var truck_name = $("#vehicle_type_id option:selected").map(function(){ return this.id }).get();
                console.log(truck_name);
                var vehicle_type_ids = $('#vehicle_type_id').val();
                     $('#multiple_no_of_truck').append('<label class="col-sm-12">No of Truck by Truck Type</label>');
                 $.each(vehicle_type_ids,function(e){
//                     console.log(vehicle_type_ids[e]);
                     $('#multiple_no_of_truck').append('<div class="col-sm-12"><input placeholder='+truck_name[e]+' class="form-control" type="number" name="no_of_truck_need['+truck_name[e]+'][]"/></div>');
                 });
            }
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datepicker({format: 'yyyy-mm-dd'});
            });
        </script>
    </body>

</html>