@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Setting</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="setting_edit" name="setting_edit" method="post" role="form">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Header Title</label>
                                    <input class="form-control" id="header_title" type="text" name="header_title" placeholder="header_title" value="{{$list[0]->header_title}}" required="">
                                    <span id="header_title-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>Admin Title</label>
                                    <input class="form-control" id="admin_title" type="text" name="admin_title" placeholder="Admin Title" value="{{$list[0]->admin_title}}" required="">
                                    <span id="admin_title-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>Email Configuration</label>
                                    <input class="form-control" id="email_id" type="email" name="email_id" placeholder="Email Configuration" value="{{$list[0]->email_id}}" required="">
                                    <span id="email-error"></span>
                                </div>
<!--                                <div class="form-group">
                                    <label>Email Password Configuration</label>
                                    <input class="form-control" id="email_id" type="password" name="email_pwd" placeholder="Email / PWD Configuration" value="{{base64_decode($list[0]->email_credential)}}" required="">
                                    <span id="email-error"></span>
                                </div>-->
                                
                            </div>                            
                            <!-- /.col-lg-6 (nested) -->
<!--                            <div class="col-lg-6" >      
                                <div class="" >
                                    <label class="form-group">Site Logo</label>
                                    <div id='profile-upload' class="form-group" style="background-image: url('{{url("storage/app/".$list[0]->site_logo)}}')">                                
                                    <div class="hvr-profile-img"><input type="file" name="site_logo" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                      <i class="fa fa-camera"></i>  
                                    </div>
                                </div>                               
                            </div>-->
                            <div class="col-lg-12 text-center">
                                <button type="button" id="setting_edit_submit" class="btn btn-primary">Save</button>                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection