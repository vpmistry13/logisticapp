@extends('admin_section.layouts.master')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   General Setting
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="">
                        <thead>
                            <tr>
                                <th>Header Title</th>
                                <th>Admin Title</th>
<!--                                <th>Site Logo</th>
                                <th>Per KM Rate</th>-->
                                <th>Admin Email</th>
                                <!--<th>email_credential</th>-->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $list[0]->header_title; ?></td>
                                <td><?php echo $list[0]->admin_title; ?></td>
<!--                                <td><?php //echo $list[0]->site_logo; ?></td>
                                <td><?php //echo $list[0]->per_km_rate; ?></td>-->
                                <td><?php echo $list[0]->email_id; ?></td>
                                <!--<td><?php // echo $list[0]->email_credential; ?></td>-->
                                <td><a href="{{url('admin/setting/edit/1')}}"><i class="fa fa-edit"></i></a></td>
                            </tr>
                        </tbody>
                    </table>                  
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@endsection