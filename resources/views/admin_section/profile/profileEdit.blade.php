@extends('admin_section.layouts.master')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">User Profile</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Profile Detail
                        </div>
                        <form name="post_data" id="profile_data_edit" method="post">
                             {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="row">
                               
                                <div class="col-lg-6">
                                        <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" name="first_name" class="form-control" id="first_name" value="{{ $info[0]->first_name }}" placeholder="Enter First Name" minlength="2" required>
                                            <span id="full_name-error"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" name="last_name" class="form-control" id="last_name" value="{{ $info[0]->last_name }}" placeholder="Enter Last Name" minlength="2" required>
                                            <span id="full_name-error"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="text" name="email" class="form-control" id="email" value="{{ $info[0]->email }}" placeholder="Enter email" minlength="2" readonly="">
                                            <span id="full_name-error"></span>
                                        </div>
                                        
<!--                                        
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <input type="date" name="birth_date" class="form-control" id="birth_date" value="{{ @$info[0]->birth_date }}" placeholder="Enter Birth Date">
                                        </div>-->
                                        
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="number" name="mobile_number" class="form-control" id="mobile_number" value="{{ @$info[0]->mobile_number }}" placeholder="Enter Mobile Number">
                                            <span id="mobile_number-error"></span>
                                        </div>
                                        
                                        
                                        

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Avatar</label>
                                        <div id='profile-upload' style="@if($info[0]->avatar)background-image:url({{ $info[0]->avatar }}) @else background-image:url({{url('images/default/default.png')}}) @endif">
                                            <div class="hvr-profile-img"><input type="file" name="avatar" id='getval' class="upload w180" title="Dimensions 80 X 80" id="imag"></div>
                                            <i class="fa fa-camera"></i>
                                        </div>
                                    </div>
                                </div>    
                                <!-- /.col-lg-6 (nested) -->
                                
                            </div>
                            <button type="button" id="profile_edit_submit" class="btn btn-default">Update</button>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <!-- /.row -->
</div>
@endsection
