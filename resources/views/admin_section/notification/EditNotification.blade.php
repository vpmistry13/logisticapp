@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Notification</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="notification_edit" name="notification_edit" method="post" role="form">
                            <div class="col-lg-6">
                                <input type="hidden" name="id" value="{{$id}}"/>
                                
                                <div class="form-group">
                                    <label>Message</label>
                                    <input class="form-control" id="message" type="message" name="message" placeholder="Email" value="{{$notification_data[0]->message}}" required="">
                                    <span id="message-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="0" <?php if($notification_data[0]->status == '0'){ echo 'checked';}?>>Deactive
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="1" <?php if($notification_data[0]->status == '1'){ echo 'checked';}?>>Active
                                        </label>
                                    </div>
                                    <span id="gender-error"></span>
                                </div>
                            </div>                            
                            
                            <div class="col-lg-12 text-center">
                                <button type="button" id="notification_edit_submit" class="btn btn-primary">Save</button>                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection