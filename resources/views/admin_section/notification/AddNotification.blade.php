@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Customer Account</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="post_data" name="post_data" method="post" role="form">
                            <div class="col-lg-6">
                                <input type="hidden" name="user_type" value="1"/>
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input class="form-control" id="full_name" type="text" name="full_name" placeholder="full name" required="">
                                    <span id="full_name-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <input class="form-control" id="user_city" type="text" name="user_city" placeholder="City" required="">
                                    <span id="city-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" required="">
                                    <span id="email-error"></span>
                                </div>
                                <div class="form-group">
                                    <label>Mobile Number</label>
                                    <input class="form-control" id="mobile_number" type="number" name="mobile_number" placeholder="Mobile Number" required="">
                                    <span id="mobile_number-error"></span>
                                </div>


                                <div class="form-group">
                                    <label>Gender</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="user_gender" required=""  value="male" checked>Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="user_gender" required=""  value="female">Female
                                        </label>
                                    </div>
                                    <span id="gender-error"></span>

                                </div>



                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            <div class="col-lg-6">                      
                                <div class="" >
                                    <label class="form-group">Profile Image</label>
                                    <div id='profile-upload' class="form-group" style="background-image: url('')">                                
                                    <div class="hvr-profile-img"><input type="file" name="image_icon" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                      <i class="fa fa-camera"></i>  
                                    </div>
                                </div>
                                <fieldset>
                                    <div class="form-group">
                                        <label for="disabledSelect">Password</label>
                                        <input class="form-control" id="password" name="password" type="password" placeholder="password" required="">
                                        <span id="password-error-first"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password</label>
                                        <input class="form-control" id="confirm_password" type="password" placeholder="confirm password" required="">
                                        <span id="password_error"></span>
                                    </div>                                   
                                </fieldset>                                                      
                            </div>
                            <div class="col-lg-12 text-center">
                                <button type="button" id="click_to_submit" class="btn btn-default">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection