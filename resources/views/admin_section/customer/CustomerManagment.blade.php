@extends('admin_section.layouts.master')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">User Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List of  Customers
                    <div class="pull-right">
                        <a class="btn btn-default btn-xs" href="{{url('admin/customer/addAccount')}}">Add Customer Account</a>
                                            </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="customer_managment">
                        <thead>
                            <tr class="design_table">
<!--                                <th>Customer ID</th>
                                <th>Profile Picture</th>-->
                                <th><small>Name</small></th>
                                <th><small>Company Name</small></th>
                                <th><small>Email</small></th>
                                <th><small>Address</small></th>
                                <th><small>Mobile Number</small></th>
                                <th><small>Mobile (O)</small></th>
                                <th><small>Action</small></th>
                            </tr>
                        </thead>
                    </table>                  
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@endsection