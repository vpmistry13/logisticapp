@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Customer Account</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="post_data_edit" name="post_data_edit" method="post" role="form">
                            {{ csrf_field() }}
                            <div class="col-lg-12">
                                <div class="row">
                                    <input type="hidden" name="user_type" value="1"/>
                                    <input type="hidden" name="user_id" value="{{$user_id}}"/>
                                    <div class="col-sm-4">
                                        <label>First Name</label>
                                        <input class="form-control" id="first_name" type="text" name="first_name" placeholder="First Name" value="{{$customer_data[0]->first_name}}" required="">
                                        <span id="first_name-error"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Last Name</label>
                                        <input class="form-control" id="last_name" type="text" name="last_name" placeholder="Last Name" value="{{$customer_data[0]->last_name}}" required="">
                                        <span id="last_name-error"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Company Name</label>
                                        <input class="form-control" id="company_name" type="text" name="company_name" placeholder="Company Name" value="{{$customer_data[0]->company_name}}" required="">
                                        <span id="company_name-error"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Email</label>
                                        <input class="form-control" id="email" type="email" name="email" placeholder="Email" value="{{$customer_data[0]->email}}" required="">
                                        <span id="email-error"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Mobile number</label>
                                        <input class="form-control" id="mobile_number" type="number" name="mobile_number" placeholder="Mobile number" value="{{$customer_data[0]->mobile_number}}" required="">
                                        <span id="mobile_number-error"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>office number</label>
                                        <input class="form-control" id="office_number" type="number" name="office_number" placeholder="Office number" value="{{$customer_data[0]->office_number}}" required="">
                                        <span id="office_number-error"></span>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Address</label>
                                        <textarea class="form-control" id="address"  name="address" placeholder="Address"  required="">{{$customer_data[0]->address}}</textarea>
                                        <span id="address-error"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-lg-6" >      
                                        <div class="col-sm-12" >
                                            <label class="col-sm-6">Profile Image</label>
                                            <div id='profile-upload' class="col-sm-6" style="background-image: url('{{$customer_data[0]->avatar}}')">                                
                                                <div class="hvr-profile-img"><input type="file" name="image_icon" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                                <i class="fa fa-camera"></i>  
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <button type="button" class="btn btn-primary" value="1" id='change_password'>Change Password</button>                                
                                        <div id="password_changing"></div>   
                                    </div>
                                    
                                </div>
                                <div class="row">
                                        <hr>
                                    <div class="col-lg-12 text-center">
                                        <button type="button" id="click_to_edit" class="btn btn-primary">Save</button>                                
                                    </div>
                                    </div>

                                <!--                                <div class="col-sm-6">
                                                                    <label>Gender</label>
                                                                    <div class="col-sm-3">
                                
                                                                        <input type="radio" name="user_gender" required=""  value="male" <?php
                                if ($customer_data[0]->user_gender == 'male') {
                                    echo 'checked';
                                }
                                ?>>Male
                                
                                                                    </div>
                                                                    <div class="col-sm-3">
                                
                                                                        <input type="radio" name="user_gender" required=""  value="female" <?php
                                if ($customer_data[0]->user_gender == 'female') {
                                    echo 'checked';
                                }
                                ?>>Female
                                
                                                                    </div>
                                                                    <span id="gender-error"></span>
                                                                </div>-->
                            </div>                            

                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection