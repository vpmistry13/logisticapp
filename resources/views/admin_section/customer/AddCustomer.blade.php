@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Customer Account</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Customer 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="post_data" name="post_data" method="post" role="form">
                              {{ csrf_field() }}
                            <div class="col-lg-12">
                                <div class="row">
                                <input type="hidden" name="user_type" value="1"/>
                                <div class="col-sm-4">
                                    <label>First Name</label>
                                    <input class="form-control" id="first_name" type="text" name="first_name" placeholder="First Name" required="">
                                    <span id="first_name-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Last Name</label>
                                    <input class="form-control" id="last_name" type="text" name="last_name" placeholder="Last Name" required="">
                                    <span id="last_name-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Company Name</label>
                                    <input class="form-control" id="company_name" type="text" name="company_name" placeholder="Company Name" required="">
                                    <span id="company_name-error"></span>
                                </div>
                                
                                <div class="col-sm-4">
                                    <label>Email</label>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" required="">
                                    <span id="email-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Mobile Number</label>
                                    <input class="form-control" id="mobile_number" type="number" name="mobile_number" placeholder="Mobile Number" required="">
                                    <span id="mobile_number-error"></span>
                                </div>
                                <div class="col-sm-4">
                                    <label>Office Number</label>
                                    <input class="form-control" id="office_number" type="number" name="office_number" placeholder="Office Number" required="">
                                    <span id="office_number-error"></span>
                                </div>

                                                                     
    
<!--                                <div class="col-sm-12">
                                    <label>Gender</label>
                                    <div class="col-md-6">
                                       
                                            <input type="radio" name="user_gender" required=""  value="male" checked>Male
                                       
                                    </div>
                                    <div class="col-md-6">
                                       
                                            <input type="radio" name="user_gender" required=""  value="female">Female
                                        
                                    </div>
                                    <span id="gender-error"></span>

                                </div>-->
                                <div class="col-md-12">
                                    <label>Address</label>
                                    <textarea class="form-control" id="user_city" type="text" name="user_city" placeholder="Address" required=""></textarea>
                                    <span id="city-error"></span>
                                </div>
                                </div>
                                <div class="row">
                                    <hr>
                                
                                 <div class="col-sm-6">
                                        <label for="disabledSelect">Password</label>
                                        <input class="form-control" id="password" name="password" type="password" placeholder="Password" required="">
                                        <span id="password-error-first"></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="confirm_password">Confirm Password</label>
                                        <input class="form-control" id="confirm_password" type="password" placeholder="Confirm Password" required="">
                                        <span id="password_error"></span>
                                    </div> 

                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-md-6" >
                                    <label >Profile Image</label>
                                    <div id='profile-upload' class="pull-right" style="background-image: url('<?php echo asset('images/default/avatar.gif'); ?>')">                                
                                    <div class="hvr-profile-img form-control"><input type="file" name="image_icon" id='getval'  class="upload w180 form-control" title="Dimensions 100 X 100" id="imag"></div>
                                      <i class="fa fa-camera"></i>  
                                    </div>
                                </div>
                                </div>

                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            
                            
                            <div class="col-lg-12">
                                <hr>
                            <div class="col-md-12 text-center">
                                <button type="button" id="click_to_submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-primary">Reset</button>
                            </div>
                            </div>
                            

                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection