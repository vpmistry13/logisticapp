@extends('admin_section.layouts.master')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Report Management</h1>
        </div>
        @if (\Session::has('success'))
        <div class="col-md-6">
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        </div>
        @elseif(\Session::has('error'))
        <div class="col-md-6">
            <div class="alert alert-danger">
                <ul>
                    <li>{!! \Session::get('error') !!}</li>
                </ul>
            </div>
        </div>
        @endif
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Reports
                </div>
                <form method="post" action="{{url('admin/generate_report')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <hr>
                            <div class="col-md-4">
                                <select class="form-control" name="report_type" required="">
                                    <option>---Select Report Type----</option>
                                    <option value="1">Customers</option>
                                    <option value="2">Truk Owners</option>
                                    <option value="3">Jobs</option>
                                </select>
<!--                                <select name="status" class="form-control">
                                    <option value="1">Active Job</option>
                                    <option value="2">Reject Job</option>
                                    <option value="3">OnGoing Job</option>
                                    <option value="4">Complete Job</option>
                                </select>-->
                            </div>
                            <div class="col-md-2">                            
                                <input type="date" name="from_date" placeholder="From Date" required=""/>
                            </div>
                            <div class="col-md-2">                            
                                <input type="date" name="to_date" placeholder="To Date" required=""/>
                            </div>
                            <div class="col-md-2">                            
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </div>
                    </div>
                </form>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <hr>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="">
                        <thead>
                            <tr class="design_table">                                
                                <th>Report Type</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th>Generate Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($report_data)
                            @foreach($report_data as $v)
                            <tr>
                                <td>@if($v->report_type == 2) Truck Owners Report @elseif($v->report_type == 1) Customers Reports @elseif($v->report_type == 3) Jobs Reports @endif</td>
                                <td>{{ date('F d, Y h:m:s',strtotime($v->from_date)) }}</td>
                                <td>{{ date('F d, Y h:m:s',strtotime($v->to_date)) }}</td>
                                <td>{{ date('F d, Y h:m:s',strtotime($v->created_at)) }}</td>
                                <td><a class="btn btn-primary btn-xs col-sm-2" href="{{url('storage/app')."/".$v->report_url}}" target="_blank"><i class="fa fa-download"></i></a>&nbsp;<form class="col-sm-2" method="post" action='{{url('admin/generate_report_removed')}}'>@csrf<button type="submit" name="report_id" value="{{$v->id}}" class="btn btn-primary btn-xs"><i class="fa fa-remove"></i></button></form></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@endsection

