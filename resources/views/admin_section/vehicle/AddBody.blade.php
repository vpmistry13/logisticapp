@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Body</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="vehicle_body_data" name="vehicle_body_data" method="post" role="form">
                            <div class="col-lg-6">                             
                                <div class="form-group">
                                    <label>Body Name</label>
                                    <input class="form-control" id="body_name" type="text" name="body_name" placeholder="Vehicle Name" required="">

                                </div>                               
                                <div class="form-group">
                                    <label>Body Image</label>
                                    <input class="form-control" type="file" name="image_icon" required="">
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="1" checked>Active
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="0">Deactive
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="vehicle_body_data_submit" class="btn btn-default">Add</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </div>

                            </div>                            


                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection