@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Vehicle</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="row">
                       <form id="vehicle_data_edit" name="vehicle_data_edit" method="post" role="form">
                           <input type="hidden" name="user_id" value="{{$user_id}}"/>
                            <div class="col-lg-6">                             
                                <div class="form-group">
                                    <label>Vehicle Name</label>
                                    <input class="form-control" id="vehicle_name" type="text" name="vehicle_name" value="{{$vehicle_data[0]->name}}" placeholder="Vehicle Name" required="">

                                </div>                               
                                <div class="" >
                                    <label class="form-group">Vehicle Image</label>
                                    <div id='profile-upload' class="form-group" style="background-image: url('{{url('../storage/app/'.$vehicle_data[0]->image_icon)}}')">                                
                                    <div class="hvr-profile-img"><input type="file" name="image_icon" id='getval'  class="upload w180" title="Dimensions 100 X 100" id="imag"></div>
                                      <i class="fa fa-camera"></i>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="1" @if($vehicle_data[0]->status == 1) checked @endif>Active
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="status" required=""  value="0" @if($vehicle_data[0]->status == 0) checked @endif>Deactive
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <button type="button" id="vehicle_data_edit_submit" class="btn btn-default">Update</button>
                                </div>

                            </div>                            


                            <!-- /.col-lg-6 (nested) -->
                        </form>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection