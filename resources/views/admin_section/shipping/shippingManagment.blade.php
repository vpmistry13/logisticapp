@extends('admin_section.layouts.master')

@section('content')
<style>
    .modal-dialog {
  width: 100%;
  height: 90%;
  margin: 0;
  padding: 25px;
}

.modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
.modal-body {
    height: auto;
    overflow-y: auto;
    padding-top: 2%;
    word-wrap: break-word;
}
</style>
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  padding-top: 1px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
li{
    list-style-type:none;
}
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Jobs Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Shipping Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="{{$list_type}}">
                        <thead>
                            <tr class="design_table">
                                <th>Job ID</th>
                                <th>Customer Name</th>
                                <th>Job Name</th>
                                <th>Trucking Start Date</th>
                                <!--<th>Total Duration</th>-->
                                <th>Job Type</th>
                                <th>Material Type</th>
                                <th>Mobile Number</th>
                                <th>Loading Location</th>
                                <!--<th>Loading_start_time</th>-->
                                <th>Delivery Location</th>
                                <!--<th>Loading_end_time</th>-->
                                <th>Price Type</th>
                                <th>Price</th>
                                <th>Truck Type</th>
                                <th>No of Truck Need</th>                             
                                <th>How Long is this Job for?</th>
                                <th>Comment</th>
                                <th>Action</th>
                                <th>Created At</th>
                            </tr>
                        </thead>                        
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

</div>
<!-- /#page-wrapper -->
@endsection

<div id="assign_group" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header primary">

                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>Job Assign to Group</strong></h4>
            </div>
            <form name="form_assign_job" method="post" role="form">
                <input type="hidden" name="shipment_id_a" id="shipment_id_a" value=""/>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-4">
                                <label>Send Out:</label></div>
                            <div class="col-md-5">
                                <ul><li>
                                        <label class="container">All Truck Owners
                                        <input type="checkbox" class="form-control-sm" id="assign_to_all" name="assign_to_all" /> 
                                        <span class="checkmark"></span>
                                    </label>
                                        </li></ul>
                            </div>
                            </div>
                        <div class="col-lg-12">
                            <hr>
                            <div class="col-md-4"><label>Group List: </label></div>

                            <input type="hidden" id="admin_id" name="admin_id" value="<?php echo Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'); ?>"/>
                            
                            <div class="col-md-4"><ul id="group_list"></ul></div>
                        </div>
                        <div class="col-lg-12">
                            <hr>
                            <div class="col-md-5">
                                <label>Message Body</label>
                            </div>
                            <div class="col-md-7">
                                <textarea class="form-control" name="message_body" id="message_body"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <hr>
                            <div class="col-md-5">
                                <label> Notification Type: </label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="notification_type" name="notification_type">
                                    <option value="1">Both</option>
                                    <option value="2">Email</option>
                                    <option value="3">SMS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 justify-content-center">
                            <div class="col-md-4"><button type="button" id="assign_shipment_group"  class="btn btn-primary">Send</button></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>