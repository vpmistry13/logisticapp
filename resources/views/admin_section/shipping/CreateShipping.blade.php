@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Job</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Job 
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
                             @if (\Session::has('success'))
                                <div class="col-md-6">
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{!! \Session::get('success') !!}</li>
                                        </ul>
                                    </div>
                                </div>
                                @elseif(\Session::has('error'))
                                <div class="col-md-6">
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{!! \Session::get('error') !!}</li>
                                        </ul>
                                    </div>
                                </div>
                                @endif
                            <form id="" name="shipping_data_create" action="{{url('admin/shiping/create_job')}}" method="post" role="form">
                                
                                @csrf
                                <div class="col-lg-12">
                                    <div class="row">
                                        <input type="hidden" name="user_type" value="3"/>
                                        <div class="col-md-6">
                                            <label>Job Name</label>
                                            <input class="form-control" id="job_name" type="text" name="job_name" placeholder="Job Name" value="" required="">
                                            <span id="job_name-error"></span>
                                        </div>                                        
                                        <div class="col-md-6">
                                            <label>Material Type</label>
                                            <input class="form-control" id="material" type="text" name="material_type" placeholder="Material Type" value="" required="">
                                            <span id="material-error"></span>
                                        </div>
                                        
<!--                                        <div class="col-md-4">
                                            <label>Total Duration</label>
                                            <input class="form-control" id="total_duration" type="text" name="total_duration" placeholder="Total Duration" value="" required="">
                                            <span id="total_duration-error"></span>
                                        </div>-->
                                        
                                        <div class="col-md-4">
                                            <label>JOB Type</label>
                                            <select class="form-control" name="job_type">
                                                 <option value="DAY" >Day JOB</option>
                                                 <option value="NIGHT" >NIGHT JOB</option>
                                                 <option value="DAY/NIGHT" >DAY/NIGHT JOB</option>
                                            </select>
                                        </div>                                        
                                        <div class="col-md-4">
                                            <label>Price</label>
                                            <input class="form-control" id="total_price" type="number" name="total_price" placeholder="Total Price" value="" required="">
                                            <span id="total_price-error"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Price Type</label>
                                            <select class="form-control" name="price_type">
                                                <?php
                                                foreach ($price_type as $p) {
                                                    echo '<option value="' . $p->id . '" >' . $p->name . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        

                                  

                                    </div>
                                    <div class="row">
                                      <div class="col-md-4">
                                          <label>Job Start Date</label><br>
                                            <div class='input-group date' id='job_start_time'>
                                           
                                                <input type='text' name="trucking_start_date" class="form-control" value="" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                      <div class="col-md-4">
                                          <label>How Long is this Job for ?</label><br>
                                          <input type='text' name="how_long_job" class="form-control" value="" required=""/>
                                          
                                            </div>
                                        </div>
                                        
                                    <div class="row">
                                        <hr>
                                        <div class="col-md-6">
                                            <label>Loading Location</label>
                                            <textarea class="form-control" id="loading_location"  name="loading_location" placeholder="Loading Location"  required=""></textarea>
                                            <span id="loading_location-error"></span>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Delivery Location</label>
                                            <textarea class="form-control" id="delivery_destination"  name="delivery_location" placeholder="Delivery Location"  required=""></textarea>
                                            <span id="delivery_destination-error"></span>
                                        </div>
<!--                                        <div class="col-md-6"><br>
                                            <label>Loading Start Time</label><br>
                                            <div class='input-group date' id='loading_start_time'>
                                           
                                                <input type='text' name="loading_start_time" class="form-control" value="" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><br>
                                            <label>Loading End Time</label><br>
                                            <div class='input-group date' id='loading_end_time'>
                                           
                                                <input type='text' name="loading_end_time" class="form-control" value="" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <div class="col-md-6">
                                            <label>Truck Type</label>
                                            <select class="form-control" id="vehicle_type_id" onchange="get_select_vehicle_type(this);" multiple name="type_of_truck[]" required="">
                                                <?php foreach ($vehicle_type as $g) { ?>
                                                    <option id="<?php echo $g->name; ?>" value="<?php echo $g->id; ?>" <?php
                                                    
                                                    ?>>{{$g->name}}</option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                            <span id="vehicle-error"></span>
                                        </div>
                                        <!--<div class="col-md-6" id="multiple_no_of_truck">-->
                                        <div class="col-md-6" id="no_of_truck">
                                            <label class="col-sm-12">No of Truck</label>
                                            
                                            
                                            <div class="col-sm-12">
                                                
                                                <!--<input placeholder='' class="form-control" type="number" name="no_of_truck_need[][]" value=""/></div>-->
                                                <input placeholder='No of Truck' class="form-control" type="number" name="no_of_truck_need" value=""/></div>
                  
                                        </div>
                                            
                                                <div class="col-md-12">
                                            <hr>
<!--                                        <div class="col-md-4">
                                            <lable>Status</lable>
                                            <select class="form-control" id="status" name="status" >
                                                <option value="0" >Pending</option>
                                                <option value="1" >Complete</option>
                                                <option value="2" >Reject</option>
                                                <option value="3">On Going</option>
                                            </select>
                                        </div>-->
                                         <!--<div class="col-md-12">-->
                                            <label>Comment</label>
                                            <textarea class="form-control" id="comment" name="comment" placeholder="comment here" required=""></textarea>
                                            <span id="comment-error"></span>
                                        <!--</div>-->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <div class="col-lg-12 text-left">
                                            <button type="submit"  class="btn btn-primary">Submit</button>                                
                                        </div>
                                    </div>
                                    </div>
                                </div>                            
                            </form>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<!-- /#page-wrapper -->
@endsection