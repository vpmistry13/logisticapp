@extends('admin_section.layouts.master')

@section('content')
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Job Order</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Form 
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
                            <form id="shipping_data_edit" name="shipping_data_edit" method="post" role="form">

                                <div class="col-lg-12">
                                    <div class="row">
                                        <input type="hidden" name="user_type" value="2"/>
                                        <input type="hidden" name="id" value="{{$id}}"/>
                                        <div class="col-md-4">
                                            <label>Job Name</label>
                                            <input class="form-control" id="job_name" type="text" name="job_name" placeholder="Job Name" value="{{$shipping_data[0]->job_name}}" required="">
                                            <span id="job_name-error"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Material Type</label>
                                            <input class="form-control" id="material" type="text" name="material_type" placeholder="Material type" value="{{$shipping_data[0]->material_type}}" required="">
                                            <span id="material-error"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Total Duration</label>
                                            <input class="form-control" id="total_duration" type="text" name="total_duration" placeholder="Total Duration" value="{{$shipping_data[0]->total_duration}}" required="">
                                            <span id="total_duration-error"></span>
                                        </div>
                                        
                                        
                                        
                                        <div class="col-md-4">
                                            <label>JOB type</label>
                                            <select class="form-control" name="job_type">
                                                 <option value="DAY" <?php if($shipping_data[0]->material_type == "DAY"){ echo "selected" ;}?>>Day JOB</option>
                                                 <option value="NIGHT" <?php if($shipping_data[0]->material_type == "NIGHT"){ echo "selected" ;}?>>NIGHT JOB</option>
                                                 <option value="DAY/NIGHT" <?php if($shipping_data[0]->material_type == "DAY/NIGHT"){ echo "selected" ;}?>>DAY/NIGHT JOB</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Price type</label>
                                            <select class="form-control" name="price_type">
                                                <?php
                                                foreach ($price_type as $p) {
                                                    echo '<option value="' . $p->id . '" >' . $p->name . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Price&nbsp;</label>
                                            $<input class="form-control" id="total_price" type="number" name="total_price" placeholder="Total Price" value="{{$shipping_data[0]->total_price}}" required="">
                                            <span id="total_price-error"></span>
                                        </div>

<!--                                        <div class="col-md-4">
                                            <label>How Long Job ?</label>
                                            <input class="form-control" id="how_long_job" type="number" name="how_long_job" placeholder="How long job" value="{{$shipping_data[0]->how_long_job}}" required="">
                                            <span id="how_long_job-error"></span>
                                        </div>-->
<!--                                        <div class="col-md-4">
                                            <label>Amount Of Material</label>
                                            <input class="form-control" id="amount_of_material" type="number" name="amount_of_material" placeholder="Amount of material" value="{{$shipping_data[0]->amount_of_material}}" required="">
                                            <span id="amount_of_material-error"></span>
                                        </div>-->
                                      

<!--                                        <div class="col-md-12">
                                            <label>Comment</label>
                                            <textarea class="form-control" id="comment" name="comment" placeholder="comment here" required="">{{$shipping_data[0]->comment}}</textarea>
                                            <span id="comment-error"></span>
                                        </div>-->

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Job Start Date</label><br>
                                            <div class='input-group date' id='job_start_time'>
                                           
                                                <input type='text' name="trucking_start_date" class="form-control" value="{{$shipping_data[0]->trucking_start_date}}" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                          <label>How Long is this Job for ?</label><br>
                                          <input type='text' name="how_long_job" class="form-control" value="{{$shipping_data[0]->how_long_job}}" required=""/>
                                          
                                            </div>
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <div class="col-md-6">
                                            <label>Loading Location</label>
                                            <textarea class="form-control" id="loading_location"  name="loading_location" placeholder="Loading Location"  required="">{{$shipping_data[0]->pickup}}</textarea>
                                            <span id="loading_location-error"></span>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Delivery Location</label>
                                            <textarea class="form-control" id="delivery_destination"  name="delivery_location" placeholder="Delivery Location"  required="">{{$shipping_data[0]->destination}}</textarea>
                                            <span id="delivery_destination-error"></span>
                                        </div>
<!--                                        <div class="col-md-6"><br>
                                            <label>Loading Start Time</label><br>
                                            <div class='input-group date' id='loading_start_time'>
                                           
                                                <input type='text' name="loading_start_time" class="form-control" value="{{$shipping_data[0]->loading_start_time}}" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6"><br>
                                            <label>Loading End Time</label><br>
                                            <div class='input-group date' id='loading_end_time'>
                                           
                                                <input type='text' name="loading_end_time" class="form-control" value="{{$shipping_data[0]->loading_end_time}}" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            </div>
                                        </div>-->
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <div class="col-md-6">
                                            <label>Truck Type</label>
                                            <select class="form-control" id="vehicle_type_id" onchange="get_select_vehicle_type(this);" multiple name="type_of_truck[]" required="">
                                                <?php foreach ($vehicle_type as $g) { ?>
                                                    <option id="<?php echo $g->name; ?>" value="<?php echo $g->id; ?>" <?php
                                                    if (in_array($g->id, explode(',', $shipping_data[0]->type_of_truck))) {
                                                        echo 'selected';
                                                    }
                                                    ?>>{{$g->name}}</option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                            <span id="vehicle-error"></span>
                                        </div>
                                        <!--<div class="col-md-6" id="multiple_no_of_truck">-->
                                        <div class="col-md-6" id="no_of_truck">
                                            <!--<label class="col-sm-12">No of Truck by Truck Type</label>-->
                                            <label class="col-sm-12">No of Truck</label>
                                            
                                            <?php // if(is_array(@unserialize($shipping_data[0]->no_of_truck_need))){ foreach(unserialize($shipping_data[0]->no_of_truck_need) as $k => $v){?>
                                            <div class="col-sm-12">
                                                
                                                <input placeholder='No of truck' class="form-control" type="number" name="no_of_truck_need" value="<?php echo $shipping_data[0]->no_of_truck_need; ?>"/></div>
                                                <!--<input placeholder='<?php// echo $k; ?>' class="form-control" type="number" name="no_of_truck_need[<?php //echo $k;?>][]" value="<?php //echo $v[0]; ?>"/></div>-->
                                            <?php // }} ?>
                                            
                                        </div>
                                              <div class="col-md-12">
                                                  <hr>
                                            <label>Comment</label>
                                            <textarea class="form-control" id="comment" name="comment"  placeholder="comment here" required="">{{@$shipping_data[0]->comment}}</textarea>
                                            <span id="comment-error"></span>
                                        </div>
                                                <div class="col-md-12">
                                                  
                                            <hr>
                                            
                                        <div class="col-md-4">
                                            <lable>Status</lable>
                                            <select class="form-control" id="status" name="status" >
                                                <option value="0" <?php if (@$shipping_data[0]->status == '0') {
                                                            echo 'selected';
                                                        } ?>>Pending</option>
                                                <option value="4" <?php if (@$shipping_data[0]->status == '1') {
                                                            echo 'selected';
                                                        } ?>>Completed</option>
                                                <option value="2" <?php if (@$shipping_data[0]->status == '2') {
                                                            echo 'selected';
                                                        } ?>>Reject</option>
                                                <option value="3" <?php if (@$shipping_data[0]->status == '3') {
                                                            echo 'selected';
                                                        } ?>>On Going</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <lable>Customer Details:</lable>
                                            <div class="form-group">
                                                <a class="btn btn-warning" href="{{url('admin/customer/EditAccount/'.base64_encode($shipping_data[0]->user_id))}}">Edit Customer Details</a>

                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <hr>
                                        <div class="col-lg-12 text-left">
                                            <button type="button" id="shipping_edit_submit" class="btn btn-primary">Save</button>                                
                                        </div>
                                    </div>

                                </div>                            
                            </form>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<!-- /#page-wrapper -->
@endsection