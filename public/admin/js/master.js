/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = APP_URL + "/";

$(document).ready(function () {
    $('#dataTables-example').DataTable({
        responsive: true,
        bPaginate: false
    });
});
$('#click_to_submit').on('click', function () {
    $('#click_to_submit').attr("disabled", "disabled");
    if (validate() == true) {
        var user_type = $('#user_type').val();
        var driving_licence = $('#doucuments_1_change').val();
        if(user_type == 2 && driving_licence == ''){
            $('#click_to_submit').removeAttr("disabled");
            return alert('Driving Licence is required.');
        }
        var data = document.getElementById('post_data');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/addAccount",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                $('#click_to_submit').removeAttr("disabled");
                var data = jQuery.parseJSON(response);
                if (data.code === 1) {
                    $("#post_data")[0].reset();
                    $('#profile-upload').removeAttr("style");
                    $('#document_1').removeAttr("style");
                    $('#document_2').removeAttr("style");
                    $('#document_3').removeAttr("style");
                    $.toast({
                        heading: 'Success',
                        text: data.message,
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                } else
                {
                    $.toast({
                        heading: 'Error',
                        text: data.message,
                        icon: 'error',
                        position: 'top-center'
                    })
                }

            }
        });
    }else{
        $('#click_to_submit').attr("disabled", "disabled");
    }

});
$('#vehicle_data_submit').on('click', function () {

    if (validate() == true) {
        var data = document.getElementById('vehicle_data');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/addVehicle",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                //  $("#post_data")[0].reset();
                $.toast({
                    heading: 'Success',
                    text: 'Vehicle Added',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }

});


$('#click_to_edit').on('click', function () {
    if (editValidate() == true) {
        var data = document.getElementById('post_data_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editAccount",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                 var data = jQuery.parseJSON(response);
                if (data.code === 1) {
                  //  $("#post_data_edit")[0].reset();
                   // $('#profile-upload').removeAttr("style");
                    $.toast({
                        heading: 'Success',
                        text: data.message,
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                } else
                {
                    $.toast({
                        heading: 'Error',
                        text: data.message,
                        icon: 'error',
                        position: 'top-center'
                    })
                }
            }
        });
    }
});
$('#vehicle_data_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('vehicle_data_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editVehicle",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Vehicle Updated',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#vehicle_body_data_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('vehicle_body_data');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/addBodytype",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Body type Added',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#good_type_data_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('good_type_data');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/addGoodtype",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Body type Added',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#vehicle_body_data_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('vehicle_body_data_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editBodytype",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Body type Added',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#goods_type_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('goods_type_edit');
        var form_data = new FormData(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editGoodstype",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {           
                $.toast({
                    heading: 'Success',
                    text: 'Goods type Added',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#profile_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('profile_data_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editProfile",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Profile Updated...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#shipping_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('shipping_data_edit');
        var form_data = new FormData(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editOrderShipping",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                $.toast({
                    heading: 'Success',
                    text: 'Job Updated...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#notification_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('notification_edit');
        var form_data = new FormData(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editNotification",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                $.toast({
                    heading: 'Success',
                    text: 'Notification Updated...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
$('#setting_edit_submit').on('click', function () {
    if (validate() == true) {
        var data = document.getElementById('setting_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/editSettings",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                $.toast({
                    heading: 'Success',
                    text: 'Updated...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
});
function vehicle_data_remove(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/deleteVehicle/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#vehicle_management').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Vehicle Removed',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
function order_shipping_remove(id) {
    
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/remove_shipping_order/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#order_shipping_list').DataTable().draw(false);
               $('#order_shipping_list_all').DataTable().draw(false);
               $('#order_shipping_active_list').DataTable().draw(false);
               $('#order_shipping_pending_list').DataTable().draw(false);
               $('#order_shipping_upcoming_list').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Removed',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
$("input[name='group_list[]']").on('change',function(){
   if ($(this).is(':checked')) {
    alert('chceked');
  }
  alert('outer');
});

function order_shipping_allow(id) {
    if (confirm('Are You sure ?')) {
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/get_group_list",
            cache: false,
            processData: false,
            contentType: false,
            type: 'GET',
            success: function (response) {
               var res = JSON.parse(response);
               if(res.code == 1){
               $('#assign_group').modal('show');
               $('#shipment_id_a').val(id);
               $('#group_list').html('');
               $.each(res.data,function (k,v){
                   $('#group_list').append('<li><input class="groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> '+v['group_name']+'</li>');
//                     $('#group_list').append('<li><label class="container">'+v['group_name']+'<input class="form-control-sm groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <span class="checkmark"></span></label></li>');
               });
                $.toast({
                    heading: 'Success',
                    text: 'Group List Loaded...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
               }else{
                 $.toast({
                    heading: 'Error',
                    text: 'No Group Found',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })  
               }
            }
        });
    }
}
var assign_to_all;
$('#assign_to_all').change(function(){
   var groups = $(".groups");
   if($(this). prop("checked") == true){
        assign_to_all = 1;        
        groups.prop('disabled', !groups.prop('disabled'));
   }else{
       assign_to_all = 0;
        groups.removeAttr('disabled',!groups.removeAttr('disabled'));
   }
   
});


$('#assign_shipment_group').on('click', function () {
      var message_body = $('#message_body').val();
    if (assign_to_all == 1) {
            var notification_type = $('#notification_type').val();                     
            var data = document.getElementById('form_assign_job');
            var form_data = new FormData(data);
            form_data.append('assign_to_all',assign_to_all)
            form_data.append('notification_type', notification_type);
            form_data.append('message_body',message_body);
            var id = $('#shipment_id_a').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url + "admin/Ajax_controller/shiping_status/" + id + "/" + 1,
                data: form_data,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (response) {
                    $('#assign_group').modal('hide');
                    $('#order_shipping_pending_list').DataTable().draw(false);
                    $('#order_shipping_upcoming_list').DataTable().draw(false);
                    $.toast({
                        heading: 'Success',
                        text: 'Shiping has been allowed.',
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                }
            });
    } else {
        if ($('.groups:checkbox:checked').length != 0) {
            var groups = [];
            $.each($("input[name='group_list[]']:checked"), function () {
                groups.push($(this).val());
            });
            var notification_type = $('#notification_type').val();
            var data = document.getElementById('form_assign_job');
            var form_data = new FormData(data);
            form_data.append('groups_ids', JSON.stringify(groups));
            form_data.append('notification_type', notification_type);
            form_data.append('message_body',message_body);
            var id = $('#shipment_id_a').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url + "admin/Ajax_controller/shiping_status/" + id + "/" + 1,
                data: form_data,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (response) {
                    $('#assign_group').modal('hide');
                    $('#order_shipping_pending_list').DataTable().draw(false);
                    $('#order_shipping_upcoming_list').DataTable().draw(false);
                    $.toast({
                        heading: 'Success',
                        text: 'Shiping has been allowed.',
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                }
            });
        } else {
            alert('Please select atleast 1 group!');
            return false;
        }
    }
});

$('#order_shipping_list_all').on('change','tr','select', function () {
   
     //alert('on change');
            var job_status = $(this,'select').find(":selected").val();
            var job_id = $(this,'#job_id').find("#job_id").val();
            
    $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url + "admin/Ajax_controller/shiping_status_admin/" + job_id + "/" + job_status,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (response) {
                   // $('#order_shipping_list_all').DataTable().draw(false);
                    $.toast({
                        heading: 'Success',
                        text: 'Job Status Changed Successfully.',
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                }
            });
    
});

$('#add_group').on('shown.bs.modal',function(){
   $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/get_group_list",
            cache: false,
            processData: false,
            contentType: false,
            type: 'GET',
            success: function (response) {
                
               var res = JSON.parse(response);
               if(res.code == 1){
               $('#group_list').html('');
               $('.models_list').html('');
               $.each(res.data,function (k,v){
                   $('#group_list').append('<li><input class="groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <label>'+v['group_name']+'</label> &nbsp;<a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a></li>');
                   
//                   $('#group_list').append('<li><label class="container_check">'+v['group_name']+'&nbsp; <a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a> <input class="form-control-sm groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <span class="checkmark"></span></label></li>');
                   
                   
//                   $('#group_list').append('<li><input class="groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <label>'+v['group_name']+'</label> &nbsp;<a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a></li>');
                   if(v.inList){
                       
                        $('.models_list').append('<div class="container" id="table_'+v['id']+'" style="display:none;"><hr><center><h3>Truckers in Group : <strong>'+v['group_name']+'</strong></h3></center><table class="table table-striped"><thead><th>Full Name</th><th>Company Name</th><th>Email</th></thead><tbody></tbody></table></div>');
                        $.each(v.inList, function (ku,u){
                            $('.models_list #table_'+v['id']+' .table tbody').append('<tr><td>'+u.first_name+' '+u.last_name+'</td><td>'+u.company_name+'</td><td>'+u.email+'</td></tr>');
                        });
                    }
               });
               
               $('#group_list a').on('click', function(){
                         $('.models_list .container').hide();
                         $('#table_'+ this.id).show();
                 });
               
                $.toast({
                    heading: 'Success',
                    text: 'Group List Loaded...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
               }else{
                 $.toast({
                    heading: 'Error',
                    text: 'No Group Found',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })  
               }
            }
        }); 
});


$('#send_message').on('shown.bs.modal',function(){
    $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/get_group_list",
            cache: false,
            processData: false,
            contentType: false,
            type: 'GET',
            success: function (response) {
                
               var res = JSON.parse(response);
               if(res.code == 1){
               $('#group_list_message').html('');
               $('.models_list_sms').html('');
               $.each(res.data,function (k,v){
                   $('#group_list_message').append('<li><input class="groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <label>'+v['group_name']+'</label> &nbsp;<a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a></li>');
                   
//                   $('#group_list_message').append('<li><label class="container_check">'+v['group_name']+'&nbsp; <a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a> <input class="form-control-sm groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <span class="checkmark"></span></label></li>');
                   
                   
//                   $('#group_list_message').append('<li><input class="groups" type="checkbox" name="group_list[]" value="'+v['id']+'"/> <label>'+v['group_name']+'</label> &nbsp;<a  class="btn btn-primary btn-xs pull-right" id="'+v['id']+'"><i class="fa fa-eye"></i></a></li>');
                   if(v.inList){
                       
                        $('.models_list_sms').append('<div class="container" id="table_'+v['id']+'" style="display:none;"><hr><center><h3>Truckers in Group : <strong>'+v['group_name']+'</strong></h3></center><table class="table table-striped"><thead><th>Full Name</th><th>Company Name</th><th>Email</th></thead><tbody></tbody></table></div>');
                        $.each(v.inList, function (ku,u){
                            $('.models_list_sms #table_'+v['id']+' .table tbody').append('<tr><td>'+u.first_name+' '+u.last_name+'</td><td>'+u.company_name+'</td><td>'+u.email+'</td></tr>');
                        });
                    }
               });
               
               $('#group_list_message a').on('click', function(){
                         $('.models_list_sms .container').hide();
                         $('#table_'+ this.id).show();
                 });
               
                $.toast({
                    heading: 'Success',
                    text: 'Group List Loaded...',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
               }else{
                 $.toast({
                    heading: 'Error',
                    text: 'No Group Found',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })  
               }
            }
        }); 
});
function order_shipping_complete(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/shiping_status/" + id +"/" + 4,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#order_shipping_active_list').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Mark as completed.',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
function order_shipping_reject(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/shiping_status/" + id +"/" + 2,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#order_shipping_pending_list').DataTable().draw(false);
                $.toast({
                    heading: 'Reject',
                    text: 'Shiping has been rejected.',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
function user_remove(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/deleteUserRemove/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'GET',
            success: function (response) {
                $.toast({
                    heading: 'Success',
                    text: 'User Removed',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
                $('#customer_managment').DataTable().draw(false);
                $('#driver_managment').DataTable().draw(false);
            }
        });
    }
}
function user_verify(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/verifyTruckOwner/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'GET',
            success: function (response) {
                $.toast({
                    heading: 'Success',
                    text: 'User Verified',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
                $('#customer_managment').DataTable().draw(false);
                $('#driver_managment').DataTable().draw(false);
            }
        });
    }
}
function vehicle_body_data_remove(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/deleteBodytype/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                $('#group_managment').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Group Removed',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
function good_type_data_remove(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/deleteBodytype/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#vehicle_goods_type_wrapper').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Vehicle Removed',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}
function document_verify(id) {
    if (confirm('Are You sure ?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/documentVerify/" + id,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
               $('#driver_managment').DataTable().draw(false);
                $.toast({
                    heading: 'Success',
                    text: 'Driver Documentation Verified.',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
            }
        });
    }
}


$(document).ready(function () {
    $('#password_changing').hide();
    $("#change_password_hide").click(function () {
    });
    $("#change_password").click(function () {
        var value = $('#change_password').val();
        if (value == 1) {
            $('#chang_password').val(2);
            $('#change_password').html('<span class="fa fa-minus">&nbsp;Change Password</span>');
            document.getElementById('change_password').value = 2;
            $("#password_changing").show();
            $('#password_changing').html('<fieldset><div class="form-group"><label for="password">Password</label><input class="form-control" id="password" name="password"  type="password" placeholder="password" required=""><span id="password-error-first"></span></div><div class="form-group"><label for="confirm_password">Confirm Password</label><input class="form-control" id="confirm_password" type="password" placeholder="confirm password" required=""><span id="password_error"></span></div></fieldset>');
        } else
        {
            $('#change_password').html('<span class="fa fa-plus">&nbsp;Change Password</span>');
            document.getElementById('change_password').value = 1;
            $('#password_changing').html('');
        }
    });
});

$('#full_name').on('change', function () {
    $('#full_name-error').hide();
});
$('#user_city').on('change', function () {
    $('#city-error').hide();
});
$('#email').on('change', function () {
    $('#email-error').hide();
});
$('#password').on('change', function () {
    $('#password-error-first').hide();
});
$('#confirm_password').on('change', function () {
    $('#password_error').hide();
});
$('#vehicle_number').on('change', function () {
    $('#vehicle-error').hide();
});
function validate() {

    if ($('#first_name').val() == '') {
        $('#first_name-error').html('<span class="btn btn-danger btn-xs">required first name</span>');

    }else{
        $('#first_name-error').html('');
    }
    if ($('#last_name').val() == '') {
        $('#last_name-error').html('<span class="btn btn-danger btn-xs">required last name</span>');

    }else{
        $('#last_name-error').html('');
    }
    if ($('#company_name').val() == '') {
        $('#company_name-error').html('<span class="btn btn-danger btn-xs">required company name</span>');

    }else{
        $('#company_name-error').html('');
    }
    if ($('#mobile_number').val() == '') {
        $('#mobile_number-error').html('<span class="btn btn-danger btn-xs">required mobile number</span>');

    }else{
        $('#mobile_number-error').html('');
    }
    if ($('#address').val() == '') {
        $('#address-error').html('<span class="btn btn-danger btn-xs">required address</span>');

    }else{
        $('#address-error').html('');
    }
    if ($('#email').val() == '') {
        $('#email-error').html('<span class="btn btn-danger btn-xs">required email</span>');

    }else{
        $('#email-error').html('');
    }
    var p = $('#password').val();
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    var minNumberofChars = 7;
    var maxNumberofChars = 16;
//    if(p){
//    if(p.length < minNumberofChars || p.length > maxNumberofChars){
//        alert('Password Pattern not match Please include 1 upercase, 1 Lower case, 1 Special Character, Min 8 Character,Max 16 Character allowed');
//        $('#password-error-first').html('<span class="btn btn-danger btn-xs">Password Pattern not match</span>');
//        $('#password').focus();
//        return false;
//    }
//    if(!regularExpression.test(p)) {
//        alert("password should contain atleast one number and one special character");
//         $('#password').focus();
//        return false;
//    }}
    if ($('#password').val() == '') {
        
        
        $('#password-error-first').html('<span class="btn btn-danger btn-xs">required password</span>');

    }else{
        $('#password-error-first').html('');
    }
    if ($('#confirm_password').val() == '') {
        $('#password_error').html('<span class="btn btn-danger btn-xs">required confirmed password</span>');

    }else{
        $('#password_error').html('');
    }
     
    if (($('#first_name').val() != '') &&($('#last_name').val() != '') && ($('#address').val() != '') && ($('#email').val() != '') && ($('#password').val() != '') && ($('#confirm_password').val() != '') && ($('#mobile_number').val() != '')) {
        return true;
    }else{
          $('#click_to_submit').attr("disabled", "disabled");
    }

}
function editValidate() {

    if ($('#full_name').val() == '') {
        $('#full_name-error').html('<button class="btn btn-danger">required full name</button>');

    }
    if ($('#user_city').val() == '') {
        $('#city-error').html('<button class="btn btn-danger">required user city</button>');

    }
    if ($('#email').val() == '') {
        $('#email-error').html('<button class="btn btn-danger">required email</button>');

    }
    if ($('#password').val() == '') {
        $('#password-error-first').html('<button class="btn btn-danger">required password</button>');

    }
    if ($('#confirm_password').val() == '') {
        $('#password_error').html('<button class="btn btn-danger">required confirmed password</button>');

    }
    if ($('#vehicle_number').val() == '') {
        $('#vehicle-error').html('<button class="btn btn-danger">required vehicle number</button>');
    }
    if (($('#full_name').val() != '') && ($('#user_city').val() != '') && ($('#email').val() != '')) {
        return true;
    }

}
$('#confirm_password').on('keyup', function () {
    var password = $('#password').val();
    var confirm_password = $('#confirm_password').val();

    if (password === confirm_password) {
        $('#password').addClass('green');
        $('#confirm_password').addClass('green');
        $('#password_error').hide();
        $('#click_to_submit').removeAttr("disabled");
    } else
    {
        $('#password_error').html('<button class="btn btn-danger">password mismatch</button>');
        $('#password_error').show();
        $('#click_to_submit').attr("disabled", "disabled");
        $('#confirm_password').removeClass('green');
        $('#confirm_password').addClass('red');
    }


});

// for chart

//window.onload = function () {
//
//    var dataPoints = [];    
//
//    var options = {
//        animationEnabled: true,
////        backgroundColor: "#F5DEB3",
//	theme: "light2",
//
//
//        title: {
//            text: "Daily Orders"
//        },
//        axisX: {
//            valueFormatString: "DD MMM YYYY",
//        },
//        axisY: {
//            title: "Orders",
//            titleFontSize: 24,
//            includeZero: false
//        },
//        data: [{
//                type: "splineArea",
//                yValueFormatString: "#,###.##",
//                dataPoints: dataPoints,
//                color: "rgba(104,179,45,1)",
//            }]
//    };
//    
//
//    function addData(data) {
//        for (var i = 0; i < data.length; i++) {
//            dataPoints.push({
//                x: new Date(data[i].date),
//                y: data[i].units
//            });
//        }
//        $("#chartContainer").CanvasJSChart(options);
//
//    }
//     $.getJSON(url + "admin/Ajax_controller/sales", addData);
//     
//    var dataPoints_revenue = [];
//    var options_revenue = {
//        animationEnabled: true,
////        backgroundColor: "#F5DEB3",
//	theme: "light2",
//
//
//        title: {
//            text: "Daily Revenue"
//        },
//        axisX: {
//            valueFormatString: "DD MMM YYYY",
//        },
//        axisY: {
//            title: "USD",
//            titleFontSize: 24,
//            includeZero: false
//        },
//        data: [{
//                type: "splineArea",
//                yValueFormatString: "$#,###.##",
//                dataPoints: dataPoints_revenue,
//                color: "rgba(104,179,45,1)",
//            }]
//    };
//    function addRevenueData(data) {
//        for (var i = 0; i < data.length; i++) {
//            dataPoints_revenue.push({
//                x: new Date(data[i].date_revenue),
//                y: data[i].amount
//            });
//        }
//        $("#chartContainer1").CanvasJSChart(options_revenue);
//
//    }
//   
//    $.getJSON(url + "admin/Ajax_controller/sales_revenue", addRevenueData);
//
//}

$(document).ready(function () {
    
    $('#customer_managment').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/customer/list_json',
        columnDefs: [
//            {targets: 1,
//                render: function (data) {
//                    if (data == 'default.png') {
//                        return '<img src="' + url + '/images/default/customer_icon.png" width="50px"/>';
//                    } else
//                    {
//                        return '<img src="' + data + '" width="50px">';
//                    }
//
//                }
//            },
        ],
        "order": [[0, "desc"]],
        columns: [
//            {data: 'id', name: 'id'},
//            {data: 'avatar', name: 'avatar'},
            {data: 'full_name', name: 'full_name'},
            {data: 'company_name', name: 'company_name'},
            {data: 'email', name: 'email'},
            {data: 'address', name: 'address'},
            {data: 'mobile_number', name: 'mobile_number'},
            {data: 'office_number', name: 'office_number'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    var selected = [];
    var driver_managment = $('#driver_managment').DataTable({
        
       
       "scrollY":        "70vh",
       scrollX : true,
        processing: true,
        "initComplete": function(settings, json) {
        $('.dataTables_scrollBody .design_table').css({visibility:'collapse'});
//         $('.dataTables_scrollBody .design_table').css('display','none');
        },
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },   
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/driver/list_json',
        rowCallback: function( row, data ) {
            if ( $.inArray(data.id, selected) !== -1 ) {
                $(row).addClass('selected');
            }
            
        },
        rowId : 'id',
        aLengthMenu: [
            [10, 25, 100, 200, -1],
            [10, 25, 100, 200, "All"]
        ],
        columnDefs: [
//            { "width": "15%" ,targets: 0},
//            { "width": "20%" ,targets: 1},
//            { "width": "20%" ,targets: 2},
//            { "width": "20%" ,targets: 3},
//            { "width": "20%" ,targets: 4},
//            { "width": "10%" ,targets: 5},
//            {targets: 1,
//                render: function (data) {
//                    if (data == 'default.png') {
//                        return '<img src="' + url + '/images/default/customer_icon.png" width="50px"/>';
//                    } else
//                    {
//                        return '<img src="' + data + '" width="50px">';
//                    }
//
//                }
//            },
//            {targets: 6,
//                render: function (data) {
//                    if (data == 'empty') {
//                        return '<span data-balloon="size: xs" data-balloon-pos="up" class="dn color-inherit link hover-indigo"><svg aria-hidden="true" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 912 512" class="svg-inline--fa fa-image fa-w-16 fa-xs"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z" class=""></path></svg></span>';
//                    } else
//                    {
//                        return '<img src="' + data + '" width="50px">';
//                    }
//
//                }
//            },
//            {targets: 7,
//                render: function (data) {
//                    if (data == 'empty') {
//                        return '<span data-balloon="size: xs" data-balloon-pos="up" class="dn color-inherit link hover-indigo"><svg aria-hidden="true" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 912 512" class="svg-inline--fa fa-image fa-w-16 fa-xs"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z" class=""></path></svg></span>';
//                    } else
//                    {
//                        return '<img src="' + data + '" width="50px">';
//                    }
//
//                }
//            },
            {targets: 8,
                render: function (data) {
                    if (data == 'empty' || data == null) {
                        return '<span data-balloon="size: xs" data-balloon-pos="up" class="dn color-inherit link hover-indigo"><svg aria-hidden="true" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 912 512" class="svg-inline--fa fa-image fa-w-16 fa-xs"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z" class=""></path></svg></span>';
                    } else
                    {
                        return '<a download="'+data+'" target="_blank" href="'+data+'"><img src="' + data + '" width="50px"></a>&nbsp;<a download="'+data+'" target="_blank" href="'+data+'"><i class="fa fa-download"></i></a>';
                    }

                }
            },
            {targets: 9,
                render: function (data) {
                    if (data == 'empty' || data == null) {
                        return '<span data-balloon="size: xs" data-balloon-pos="up" class="dn color-inherit link hover-indigo"><svg aria-hidden="true" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 912 512" class="svg-inline--fa fa-image fa-w-16 fa-xs"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z" class=""></path></svg></span>';
                    } else
                    {
                        return '<a download="'+data+'" target="_blank" href="'+data+'"><img src="' + data + '" width="50px"></a>&nbsp;<a download="'+data+'" target="_blank" href="'+data+'"><i class="fa fa-download"></i></a>';
                    }

                }
            },
            {targets: 10,
                render: function (data) {
                    if (data == 'empty' || data == null) {
                        return '<span data-balloon="size: xs" data-balloon-pos="up" class="dn color-inherit link hover-indigo"><svg aria-hidden="true" data-prefix="far" data-icon="image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 912 512" class="svg-inline--fa fa-image fa-w-16 fa-xs"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm-6 336H54a6 6 0 0 1-6-6V118a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v276a6 6 0 0 1-6 6zM128 152c-22.091 0-40 17.909-40 40s17.909 40 40 40 40-17.909 40-40-17.909-40-40-40zM96 352h320v-80l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L192 304l-39.515-39.515c-4.686-4.686-12.284-4.686-16.971 0L96 304v48z" class=""></path></svg></span>';
                    } else
                    {
                        return '<a download="'+data+'" target="_blank" href="'+data+'"><img src="' + data + '" width="50px"></a>&nbsp;<a download="'+data+'" target="_blank" href="'+data+'"><i class="fa fa-download"></i></a>';
                    }

                }
            },
            
        ],
//        createdRow: function(row, data, dataIndex){
//            // Initialize custom control
//            $('.driver_rating', row).raty({
//                half: true,
//                starHalf: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-half.png',
//                starOff: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-off.png',
//                starOn: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-on.png',
//                score: function(){
//                    return $(this).data('score');
//                },
//                click: function(score){
//                    $(this).data('score', score);
//                }
//            });
//         },
        "order": [[0, "desc"]],
        columns: [
//            {data: 'id', name: 'id'},
//            {data: 'avatar', name: 'avatar'},
            {data: 'full_name', name: 'full_name'},
            {data: 'company_name', name: 'company_name'},
            {data: 'email', name: 'email'},
            {data: 'address', name: 'address'},
            {data: 'mobile_number', name: 'mobile_number'},            
//            {data: 'office_number', name: 'office_number'},            
            {data: 'type', name: 'type'},            
            {data: 'no_of_truck', name: 'no_of_truck'},
            {data: 'cdl', name: 'cdl'},
            {data: 'insurance_image', name: 'insurance_image'},
            {data: 'driving_licence_image', name: 'driving_licence_image'},
//            {data: 'avg_rate', name: 'avg_rate'},
            {data: 'wq_image', name: 'wq_image'},
//            {data: 'vehicle_number', name: 'vehicle_number'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
     $('#driver_managment tbody').on('click', 'tr', function () {
        $('#selected_ids').html('');
        $('#add_to_existing').html('');
        $('#send_bulk_msg').html(''); 
        var id = this.id;
        var index = $.inArray(id, selected);
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
        if(selected.length > 0)
        {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/get_emails_by_id",
            type: 'POST',
            data : {'email_ids' : selected},
            success: function (response) {
              var response = JSON.parse(response);
              $('#selected_ids').html('');
              $('#add_to_existing').html('');
              $('#send_bulk_msg').html('');
              var li_append = [];
                 li_append.push('<table class="table table-striped design_custom">');
                 li_append.push('<th>Full Name</th><th>Company Name</th><th>Email</th><th>Remove</th>');
              $.each(response.data, function(k,v){
                 li_append.push('<tr class="li_'+v.id+'"><td >'+v.first_name+' '+v.last_name+'</td><td>'+v.company_name+'</td><td >'+v.email+'</td><td><a class="" id="'+v.id+'" href="javascript:void('+v.id+')"><i class="fa fa-remove"></i></a></td></tr>'); 
//                 li_append.push('<tr><td><li class="li_'+v.id+'">'+v.email+'<a class="pull-right" id="'+v.id+'" href="javascript:void('+v.id+')"><i class="fa fa-remove"></i></a></li></td></tr>'); 
//                 li_append.push('<li class="li_'+v.id+'">'+v.email+'</li>'); 
                 
              });
              li_append.push('</table>');
              var apply_value = li_append.toString()
              $('#selected_ids').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
              $('#add_to_existing').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
              $('#send_bulk_msg').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
            }
        });        
     }
        $(this).toggleClass('selected');
    } );
//    $('#select-all:checkbox').each(function(){
//        if(this.checked){
//            
//        }else{
//            alert('off');
//        }
//    });

    $('#select-all').on('click', function(){
          
            var rows = driver_managment.rows({ 'search': 'applied' }).nodes();
                 
            $.each(rows, function(k,v){
              
               
                   var id = v.id;
                   var index = $.inArray(id, selected);
                      $('#select-all:checkbox').each(function(){    
                    if(this.checked){
                        $('#driver_managment tbody #'+id).addClass('selected');
                        selected.push( id );
                    }else{
                        selected = [];
                        $('#driver_managment tbody #'+id).removeClass('selected');
                    }
                }); 
             
          
             });
              if(selected.length > 0)
                        {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: url + "admin/Ajax_controller/get_emails_by_id",
                            type: 'POST',
                            data : {'email_ids' : selected},
                            success: function (response) {
                              var response = JSON.parse(response);
                              $('#selected_ids').html('');
                              $('#add_to_existing').html('');
                              $('#send_bulk_msg').html('');
                              var li_append = [];
                                 li_append.push('<table class="table table-striped design_custom">');
                                 li_append.push('<th>Full Name</th><th>Company Name</th><th>Email</th><th>Remove</th>');
                              $.each(response.data, function(k,v){
                                 li_append.push('<tr class="li_'+v.id+'"><td >'+v.first_name+' '+v.last_name+'</td><td>'+v.company_name+'</td><td >'+v.email+'</td><td><a class="" id="'+v.id+'" href="javascript:void('+v.id+')"><i class="fa fa-remove"></i></a></td></tr>'); 
                //                 li_append.push('<tr><td><li class="li_'+v.id+'">'+v.email+'<a class="pull-right" id="'+v.id+'" href="javascript:void('+v.id+')"><i class="fa fa-remove"></i></a></li></td></tr>'); 
                //                 li_append.push('<li class="li_'+v.id+'">'+v.email+'</li>'); 

                              });
                              li_append.push('</table>');
                              var apply_value = li_append.toString()
                              $('#selected_ids').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
                              $('#add_to_existing').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
                              $('#send_bulk_msg').append('<ul class="col-md-12">'+apply_value.replace(/,/g,"")+'</ul>');
                            }
                        });        
                     }else{
                        $('#selected_ids').html('');
                        $('#add_to_existing').html('');
                        $('#send_bulk_msg').html('');
                     } 
//            $('#driver_managment', rows).prop('selected');
        });

//        $('#driver_managment tbody').on('change', 'input[type="checkbox"]', function(){
//      
//            if(!this.checked){
//                var el = $('#select-all').get(0);           
//                if(el && el.checked && ('indeterminate' in el)){
//                    el.indeterminate = true;
//                }
//            }
//        });
    
    $('#selected_ids').on('click','a', function(){        
        var id = this.id;
        $("#selected_ids .li_"+id).remove();
        $("#add_to_existing .li_"+id).remove();
        $("#send_bulk_msg .li_"+id).remove();
        var index = $.inArray(id, selected);
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
         
        $('#driver_managment #'+this.id).removeClass('selected');
    });
    $('#add_to_existing').on('click','a', function(){        
        var id = this.id;
        $("#selected_ids .li_"+id).remove();
        $("#add_to_existing .li_"+id).remove();
        $("#send_bulk_msg .li_"+id).remove();
        var index = $.inArray(id, selected);
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
         
       $('#driver_managment #'+this.id).removeClass('selected');
    });
    $('#send_bulk_msg').on('click','a', function(){        
        var id = this.id;
        $("#selected_ids .li_"+id).remove();
        $("#add_to_existing .li_"+id).remove();
        $("#send_bulk_msg .li_"+id).remove();
        var index = $.inArray(id, selected);
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
         
       $('#driver_managment #'+this.id).removeClass('selected');
    });
    $('#saveGroup').on('click', function(){
        var group_name = $('#group_name').val();
        var admin_id = $('#admin_id').val();
        if((selected.length) == 0){
           return alert('Select Atleast One Truckowner.');
        }
        if(group_name == ''){
            $('#group_name').focus();
            $('#group_name').css('border-color','red');
            return $('#group_name').attr('placeholder','This field is required.');
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/save_group",
            type: 'POST',
            data : {'email_ids' : selected,'group_name' : group_name,'admin_id' : admin_id},
            success: function (response) {
            $.toast({
                    heading: 'Success',
                    text: 'Group created successfully..!',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
              selected = [];
              $('#group_name').val('');
              $('#create_group').modal('hide');
              $('#selected_ids').html('');
              $('#driver_managment').DataTable().draw(false);
            }
        });        
    });
    $('#add_to_existing_group').on('click', function(){
        var group_id = [];
        
        $("input[name='group_list[]']:checked").each(function ()
        {
            group_id.push(parseInt($(this).val()));
            
        });
        var admin_id = $('#admin_id').val();
        if((selected.length) == 0){
           return alert('Select Atleast One Truckowner.');
        }
        if(group_id.length == 0){
            return alert("Please create group first...!");
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/add_to_group",
            type: 'POST',
            data : {'email_ids' : selected,'group_id' : group_id,'admin_id' : admin_id},
            success: function (response) {
            $.toast({
                    heading: 'Success',
                    text: 'Group updated successfully..!',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
              selected = [];
              $('#add_group').modal('hide');
              $('#selected_ids').html('');
              $('#add_to_existing').html('');
              $('#driver_managment').DataTable().draw(false);
            }
        });        
    });
    $('#send_notification_bulk').on('click', function(){
        
        var admin_id = $('#admin_id').val();
        var group_id = [];
        
        $("input[name='group_list[]']:checked").each(function ()
        {
            group_id.push(parseInt($(this).val()));
            
        });
        var message_subject = $('#message_subject').val();
        var message_body = $('#message_body').val();
        var notification_type = $('#notification_type').val();
        if(group_id.length ==0){
            if((selected.length) == 0){
               return alert('Select Atleast One Truckowner.');
            }
        }
        if(message_subject == ""){
            $('#message_subject').focus();
            return $('#message_subject').css('border','2px solid red');
        }
        if(message_body == ""){
            $('#message_body').focus();
            return $('#message_body').css('border','2px solid red');
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/send_message_bulk",
            type: 'POST',
            data : {'email_ids' : selected,'message_subject':message_subject,'message_body': message_body,'admin_id' : admin_id,'notification_type' : notification_type,'group_id' : group_id},
            success: function (response) {
                $('#send_message').modal('hide');
                $('#message_body').val('');
                $('#message_subject').val('');
                $('#message_subject').css('');
                $('#message_body').css('');
            $.toast({
                    heading: 'Success',
                    text: 'Message has been send successfully.!',
                    showHideTransition: 'slide',
                    icon: 'success',
                    position: 'top-center'
                })
              selected = [];
              $('#selected_ids').html('');
              $('#add_to_existing').html('');
              $('#driver_managment').DataTable().draw(false);
            }
        });        
    });
     var emails_array = "";
    $('#group_managment').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },   
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/group/list_json',
//        rowCallback: function( row, data ) {
//            if ( $.inArray(data.id, selected) !== -1 ) {
//                $(row).addClass('selected');
//            }
//            
//        },
        rowId : 'id',
        columnDefs: [
//            {targets: 1, 
//                render: function (data,type,row) {     
//                  var str = data.toString();
//                  var emails = str.split(" ");
//                  $.each(emails, function (v){
//                      emails[v] = '<li>' + emails[v] + '</li>';
//                  });
//                  return emails;
//                 }
//            },
        ],
        "order": [[0, "desc"]],
        columns: [
//            {data: 'id', name: 'id'},
//            {data: 'avatar', name: 'avatar'},
            {data: 'group_name', name: 'group_name'},
//            {data: 'emails',name : 'emails',searchable:true},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    
    
    $('#vehicle_management').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/vehicle/list_json',
        columnDefs: [
            {targets: 2,
                render: function (data) {
                    if (data == 'default.png') {
                        return '<img src="' + url + '/images/default/vehicle_icon.png" width="50px"/>';
                    } else
                    {
                        return '<img src="' + url+'../storage/app/'+data + '" width="50px">';
                    }

                }
            },
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'id', name: 'id'},            
            {data: 'name', name: 'name'},
            {data: 'image_icon', name: 'image_icon'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        dom: 'Bfrtip',
         buttons: [
             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
             
             'colvis'
        ],

    });
    $('#vehicle_body_type_management').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/vehicle_body/list_json',
        columnDefs: [
            {targets: 2,
                render: function (data) {
                    if (data == 'default.png') {
                        return '<img src="' + url + '/images/default/body_type_icon.png" width="50px"/>';
                    } else
                    {
                        return '<img src="' + url+'../storage/app/'+data + '" width="50px">';
                    }

                }
            },
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'id', name: 'id'},            
            {data: 'name', name: 'name'},
            {data: 'image_icon', name: 'image_icon'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        dom: 'Bfrtip',
         buttons: [
             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
             
             'colvis'
        ],

    });
    $('#vehicle_goods_type').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/goods_type/list_json',
        columnDefs: [
            {targets: 2,
                render: function (data) {
                    if (data == 'default.png') {
                        return '<img src="' + url + '/images/default/goods_type_icon.png" width="50px"/>';
                    } else
                    {
                        return '<img src="' + url+'../storage/app/'+data + '" width="50px">';
                    }

                }
            },
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'id', name: 'id'},            
            {data: 'name', name: 'name'},
            {data: 'image_icon', name: 'image_icon'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        dom: 'Bfrtip',
         buttons: [
             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
             
             'colvis'
        ],

    });
    $('#order_shipping_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/shiping/list_json',
        "aoColumnDefs": [ 
            {
                     "aTargets": [ 10 ],
                     "mRender": function ( data, type, full ) {
                      return '$'+data;
                      }
                },
               {
                     "aTargets": [ 12 ],
                     "mRender": function ( data, type, full ) {
                      return $("<div/>").html(data).text(); 
                      }
                }
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'},         
            {data: 'full_name', name: 'full_name'},             
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
//            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
        //dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#order_shipping_list_all').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        rowId : 'id',
        responsive: true,
        ajax: url + 'admin/shiping/list_all_json',
        "aoColumnDefs": [ 
            {
                     "aTargets": [ 10 ],
                     "mRender": function ( data, type, full ) {
                      return '$'+data;
                      }
                },
               {
                     "aTargets": [ 12 ],
                     "mRender": function ( data, type, full ) {
                      return $("<div/>").html(data).text(); 
                      }
                }
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'}, 
            {data: 'full_name', name: 'full_name'},            
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
//            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
        //dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#order_shipping_active_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/shiping/list_active_json',
        columnDefs: [
              {targets: 0,
                  render: function (data) {

//                          return '<input type="checkbox" name="select_box[]" value="'+data+'">';


                  }
              },
              {targets: 12,
                  render: function (data) {

                          return '$'+data;


                  }
              },
          ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'},         
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#order_shipping_live_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/shiping/list_live_json',
        "createdRow": function( row, data, dataIndex){
               if(data.status == 3){
                   $(row).css('background-color','#efbe65');
               }
            },
        "aoColumnDefs": [ 
            {
                     "aTargets": [ 10 ],
                     "mRender": function ( data, type, full ) {
                      return '$'+data;
                      }
                },
               {
                     "aTargets": [ 12 ],
                     "mRender": function ( data, type, full ) {
                      return $("<div/>").html(data).text(); 
                      }
                }
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'},         
            {data: 'full_name', name: 'full_name'},            
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
//            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#order_shipping_pending_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/shiping/list_pending_json',
        "aoColumnDefs": [ 
            {
                     "aTargets": [ 10 ],
                     "mRender": function ( data, type, full ) {
                      return '$'+data;
                      }
                },
               {
                     "aTargets": [ 12 ],
                     "mRender": function ( data, type, full ) {
                      return $("<div/>").html(data).text(); 
                      }
                }
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'},         
            {data: 'full_name', name: 'full_name'},            
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
//            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#order_shipping_upcoming_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/shiping/list_upcoming_json',
        columnDefs: [
            {targets: 0,
                render: function (data) {
                    
//                        return '<input type="checkbox" name="select_box[]" value="'+data+'">';
                    

                }
            },
            {targets: 12,
                render: function (data) {
                    
                        return '$'+data;
                    

                }
            },
        ],
        "order": [[0, "desc"]],
        columns: [
            {data: 'order_booking_id', name: 'order_booking_id'},         
            {data: 'job_name', name: 'job_name'},            
            {data: 'trucking_start_date', name: 'trucking_start_date'},            
            {data: 'total_duration', name: 'total_duration'},            
            {data: 'job_type', name: 'job_type'},            
            {data: 'material_type', name: 'material_type'},           
            {data: 'mobile_number', name: 'mobile_number'},            
            {data: 'pickup', name: 'pickup'},
//            {data: 'loading_start_time', name: 'loading_start_time'},
            {data: 'destination', name: 'destination'},
//            {data: 'loading_end_time', name: 'loading_end_time'},
            {data: 'price_type', name: 'price_type'},
            {data: 'total_price', name: 'total_price'},
            {data: 'type_of_truck', name: 'type_of_truck'},
            {data: 'no_of_truck_need', name: 'no_of_truck_need'},
           
            {data: 'how_long_job', name: 'how_long_job'},
            {data: 'comment', name: 'comment'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,2,3,4,5] }},
//             
//             'colvis'
//        ],

    });
    $('#master_notification_list').DataTable({
        processing: true,
        dom : "<'row' <'col-sm-6'<'top'i>><'col-sm-6' f>rt><'row'<'col-sm-6'<'bottom' l>> <'col-sm-6'p>>",
        language: {
//            'loadingRecords': '&nbsp;',
            'processing': '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },  
        serverSide: true,
        responsive: true,
        ajax: url + 'admin/notification/list_json',
        "order": [[0, "desc"]],
        columns: [
            {data: 'id', name: 'id'},
            {data: 'message', name: 'message'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
//        dom: 'Bfrtip',
//         buttons: [
//             {extend:'print',exportOptions: { columns: [0,1] }},
//             
//             'colvis'
//        ],

    });

    $('#users_group tr td a.clearitem').on('click', function (){
       
        $(this).closest('tr').remove();
        $.toast({
                        heading: 'Success',
                        text: "Removed user from group!",
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
    });
    
    $('#click_to_submit_group').on('click', function () {

        if($('#group_name').val() == ''){
            $('#group_name').focus();
            $('#group_name').css('background-color','red');
            return false;
        }
        var data = document.getElementById('group_data_edit');
        var form_data = new FormData(data);
        console.log(data);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url + "admin/Ajax_controller/edit_group_",
            data: form_data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                
                var data = jQuery.parseJSON(response);
                if (data.code === 1) {
                   $('#group_name').css('background-color','white');
                    $.toast({
                        heading: 'Success',
                        text: data.message,
                        showHideTransition: 'slide',
                        icon: 'success',
                        position: 'top-center'
                    })
                } else
                {
                    $.toast({
                        heading: 'Error',
                        text: data.message,
                        icon: 'error',
                        position: 'top-center'
                    })
                }

            }
        });
    

        });
        
});

  