/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function () {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1)
            height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function () {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});
$('#getval').on('change', function () {
    readURL();
});
$('#doucuments_1_change').on('change', function () {
    document_1();
});
$('#doucuments_2_change').on('change', function () {
    readURL_2();
});
$('#doucuments_3_change').on('change', function () {
    readURL_3();
});

function readURL() {
    var file = document.getElementById("getval").files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        document.getElementById('profile-upload').style.backgroundImage = "url(" + reader.result + ")";
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
    }
}
function document_1() {
    var file = document.getElementById("doucuments_1_change").files[0];
    var reader_1 = new FileReader(file);
    reader_1.onload = function(e) {
    var rawLog = reader_1.result;
     document.getElementById('document_1').style.backgroundImage = "url(" + rawLog + ")";
    }
    if (file) {
        reader_1.readAsDataURL(file);
    }
}
function readURL_2() {
    var file = document.getElementById("doucuments_2_change").files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        document.getElementById('document_2').style.backgroundImage = "url(" + reader.result + ")";
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
    }
}
function readURL_3() {
    var file = document.getElementById("doucuments_3_change").files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        document.getElementById('document_3').style.backgroundImage = "url(" + reader.result + ")";
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
    }
}




