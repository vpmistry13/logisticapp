<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $primary_table = 'user_profile';
    public function up()
    {
        Schema::create($this->primary_table, function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_id');
            $table->string('type')->nullable();
            $table->string('user_gender')->nullable();
            $table->integer('no_of_truck')->default(0);
            $table->string('cdl')->nullable();
            $table->string('address')->nullable();
            $table->string('avatar')->nullable();
            $table->string('driving_licence_image')->nullable();
            $table->string('insurance_image')->nullable();
            $table->string('wq_image')->nullable();
            $table->string('office_number')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists($this->primary_table);
    }
}
