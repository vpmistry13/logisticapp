<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderAccpted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $primary_table = 'order_accpted';
    public function up()
    {
        Schema::create($this->primary_table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id');
            $table->integer('customer_id');
            $table->integer('job_id');
            $table->integer('status')->default(1);                    
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists($this->primary_table);
    }
}
