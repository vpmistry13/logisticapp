<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PriceType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'price_type';
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        $data[0]['name']= 'Per Load';
        $data[1]['name']= 'Per Ton';
        $data[2]['name']= 'Per Hour';
        DB::table($this->table)->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists($this->table);
    }
}
