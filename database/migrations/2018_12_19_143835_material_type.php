<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MaterialType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'material_type';
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        
        $data[0]['name'] = 'Sand';
        $data[1]['name'] = 'Dirt';
        $data[2]['name'] = 'Crushed Concrete';
        $data[3]['name'] = 'Stabilized Sand';
        $data[4]['name'] = 'Bull Rock';
        $data[5]['name'] = 'Other';
        
        DB::table($this->table)->insert($data);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists($this->table);
    }
}
