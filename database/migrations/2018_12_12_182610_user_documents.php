<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $primary_table = 'user_documents';
    public function up()
    {
        Schema::create($this->primary_table, function (Blueprint $table) {
            $table->increments('id');
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_id');
            $table->string('document_name');
            $table->string('location');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists($this->primary_table);
    }
}
