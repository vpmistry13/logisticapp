<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TruckType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'truck_type';
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        
        $data[0]['name'] = 'Truck';
        $data[1]['name'] = 'Tow Truck';
        $data[2]['name'] = 'Semi-trailer Truck';
        $data[3]['name'] = 'Stabilized Sand';
        $data[4]['name'] = 'Box Truck';
        $data[5]['name'] = 'Flatbed Truck';
        $data[6]['name'] = 'Beverage Trailer';
        
        DB::table($this->table)->insert($data);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists($this->table);
    }
}
