<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'book_jobs';
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('job_name',100);
            $table->string('trucking_start_date',191);
            $table->string('total_duration',100);
            $table->string('job_type',100)->default('DAY')->comment('DAY/NIGHT');
            $table->string('material_type');
            $table->string('loading_location',191);
            $table->string('loading_start_time',191)->nullable();
            $table->string('from_lat',191)->nullable();
            $table->string('from_long',191)->nullable();
            $table->string('delivery_location',191);
            $table->string('loading_end_time',191)->nullable();
            $table->string('to_lat',191)->nullable();
            $table->string('to_long',191)->nullable();
            $table->string('price_type');
            $table->integer('total_price');
            $table->string('how_long_job')->nullable();
            $table->string('amount_of_material')->nullable();
            $table->string('no_of_truck_need',191)->nullable();
            $table->string('type_of_truck')->nullable();
            $table->string('assign_to')->nullable();
            $table->string('assign_to_all')->nullable();
            $table->string('notify_type')->nullable();
            $table->string('comment')->nullable();            
            $table->integer('status')->default(0)->comment('0 - Pending, 1 - Complete, 2 -Reject, 3 - Live');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
