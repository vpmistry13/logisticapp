<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $table = 'reports';
    public function up()
    {
        Schema::create($this->table,function($table){
            $table->increments('id');
            $table->string('report_type');
            $table->string('report_url')->nullable();
            $table->string('from_date');
            $table->string('to_date');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP')); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
