<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobAssign extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $to;
    protected $name;
    protected $sub_ject;
    protected $dataM;
    public function __construct($t, $n, $s,$data = array()) {
        $this->to = $t;
        $this->name = $n;
        $this->sub_ject = $s;
        $this->dataM = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('email.jobassign')->subject('New job available - Trucks needed!')->with([
                    'name' => $this->name, 'sub_ject' => $this->sub_ject,'data' => $this->dataM]);
    }

}
