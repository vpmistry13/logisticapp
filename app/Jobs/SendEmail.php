<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\JobAssign;
use App\Mail\JobCreate;
use App\Mail\TruckOwnerAccountApprove;
use App\Mail\JobAcceptedTruckOwner;
use App\Mail\WelcomeMail;
use App\Mail\WelcomeMailTruck;
use App\Mail\JBCTruckAccountApprove;
use App\Mail\NewCustomerAccount;
use App\Mail\TruckerMessage;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $to;
    protected $name;
    protected $sub_ject;
    protected $dataM;
    protected $mailtype;
    public function __construct($to, $name, $subject,$data = array(),$mail_type) {
        $this->to = $to;
        $this->name = $name;
        $this->sub_ject = $subject;
        $this->dataM = $data;
        $this->mailtype = $mail_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
       
        if($this->mailtype == 1):
            // when truck owner assign job in group
            $email_data = new JobAssign($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 2):
            // when new job create
            $email_data = new JobCreate($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 3):
            // when truck owner signup
            $email_data = new TruckOwnerAccountApprove($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 4):
            // when truck owner accept job
            $email_data = new JobAcceptedTruckOwner($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 5):
            // welcome customer
            $email_data = new WelcomeMail($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 6):
            // welcome truckowner
            $email_data = new WelcomeMailTruck($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 7):
            // welcome truckowner when admin aproved account
            $email_data = new JBCTruckAccountApprove($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        
        if($this->mailtype == 8):            
            // when new customer mail send to admin
            
            $email_data = new NewCustomerAccount($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($this->mailtype == 9):            
            // Trucker Bulk Messages            
            $email_data = new TruckerMessage($this->to,$this->name,$this->sub_ject,$this->dataM);
        endif;
        if($email_data):
            Mail::to($this->to)->send($email_data);
        endif;
        
    }
}
