<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Twilio\Rest\Client;
use Twilio\Rest\Accounts;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $account_sid;
    protected $auth_token;
    protected $gateway;
    protected $twilio_number;
    
    protected $send_to;
    protected $message_body;
    public function __construct($send_to,$message_body)
    {
        $this->send_to = $send_to;
        $this->message_body = $message_body;
        // Your Account SID and Auth Token from twilio.com/console
        $this->account_sid = 'ACb7e460c608a56d93293d49781f747c4b';
        $this->auth_token = '098ac5547b1e8fed39ccc4f3a157977d';
        $this->twilio_number = "+13124770448";
        $this->gateway = new Client($this->account_sid, $this->auth_token);
        
        // In production, these should be environment variables. E.g.:
        // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {       
        
        // A Twilio number you own with SMS capabilities
        
        $data = $this->gateway->messages->create(
            $this->send_to,    
            array(
                'from' => $this->twilio_number,
                'body' => $this->message_body
            )
        );
    }
    public static function send(){
        echo 'test';
        $account_sid = 'ACb7e460c608a56d93293d49781f747c4b';
        $auth_token = '098ac5547b1e8fed39ccc4f3a157977d';
        $twilio_number = "+13124770448";

        $client = new Client($account_sid, $auth_token);
        
$message = $client->messages->create(
  '+919099011665',
  array(
    'from' => $twilio_number,
    'body' => 'Thanks for your order! On a scale of 1-10 would you recommend ' .
              '[company_name] to a friend? Reply with the number 1 to 10 to this message.',
  )
);

print($message->sid);
     //   print_r($data);
    }
}
