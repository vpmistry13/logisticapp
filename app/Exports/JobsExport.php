<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class JobsExport implements FromCollection,WithHeadings
{
    public function __construct(int $status,string $from_date,string $to_date)
    {
        $this->status = $status;
        $this->from_date = strtotime($from_date);
        $this->to_date = strtotime($to_date);
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $users = 'users';
    protected $profile = 'user_profile';
    protected $jobs = 'book_jobs';
    protected $price_type = 'price_type';
    public function collection()
    {
        if($this->status != 0):
            $data = DB::table($this->jobs.' as j')->select('j.job_name','j.trucking_start_date','j.job_type','j.material_type','loading_location','delivery_location','pt.name as price_type','total_price','no_of_truck_need','u.first_name','u.last_name','u.company_name','u.mobile_number','u.email')->leftJoin($this->price_type.' as pt','pt.id','=','j.price_type')->leftJoin($this->users.' as u','u.id','=','j.user_id')->leftJoin($this->profile.' as p','p.user_id','=','u.id')->where('j.status',$this->status)->whereBetween('j.created_at', [Date('Y-m-d',$this->from_date), Date('Y-m-d',$this->to_date)])->get();
            if($data):
                foreach($data as $v):
                    if($v->trucking_start_date):
                    $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                    $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                    endif;
                    if(@$v->created_at):
                      $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                      $v->created_at = $created_at->format('F d, Y h:m:s'); 
                    endif;
                endforeach;
            endif;
            return $data;
            else:            
            $data = DB::table($this->jobs.' as j')->select('j.job_name','j.trucking_start_date','j.job_type','j.material_type','loading_location','delivery_location','pt.name as price_type','total_price','no_of_truck_need','u.first_name','u.last_name','u.company_name','u.mobile_number','u.email')->leftJoin($this->price_type.' as pt','pt.id','=','j.price_type')->leftJoin($this->users.' as u','u.id','=','j.user_id')->leftJoin($this->profile.' as p','p.user_id','=','u.id')->whereBetween('j.created_at', [Date('Y-m-d',$this->from_date), Date('Y-m-d',$this->to_date)])->get();    
            if($data):
                foreach($data as $v):
                    if($v->trucking_start_date):
                    $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                    $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                    endif;
                    if(@$v->created_at):
                      $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                      $v->created_at = $created_at->format('F d, Y h:m:s'); 
                    endif;
                endforeach;
            endif;
            return $data;
        endif;
        
    }
    
    public function headings(): array
    {
        return [
            'Job Name',
            'Trucking Start Date',
            'Job Type',
            'Material Type',
            'Loading Location',
            'Delivery Location',
            'Price Type',
            'Price',
            'No of Truck Need',
            'First Name',
            'Last Name',
            'Company Name',
            'Mobile Number',
            'Email Address'
        ];
    }
}
