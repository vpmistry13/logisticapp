<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    public function __construct(int $user_type,string $from_date,string $to_date)
    {
        $this->user_type = $user_type;
        $this->from_date = strtotime($from_date);
        $this->to_date = strtotime($to_date);
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $users = 'users';
    protected $profile = 'user_profile';
    public function collection()
    {
           
        $data =  DB::table($this->users.' as u')->select('first_name','last_name','company_name','mobile_number','email','user_status','no_of_truck','cdl','avatar','address','driving_licence_image','insurance_image','wq_image','office_number','u.created_at')->leftJoin($this->profile.' as p','p.user_id','=','u.id')->where('user_type',$this->user_type)->whereBetween('u.created_at', [Date('Y-m-d',$this->from_date), Date('Y-m-d',$this->to_date)])->get();
        if($data):
                foreach($data as $v):                    
                    if(@$v->created_at):
                      $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                      $v->created_at = $created_at->format('F d, Y h:m:s'); 
                    endif;
                endforeach;
            endif;
        return $data;
        
    }
    public function headings(): array
    {
        return [
            'First Name',
            'Last Name',
            'company_name',
            'mobile_number',
            'email',
            'user_status',
            'no_of_truck',
            'CDL Number',
            'Profile URL',
            'Address',
            'driving_licence_image',
            'insurance image',
            'W9 Form Image',
            'office number',
            'Created at'
        ];
    }
}
