<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class signup extends Model
{
    protected $primary_table = 'password_resets';
    protected $user_table = 'users';
    protected $profile = 'user_profile';
//    protected $order_booking = 'order_booking';
//    protected $order_managment = 'order_managment';
//    
//   
    
    public function insert_user($data){
       
        return DB::table($this->user_table)->insertGetId($data);
    }
    
    public function insert_user_p($data){
       
        return DB::table($this->profile)->insertGetId($data);
    }
    
    public function update_user($id,$data){
        return DB::table($this->user_table)->whereid($id)->update($data);
    }
    public function update_profile($id,$data){
        $cont = DB::table($this->profile)->whereuser_id($id)->count();
        if($cont >= 1){
            return DB::table($this->profile)->whereuser_id($id)->update($data);
        }else{
            $data['user_id'] = $id;
            return DB::table($this->profile)->insert($data);
        }
    }
    public function verify_user($id){
        return DB::table($this->user_table)->whereid($id)->update(array("verified" => 1));
    }
    public function delete_user($id){
        DB::table($this->user_table)->whereid($id)->delete();
        DB::table($this->profile)->whereuser_id($id)->delete();
//        DB::table($this->order_booking)->whereuser_id($id)->delete();
//        DB::table($this->order_managment)->whereuser_id($id)->delete();
    }
    public function email_exit($username)
    {
        $q = DB::table($this->user_table)->where('email',$username)->get();
        if(count($q) > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
        
    }
    
    public function user_name($username)
    {
        $q = DB::table($this->profile)->where('user_name',$username)->get();
        if(count($q) > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
}
