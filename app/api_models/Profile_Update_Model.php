<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Profile_Update_Model extends Model {

    //
    protected $primary_table = 'users';
    protected $profile = 'profile';

    public function picture_exits($user_id) {
        $q = DB::table($this->profile)->where('user_id',$user_id)->get();
        
        if (count($q) > 0) {
            if($q[0]->avatar == 'default.jpg')
            {
                return 0;
            }
            else
            {
                return $q;
            }
        } else {
            return 0;
        }
    }

    public function edit_Profile_Picture($data, $user_id) {
        $q = DB::table($this->profile)->where('user_id', $user_id)->update($data);
        return $q;
    }

}
