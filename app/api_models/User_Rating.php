<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User_Rating extends Model {

    //
    protected $user_rating = 'user_rating';

    public function rate_driver($user_id, $trip_id, $driver_id, $rate, $comment = null) {
        $data['user_id'] = $user_id;
        $data['trip_id'] = $trip_id;
        $data['driver_id'] = $driver_id;
        $data['rate'] = $rate;
        $data['comment'] = $comment;
        $exit = DB::table($this->user_rating)->wheretrip_id($trip_id)->count();
        if ($exit < 1) {
            DB::table($this->user_rating)->insert($data);
        } else {
            $update['rate'] = $rate;
            $update['comment'] = $comment;            
            DB::table($this->user_rating)->wheretrip_id($trip_id)->update($update);
        }
    }
    
    public function pending_rating($trip_id){
       $q = DB::table($this->user_rating)->whereNotIn('trip_id', $trip_id)->get();
       return $q;
    }

}
