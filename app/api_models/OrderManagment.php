<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderManagment extends Model {

    //
    protected $order_managment = 'order_managment';
    protected $booking = 'order_booking';
    protected $user = 'profile';

    public function entry($data) {
        DB::table($this->order_managment)->insert($data);
    }

    public function requestExit($data) {
        $q = DB::table($this->order_managment)->where($data)->get();
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function changeStatus($id,$data){
        DB::table($this->order_managment)->where($id)->update($data);        
    }
    public function changeBookStatus($id,$data){
        DB::table($this->booking)->where($id)->update($data);
    }

    public function getRequests($data, $limit) {
        $q = DB::table($this->booking . ' as o')->Join($this->order_managment . ' as b', 'b.trip_id', '=', 'o.id')->join($this->user . ' as u', 'u.user_id', '=', 'o.user_id')->where($data)->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getDrivertrip($data, $limit) {
        $q = DB::table($this->order_managment.' as om')->leftJoin($this->booking.' as b', 'b.id','=','om.trip_id')->join($this->user . ' as u', 'u.user_id', '=', 'om.user_id','inner')->where($data)->where('om.status','!=','0')->orderByDesc('b.id')->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getUsertrip($data, $limit) {
        $q = DB::table($this->order_managment.' as om')->leftJoin($this->booking.' as b', 'b.id','=','om.trip_id')->join($this->user . ' as u', 'u.user_id', '=', 'om.driver_id','inner')->where($data)->where('om.status','!=','0')->orderByDesc('b.id')->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }

}
