<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class User_Model extends Model {

    //
    protected $users = 'users';
    protected $Profile = 'user_profile';
    protected $social_table = 'social_facebook_accounts';

    public function profile_edit($data, $id) {
      
        $get = DB::table($this->Profile)->whereuser_id($id)->get();       
        if (count($get) > 0) {
            $q = DB::table($this->Profile)->whereuser_id($id)->update($data);
        } else {
            $data['user_id'] = $id;           
            $q = DB::table($this->Profile)->insert($data);
        }
        return $q;
    }
    public function profile_edit_u($data, $id) {
      
       return DB::table($this->users)->whereid($id)->update($data);       
       
    }

    public function passwordCorrect($suppliedPassword) {
        return Hash::check($suppliedPassword, Auth::user()->password, []);
    }

    public function search_username($search, $limit) {
        return DB::table($this->Profile . ' as p')->select('p.user_id as id', 'full_name', 'user_name', 'birth_date', 'user_city', 'user_gender', 'avatar as profile_picture', 'mobile_number', 'account_privacy', 'provider', 'provider_user_id')->leftJoin($this->social_table . ' as s', 's.user_id', '=', 'p.user_id')->where('user_name', 'like', '%' . $search . '%')->paginate($limit);
    }

    public function get_token($user_id) {
        $q = DB::table($this->Profile)->select('user_token')->where('user_id', $user_id)->get();

        if (count($q) > 0) {
            return $q[0]->user_token;
        } else {
            return 0;
        }
    }
    public function getDriverList(){
        $q = DB::table($this->users . ' as u')->select('u.first_name','u.last_name','u.id','u.email',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address as driver_city','u.mobile_number as mobile_number','p.avatar','p.cdl')->join($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(2)->get();
       
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getCustomerList($limit){
        $q = DB::table($this->users . ' as u')->select('u.first_name','u.last_name','u.id','u.email',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address','u.mobile_number as mobile_number','p.avatar')->join($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(1)->paginate($limit);
       
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getCustomerListJSON($limit){    
        $users = DB::table($this->users . ' as u')->select('u.id','u.email','u.company_name',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address','u.mobile_number as mobile_number','p.avatar','p.office_number')->Leftjoin($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(1)->orderByDesc('u.id')->get();
      
        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a href="'. url('admin/customer/EditAccount/'.base64_encode($user->id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return user_remove('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('id', '{{$id}}')
            ->removeColumn('password')
            ->make(true);
       
    }
    public function getDriverListJSON($limit){    
        $users = DB::table($this->users . ' as u')->select('u.id','u.email','u.verified',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.company_name','p.address as address','u.mobile_number as mobile_number','p.avatar','p.cdl','p.no_of_truck',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, p.type)) as type'),'p.office_number','p.driving_licence_image','p.cdl','p.insurance_image','p.wq_image')->Leftjoin($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(2)->orderByDesc('u.id')->get();
        
        return Datatables::of($users)
            ->addColumn('action', function ($user) {
//                if($user->document_verification === 1){
//                    $status = '<button type="button" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-check"></i> Verified</button>';
//                }else
//                {
//                    $status = '<button type="button" onclick="return document_verify('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-check"></i> Click to Verify</button>';
//                }
                if($user->verified == 0):
                    return '<a href="'. url('admin/driver/EditAccount/'.base64_encode($user->id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return user_remove('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</button>&nbsp;<button type="button" onclick="return user_verify('.$user->id.')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-tick"></i>Approve</button>&nbsp;';
                endif;
                return '<a href="'. url('admin/driver/EditAccount/'.base64_encode($user->id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return user_remove('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</button>&nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->removeColumn('password')
            ->make(true);
       
    }
    public function getCustomerListDATA($limit){
       return DataTables::make(DB::table($this->users . ' as u')->select('u.id','u.email',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address','u.mobile_number as mobile_number','p.avatar')->join($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(1)->get());
    }
    public function totalCustomerCount(){
       return DB::table($this->users . ' as u')->select('u.id','u.email',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address','u.mobile_number as mobile_number','p.avatar')->join($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->whereuser_type(1)->count();
    }
    public function getUser($id){
        
        $q = DB::table($this->users . ' as u')->select('u.first_name','u.last_name','u.id','u.email','u.company_name',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address','p.user_gender','u.password','p.cdl','u.user_type','u.mobile_number as mobile_number','p.avatar','p.office_number','p.type','p.no_of_truck','p.driving_licence_image','p.insurance_image','p.wq_image','p.cdl')->Leftjoin($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->where('u.id',$id)->get();
       
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getUserProfile($id){
        
        $q = DB::table($this->users . ' as u')->select('u.*','p.*','u.first_name as name')->leftJoin($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->where('u.id',$id)->get();
       
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getDriverDetails($id) {
        $q = DB::table($this->users . ' as u')->select('u.first_name','u.last_name',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'p.address as address as driver_city','u.mobile_number as mobile_number','p.avatar','p.cdl','p.office_number')->Leftjoin($this->Profile . ' as p', 'p.user_id', '=', 'u.id')->where('u.id', $id)->get();
       
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    

}
