<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FollowMaster extends Model {

    //
    public $table = 'followers_master';
    protected $primary_table = 'followers_master';
    protected $secondry_table = 'users';
    protected $profile = 'profile';
    protected $social_table = 'social_facebook_accounts';
    protected $fillable = [
        'user_id', 'follow_id', 'status',
    ];

    public function addFollow($data) {
        $t = DB::table($this->primary_table)->insert($data);
        return DB::lastInsertId();
    }

    public function getFollowExit($data) {
        $t = DB::table($this->primary_table)->where($data)->get();
        if (count($t) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getRequests($data, $limit) {
        $t = DB::table($this->primary_table)->select('p.user_id as follow_id', 'full_name', 'user_name', 'avatar as profile_picture', 'provider_user_id', 'provider')->leftJoin($this->profile.' as p','p.user_id','=',$this->primary_table . '.follow_id')->leftJoin($this->secondry_table, $this->primary_table . '.follow_id', '=', $this->secondry_table . '.id')->leftJoin($this->social_table, $this->social_table . '.user_id', '=', $this->secondry_table . '.id')->where($data)->paginate($limit);
        if (count($t) > 0) {
            return $t;
        } else {
            return 0;
        }
    }

    public function getUser($id) {
        $t = DB::table($this->profile)->select('full_name')->whereuser_id($id)->get();
        if(count($t) > 0){
        return $t;
        }
        else
        {
            return 0;
        }
    }

    public function getCount($data) {
        $t = DB::table($this->primary_table)->select(DB::raw('count(id) as count'))->where($data)->get();
        return $t[0]->count;
    }

    public function status($data, $where) {
        $t = DB::table($this->primary_table)->where($where)->update($data);
        return $t;
    }

    public function getFollowers_list($id, $limit) {

        $r = DB::table($this->primary_table . ' as f')->select("u.id", "p.full_name", "p.avatar as profile_picture", "s.provider_user_id", "s.provider","p.user_city", DB::raw("case when ca7s_b.id is null then
					   'follow'
					else
					   'following'
                                        end	as user_status"))->leftJoin($this->secondry_table . ' as u', 'u.id', '=', 'f.user_id')->leftJoin($this->primary_table . ' AS b', function($join) {
                    $join->on('b.user_id', '=', 'f.follow_id');
                    $join->on('b.follow_id', '=', 'u.id');
                })->leftJoin($this->profile.' as p', 'p.user_id', '=', 'f.user_id')->leftJoin($this->social_table . ' as s', 'f.follow_id', '=', 's.user_id')->where('f.follow_id', $id)->paginate($limit);


        if (count($r) > 0)
            return $r;
        else
            return 0;
    }

    public function getFollowing_list($id, $limit) {

        $r = DB::table($this->primary_table . ' as f')->select("u.id", "p.full_name", "p.avatar as profile_picture", "s.provider_user_id", "s.provider","p.user_city", DB::raw("case when ca7s_b.id is not null then
					   'follow'
					else
					   'following'
                                        end	as user_status"))->leftJoin($this->secondry_table . ' as u', 'u.id', '=', 'f.follow_id')->leftJoin($this->primary_table . ' AS b', function($join) {
                    $join->on('b.user_id', '=', 'f.follow_id');
                    $join->on('b.follow_id', '=', 'u.id');
                })->leftJoin($this->profile.' as p', 'p.user_id', '=', 'f.follow_id')->leftJoin($this->social_table . ' as s', 'f.follow_id', '=', 's.user_id')->where('f.user_id', $id)->paginate($limit);

        if (count($r) > 0)
            return $r;
        else
            return 0;
    }
    
    public function removeFollow($data)
    {      
       $q = DB::table($this->primary_table)->where($data)->delete();
    }

}
