<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class General_Model extends Model
{
    //
    protected $goods_type = 'goods_type';
    protected $vehicle_type = 'vehicle_type';
    protected $body_type = 'body_type';
    public function getGoodsType()
    {
        $q = DB::table($this->goods_type)->select('id','name','image_icon')->get();
        return $q;
    }
    public function getGoodsTypebyID($id)
    {
        $q = DB::table($this->goods_type)->select('id','name','image_icon')->whereid($id)->get();
        return $q;
    }
    public function getVehicleType()
    {
        $q = DB::table($this->vehicle_type)->select('id','name','image_icon')->get();
        return $q;
    }
    public function getVehicleTypebyID($id)
    {
        $q = DB::table($this->vehicle_type)->select('id','name','image_icon')->whereid($id)->get();
        return $q;
    }
    public function getBodyType()
    {
        $q = DB::table($this->body_type)->select('id','name','image_icon')->get();
        return $q;
    }
}
