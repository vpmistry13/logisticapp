<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserNotification extends Model {

    //
    protected $user_notification = 'user_notification';
    protected $notification_master = 'notification_master';

    public function set($data) {
        DB::table($this->user_notification)->insert($data);
    }

    public function get($user_id, $limit) {
        $q = DB::table($this->user_notification)->whereuser_id($user_id)->wherestatus(0)->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }

    public function read($id) {
        $status['status'] = 1;
        DB::table($this->user_notification)->whereid($id)->update($status);
    }

    public function clearAll($param) {
        DB::table($this->user_notification)->whereuser_id($param)->delete();
    }

    public function clearOnes($param) {
        DB::table($this->user_notification)->whereid($param)->delete();
    }

    public function notification_master($id) {
        $q = DB::table($this->notification_master)->select('message')->whereid($id)->get();
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }

}
