<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class password_resets extends Model
{
    //
    
    protected $password_reset = 'password_resets';
    
    public function entry($data,$email) {
  
        $q = DB::table($this->password_reset)->whereemail($email)->get();
       
        if(count($q) < 1)
        {           
           
            $data['email'] = $email;
            DB::table($this->password_reset)->insert($data);
        }
        else
        { 
          
            $token['token'] = $data['token'];
            DB::table($this->password_reset)->where('email',$email)->update($token);
        }
    }
    public function verify_otp($data)
    {
         $q = DB::table($this->password_reset)->where($data)->get();
         if(count($q) > 0){             
             DB::table($this->password_reset)->where($data)->delete();
             return 1;
         }
         else
         {
             return 0;
         }
    }
}
