<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Login extends Model
{
    //initilize table name   
    protected $primary_table = 'password_resets';
    protected $user_table = 'users';
    protected $profile = 'profile';
    protected $session ='sessions';
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);       
        
    }
    public function getUser($id)
    {
        $data = DB::table($this->user_table.' as u')->select('full_name','user_city','birth_date','user_gender','avatar','mobile_number','user_token','account_privacy','document_verification')->leftJoin($this->profile.' as p', 'p.user_id','=','u.id')->whereid($id)->get();
        return $data;       
    }
    public function auth_login($email)
    {
        $data = DB::table($this->user_table)->select('id as user_id','email','verified')->where('email',$email)->get();
        return $data;
    }
   
}
