<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Book_order_Model extends Model
{
    //
    protected $book_order = 'book_jobs';
    protected $order_managment = 'order_managment';
    protected $profile = 'profile';
    protected $body_type = 'body_type';
    protected $vehicle_type = 'vehicle_type';
    protected $goods_type = 'goods_type';
    protected $order_accpted = 'order_accpted';
    protected $users = 'users';
    protected $price_type = 'price_type';

    public function active_jobs_truckOwner($user_id){        
        $q =  DB::table($this->book_order.' as o')->select('o.id as job_id','o.job_name','o.user_id','o.material_type','o.loading_location','o.delivery_location','o.price_type','o.total_price','o.how_long_job','o.amount_of_material','o.no_of_truck_need','o.type_of_truck','o.comment','o.status','o.created_at','o.updated_at','o.assign_to','p.name as price_type_name')->leftJoin($this->price_type.' as p','p.id','=','o.price_type')->leftJoin($this->users.' as u','u.id','=','o.user_id')->whereRaw('(o.status = 1 AND assign_to_all = 1) OR o.status = 1 and o.assign_to like "%'.$user_id.'%"')->paginate();
        if(count($q) > 0){
            return $q;
        }else{
            return false;
        }
    }
    public function active_jobs_truckOwner_active($id){        
        $q =  DB::table($this->book_order.' as o')->select('o.id as job_id','o.job_name','o.user_id','o.material_type','o.loading_location','o.delivery_location','o.price_type','o.total_price','o.how_long_job','o.amount_of_material','o.no_of_truck_need','o.type_of_truck','o.comment','o.status','o.created_at','o.updated_at','o.assign_to','o.trucking_start_date','p.name as price_type_name')->leftJoin($this->price_type.' as p','p.id','=','o.price_type')->leftJoin($this->users.' as u','u.id','=','o.user_id')->rightJoin($this->order_accpted.' as oa','oa.job_id','=','o.id')->where('o.status',3)->where('oa.driver_id',$id)->paginate();
      
        if(count($q) > 0){
            return $q;
        }else{
            return false;
        }
    }
    public function active_jobs_truckOwner_complete($id){        
        $q =  DB::table($this->book_order.' as o')->select('o.id as job_id','o.job_name','o.user_id','o.material_type','o.loading_location','o.delivery_location','o.price_type','o.total_price','o.how_long_job','o.amount_of_material','o.no_of_truck_need','o.type_of_truck','o.comment','o.trucking_start_date','o.status','o.created_at','o.updated_at')->leftJoin($this->users.' as u','u.id','=','o.user_id')->rightJoin($this->order_accpted.' as oa','oa.job_id','=','o.id')->where('o.status',4)->where('oa.driver_id',$id)->paginate();
        if(count($q) > 0){
            return $q;
        }else{
            return false;
        }
    }
    
    public function mark_as_complete_job($job_id,$driver_id){
        return DB::table($this->book_order.' as o')->rightJoin($this->order_accpted.' as oa','oa.job_id','=','o.id')->where('o.id',$job_id)->where('oa.driver_id',$driver_id)->update(array("o.status" => 4));
    }
    
    public function book($data){
       return DB::table($this->book_order)->insertGetId($data);
    }
    public function book_u($job_id,$data){
       return DB::table($this->book_order)->whereid($job_id)->update($data);
    }
    
    public function order_accpted($d){
        return DB::table($this->order_accpted)->insertGetId($d);
    }
    
    public function order_update_accepted($id){
        return DB::table($this->book_order)->whereid($id)->update(array("status" => 3));
    }
    
    public function getCustomerByJob($id){
        return DB::table($this->book_order.' as o')->select('u.*','o.*','o.id as order_id')->leftJoin($this->users.' as u','u.id','=','o.user_id')->where('o.id',$id)->first();
    }
    public function getOrderAllowed($id){
        $q = DB::table($this->order_accpted)->wherejob_id($id)->count();
        if($q === 0){
            return true;
        }else{
            return false;
        }
    }
    public function getOrders($id,$type){
        $q = DB::table($this->book_order.' as b')->select('b.id as job_id','b.user_id','b.job_name','b.trucking_start_date','b.total_duration','b.job_type','b.material_type','b.loading_location','b.loading_start_time','b.delivery_location','b.loading_end_time','p.name as price_type_name','b.price_type','b.total_price','b.how_long_job','b.amount_of_material','b.no_of_truck_need','b.type_of_truck','b.comment','b.status','b.created_at','b.updated_at')->leftJoin($this->price_type.' as p','p.id','=','b.price_type')->whereuser_id($id)->where('b.status',$type)->orderByDesc('b.id')->paginate();
        if(count($q) > 0){
            return $q;
        }else{
            return 0;
        }
    }
    public function getOrders_truck($id){
        $q = DB::table($this->book_order.' as b')->select('b.id as job_id','b.job_name','b.user_id','b.job_name','b.trucking_start_date','b.total_duration','b.job_type','b.material_type','b.loading_location','b.loading_start_time','b.delivery_location','b.loading_end_time','b.price_type','b.total_price','b.how_long_job','b.amount_of_material','b.no_of_truck_need','b.type_of_truck','b.comment','b.status','b.created_at','b.updated_at','p.name as price_type_name')->leftJoin($this->price_type.' as p','p.id','=','b.price_type')->leftJoin($this->order_accpted.' as oa','oa.job_id','=','b.id')->whereRaw('b.assign_to like "%'.$id.'%"')->orderByDesc('b.id')->paginate();
        if(count($q) > 0){
            return $q;
        }else{
            return 0;
        }
    }
    public function getOrders_truck_count($id){
        $q = DB::table($this->book_order.' as b')->select('b.id as job_id','b.job_name','b.user_id','b.job_name','b.trucking_start_date','b.total_duration','b.job_type','b.material_type','b.loading_location','b.loading_start_time','b.delivery_location','b.loading_end_time','b.price_type','b.total_price','b.how_long_job','b.amount_of_material','b.no_of_truck_need','b.type_of_truck','b.comment','b.status','b.created_at','b.updated_at')->leftJoin($this->order_accpted.' as oa','oa.job_id','=','b.id')->whereRaw('b.status = 1 and assign_to_all = 1 OR b.status = 1 and b.assign_to like "%'.$id.'%" OR oa.driver_id = '.$id)->orderByDesc('b.id')->paginate();
        if(count($q) > 0){
            return count($q);
        }else{
            return 0;
        }
    }
    public function getOrders_wthout_status($id){
        $q = DB::table($this->book_order.' as b')->select('b.id as job_id','b.job_name','b.user_id','b.job_name','b.trucking_start_date','b.total_duration','b.job_type','b.material_type','b.loading_location','b.loading_start_time','b.delivery_location','b.loading_end_time','b.price_type','p.name as price_type_name','b.total_price','b.how_long_job','b.amount_of_material','b.no_of_truck_need','b.type_of_truck','b.comment','b.status','b.created_at','b.updated_at')->leftJoin($this->price_type.' as p','p.id','=','b.price_type')->whereuser_id($id)->orderByDesc('b.id')->paginate();
        if(count($q) > 0){
            return $q;
        }else{
            return 0;
        }
    }
    public function getOrder($id){
        $q = DB::table($this->book_order.' as ob')->select('ob.*','om.*','p.*','v.name as vehicle_type','b.name as body_type','g.name as goods_type_id')->join($this->order_managment.' as om', 'om.trip_id','=','ob.id')->join($this->profile.' as p', 'p.user_id','=','om.user_id')->leftJoin($this->vehicle_type.' as v', 'v.id','=','ob.vehicle_type_id')->leftJoin($this->body_type.' as b', 'b.id','=','p.body_type')->leftJoin($this->goods_type.' as g', 'g.id','=','ob.goods_type_id')->where('om.status','4')->where('ob.id',$id)->get();
        if(count($q) > 0){
            return $q;
        }else
        {
            return 0;
        }
    }
    
    public function total_active_t($user_id){
       return DB::table($this->book_order.' as o')->select('o.id as job_id','o.user_id','o.material_type','o.loading_location','o.delivery_location','o.price_type','o.total_price','o.how_long_job','o.amount_of_material','o.no_of_truck_need','o.type_of_truck','o.comment','o.status','o.created_at','o.updated_at')->leftJoin($this->users.' as u','u.id','=','o.user_id')->whereRaw('(status = 1 AND assign_to_all = 1) OR status = 1 and o.assign_to like "%'.$user_id.'%"')->count();
        
      
    }
    
    public function total_live_t($id){
        return DB::table($this->order_accpted.' as oa')->leftJoin($this->book_order.' as b','b.id','=','oa.job_id')->where('b.status',3)->wheredriver_id($id)->count();
    }
    
    public function total_completed_t($id){
        return DB::table($this->order_accpted.' as oa')->leftJoin($this->book_order.' as b','b.id','=','oa.job_id')->where('b.status',4)->wheredriver_id($id)->count();
    }
    public function total_live_c($id){
        return DB::table($this->order_accpted.' as oa')->leftJoin($this->book_order.' as b','b.id','=','oa.job_id')->where('b.status',3)->wherecustomer_id($id)->count();
    }
    public function total_active_c($id){
        return DB::table($this->book_order)->wherestatus(1)->whereuser_id($id)->count();
    }
    public function total_pending_c($id){
        return DB::table($this->book_order)->wherestatus(0)->whereuser_id($id)->count();
    }
    
    public function total_completed_c($id){
        return DB::table($this->order_accpted.' as oa')->leftJoin($this->book_order.' as b','b.id','=','oa.job_id')->where('b.status',4)->wherecustomer_id($id)->count();
    }
    
    public function total_all_count($id){
        return DB::table($this->order_accpted.' as oa')->RightJoin($this->book_order.' as b','b.id','=','oa.job_id')->whereIn('b.status',[4,3,1,0])->where('b.user_id',$id)->count();
    }
    
    public function total_reject($id){
        return 0;
    }
    
    public function remove_job($id){
        return DB::table($this->book_order)->whereid($id)->delete();
    }
    
    public function byTotal($id,$type){
        if($type == 1){
            return DB::table($this->book_order.' as o')->select(DB::raw('(select COUNT(id) from book_jobs WHERE status = 1 AND id=o.id) as active,(select COUNT(id) from book_jobs WHERE status = 2 AND id=o.id) as Reject,(select COUNT(id) from book_jobs WHERE status = 3 AND id=o.id) as ongoing,(select COUNT(id) from book_jobs WHERE status = 4 AND id=o.id) as completed'))->leftJoin($this->order_accpted.' as oa','oa.job_id','=','o.id')->wherecustomer_id($id)->first();
        }
        if($type == 2){
            return DB::table($this->book_order.' as o')->select(DB::raw('(select COUNT(id) from book_jobs WHERE status = 1 AND id=o.id) as active,(select COUNT(id) from book_jobs WHERE status = 2 AND id=o.id) as Reject,(select COUNT(id) from book_jobs WHERE status = 3 AND id=o.id) as ongoing,(select COUNT(id) from book_jobs WHERE status = 4 AND id=o.id) as completed'))->leftJoin($this->order_accpted.' as oa','oa.job_id','=','o.id')->wheredriver_id($id)->first();
        }
    }
    
    
}
