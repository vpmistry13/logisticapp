<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Geo_Location_User extends Model {

    //
    protected $geo_location = 'geolocation_user';

    public function update_location($id, $lat, $long) {
        $data['user_id'] = $id;
        $data['latitude'] = $lat;
        $data['longitude'] = $long;
        $exit = DB::table($this->geo_location)->whereuser_id($id)->count();
        if ($exit < 1) {
            DB::table($this->geo_location)->insert($data);
        } else {
            DB::table($this->geo_location)->whereuser_id($id)->update($data);
        }
    }

    public function getLocation($user_id) {
        $q = DB::table($this->geo_location)->whereuser_id($user_id)->get();
        return $q;
    }

    public function find_Nearby($lat, $long) {
        $q = DB::table($this->geo_location)->select(DB::raw('user_id,latitude, longitude, SQRT(POW(69.1 * (latitude -  '. $lat.' ), 2) + POW(69.1 * (' . $long . ' - longitude) * COS(latitude / 57.3), 2)) AS distance'))->havingRaw('distance < 50')->orderBy('distance')->get();
        return $q;
        
    }

}
