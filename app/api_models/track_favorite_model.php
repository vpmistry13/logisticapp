<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class track_favorite_model extends Model
{
    //
    protected $primary_table = 'track_favorites';
    
    public function favorite_exit($data) {
        $q = DB::table($this->primary_table)->where($data)->get();       
        return count($q);
    }
    
    public function favorite($data)
    {
       $q = DB::table($this->primary_table)->insertGetId($data);
        return $q;
    }
    public function unfavorite($data)
    {
       $q = DB::table($this->primary_table)->where($data)->delete();
        return $q;
    }
}
