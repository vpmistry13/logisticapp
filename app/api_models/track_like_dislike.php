<?php

namespace App\api_models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class track_like_dislike extends Model
{
    //
    protected $primary_table = 'track_likes';
    
    public function like_exit($data)
    {
        $q = DB::table($this->primary_table)->where($data)->get();       
        return count($q);
    }
    public function track_like($data)
    {
       $q = DB::table($this->primary_table)->insertGetId($data);
        return $q;
    }
    public function track_like_delete($data)
    {
       $q = DB::table($this->primary_table)->where($data)->delete();
        return $q;
    }
}
