<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://localhost/logisticapp/api/*',
        'http://34.219.195.7/logisticapp/api/*',
        'http://18.223.184.223/logisticapp/api/*',
        'http://localhost/logisticapp/admin/Ajax_controller/*',
        'http://34.219.195.7/logisticapp/admin/Ajax_controller/*',
        'http://18.223.184.223/logisticapp/admin/Ajax_controller/*',
    ];
}
