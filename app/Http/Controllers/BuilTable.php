<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuilTable {

    public static function  build_table($array) {
        // start table
        $html = '<table border="1" style="background-color:lightblue;">';
        // header row

        foreach ($array as $key => $value) {
            $html .= '<tr>';
            if($key == 'No of Trucks Needed'):
                $html .= '<th>' . ucfirst(str_replace('_',' ',htmlspecialchars($key))) . '</th><td>' . htmlspecialchars(str_replace('"', '', $value)) . '</td>';
                else:
                $html .= '<th>' . ucfirst(str_replace('_',' ',htmlspecialchars($key))) . '</th><td>' . htmlspecialchars($value) . '</td>';
            endif;
            
            $html .= '</tr>';
        }



        $html .= '</table>';
        return $html;
    }

}
