<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\api_models\signup;
use App\Models\common;
use App\api_models\Book_order_Model;
use Illuminate\Support\Facades\File;
use App\Jobs\SendEmail;
use Illuminate\Support\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;

class Webservices extends Controller {
    protected $admin_email = 'jbctruckloads@gmail.com';
    protected $general_settings;
    public function __construct() {
        $this->users = new Users();
        $this->signup_model = new signup();
        $this->common = new common();
        $this->Book_order_Model = new Book_order_Model();
        $this->general_settings = DB::table('general_settings')->first();
        $this->admin_email = $this->general_settings->email_id;
    }

    // check if exit or not
    public function email_exit(Request $request) {

        if ($request->input('email') != '') {
            $user_exits = $this->signup_model->email_exit($request->input('email'));
            if ($user_exits == 0) {
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = 'new email';
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'email already exit';
            }
        } else {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        }

        echo json_encode($response);
    }

    /// do login
    public function login(Request $request) {

        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'user_password' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $data['email'] = $request->input('email');
            $data['password'] = $request->input('user_password');
            $data['verified'] = 1;
            //attempt login 
            if (Auth::attempt($data)) {
                if ($request->session()->exists('_token')) {
                    $data = $this->users->auth_login($data['email']);
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'Logged In Successfully.';
                    $response['data'] = $data;


                    if ($request->input('user_token') != '') {
                        $up['user_token'] = $request->input('user_token');
                        $where['id'] = Auth::user()->id;
                        $this->users->update_token($up, $where);
                    }
                } else {
                    $response['code'] = 0;
                    $response['status'] = 'failed';
                    $response['message'] = 'Login Again.';
                }
            } else {

                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'invalid credential or account verification pending.';
            }
        }
        echo json_encode($response);
    }

    // make new user
    public function signup(Request $request) {

        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'user_password' => 'required',
                    'mobile_number' => 'required',
                    'user_token' => 'required',
                    'address' => 'required',
                    'company_name' => 'required',
                    'user_type' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            //check email exit or not
            $user_exits = $this->users->email_exit($request->email);
            if ($user_exits == 0) {
                $u['email'] = $request->email;
                $u['first_name'] = $request->first_name;
                $u['last_name'] = $request->last_name;
                $u['mobile_number'] = '+1'.$request->mobile_number;
                $u['user_token'] = $request->user_token;
                $u['password'] = Hash::make($request->user_password);
                $u['user_type'] = $request->user_type;
                $u['company_name'] = $request->company_name;
                if($request->user_type == 1):
                    $u['verified'] = 1;
                endif;
                $p['user_id'] = $this->users->insert_data($u);
                $p['address'] = $request->address;
                if (!empty($p)) {
                    if ($request->type != '') {
                        $p['type'] = $request->type;
                    }
                    if ($request->no_of_truck != '') {
                        $p['no_of_truck'] = $request->no_of_truck;
                    }
                    if ($request->cdl != '') {
                        $p['cdl'] = $request->cdl;
                    }
                    if ($request->office_number != '') {
                        $p['office_number'] = $request->office_number;
                    }
                if ($request->input('driving_licence_image') != '') {
                    $image = $request->input('driving_licence_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "driver_licence-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['driving_licence_image'] = url('storage/app/document/' . $imageName);
                }
                if ($request->input('insurance_image') != '') {
                    $image = $request->input('insurance_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "insurance_image-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['insurance_image'] = url('storage/app/document/' . $imageName);
                }
                if ($request->input('wq_image') != '') {
                    $image = $request->input('wq_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "wq_image-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['wq_image'] = url('storage/app/document/' . $imageName);
                }

                
                if ($request->input('avatar') != '') {
                    $image = $request->input('avatar');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "profile_picture-" . time() . ".png";
                    $path = storage_path() . '/app/profile_image';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['avatar'] = url('storage/app/profile_image/' . $imageName);
                }
                    $name =  $request->company_name;
                    $data['first_name'] = $request->first_name;
                    $data['last_name'] = $request->last_name;
                    $data['company_name'] = $request->company_name;
                    $data['email'] = $request->email;
                    $data['mobile_number'] = $request->mobile_number;
                    if($request->user_type == 2):                        
                        $emailJob = (new SendEmail($this->admin_email,$name,'JBL truck Loads - New Truck Driver/Owner Signup',$data,3))->delay(Carbon::now()->addSeconds(3));
                        $emailJobWelcom = (new SendEmail($request->email,$name,'JBL truck Loads - Welcome',$data,6))->delay(Carbon::now()->addSeconds(3));
                        dispatch($emailJob);
                        dispatch($emailJobWelcom);
                        else:
                        $emailJobWelcomC = (new SendEmail($request->email,$name,'JBL truck Loads - Welcome',$data,5))->delay(Carbon::now()->addSeconds(3));
                        dispatch($emailJobWelcomC);    
                        $emailJob = (new SendEmail($this->admin_email,$name,'JBL truck Loads Signup',$data,8))->delay(Carbon::now()->addSeconds(3));
                        dispatch($emailJob);
                    endif;
                    
                    $this->users->insert_profile($p);
                }
                $response['code'] = 1;
                $response['status'] = 'success';
                if($request->user_type == 2):
                    $response['message'] = 'sign-up awaiting approval';
                    else:
                    $response['message'] = 'signup sucecssfully..!';
                endif;
                
                $response['data'] = $request->all();
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'email already exit';
            }
        }

        echo json_encode($response);
    }

    // profile get
    public function profile_get(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $data = $this->users->getUserData($request->user_id);
            if ($data) {
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = 'profile data loaded';
                $response['data'] = $data;
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'invalid record or not found.';
            }
        }
        echo json_encode($response);
    }

    // logout user
    public function logout(Request $request) {
        Auth::guard()->logout();

        $request->session()->invalidate();

        $response['code'] = 1;
        $response['status'] = "Success";
        $response['message'] = 'You Logged Out';
        echo json_encode($response);
    }

    // make edit profile of user

    public function profile_edit(Request $request) {

        $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'email' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {

            $user_id = $request->input('user_id');
//            $match_id = $request->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
//            print_r($match_id);exit;
            if ($match_id == $user_id) {
                if ($request->input('first_name') != '') {
                    $insert_data['first_name'] = $request->input('first_name');
                }
                if ($request->input('last_name') != '') {
                    $insert_data['last_name'] = $request->input('last_name');
                }
                if ($request->input('company_name') != '') {
                    $insert_data['company_name'] = $request->input('company_name');
                }
                if ($request->input('mobile_number') != '') {
                    $insert_data['mobile_number'] = $request->input('mobile_number');
                }
                if ($request->input('user_token') != '') {
                    $insert_data['user_token'] = $request->input('user_token');
                }
                if ($request->input('user_birthdate') != '') {
                    $insert_data['birth_date'] = $request->input('user_birthdate');
                }
                if ($request->input('user_type') != '') {
                    $insert_data['user_type'] = $request->input('user_type');
                }

                if ($request->input('user_password') != '') {
                    $insert_data['password'] = Hash::make($request->input('user_password'));
                }


                if ($request->input('driving_licence_image') != '') {
                    $image = $request->input('driving_licence_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "driver_licence-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['driving_licence_image'] = url('storage/app/document/' . $imageName);
                }
                if ($request->input('insurance_image') != '') {
                    $image = $request->input('insurance_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "insurance_image-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['insurance_image'] = url('storage/app/document/' . $imageName);
                }
                if ($request->input('wq_image') != '') {
                    $image = $request->input('wq_image');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "wq_image-" . time() . ".png";
                    $path = storage_path() . '/app/document';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['wq_image'] = url('storage/app/document/' . $imageName);
                }

                if ($request->input('type') != '') {
                    $p['type'] = $request->type;
                }
                if ($request->input('no_of_truck') != '') {
                    $p['no_of_truck'] = $request->no_of_truck;
                }
                if ($request->input('cdl') != '') {
                    $p['cdl'] = $request->cdl;
                }
                if ($request->input('avatar') != '') {
                    $image = $request->input('avatar');  // your base64 encoded
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = "profile_picture-" . time() . ".png";
                    $path = storage_path() . '/app/profile_image';
                    File::makeDirectory($path, $mode = 7777, FALSE, true);
                    File::put($path . '/' . $imageName, base64_decode($image));
                    $p['avatar'] = url('storage/app/profile_image/' . $imageName);
                }
                if ($request->input('address') != '') {
                    $p['address'] = $request->address;
                }
                if ($request->input('office_number') != '') {
                    $p['office_number'] = $request->office_number;
                }
                if ($request->input('user_gender') != '') {
                    $p['user_gender'] = $request->user_gender;
                }

                $update = $this->users->update_user_profile($request->user_id, $insert_data);
                $data = $this->users->getUserData($request->user_id);
                foreach ($data as $k => $v) {
                    if ($v == null || $v == '') {
                        $data->$k = "";
                    } else {
                        $data->$k = $v;
                    }
                }
                if (!empty($update) || !empty($p)) {
                    if (!empty($p)) {
                        $this->users->update_user_profile_t($request->user_id, $p);
                    }
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'Update successful';
                    $response['data'] = $data;
                } else {
                    $response['code'] = 0;
                    $response['status'] = 'failed';
                    $response['message'] = 'Noting to update';
                    $response['data'] = $data;
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 're-login or try again.';
            }
        }
        echo json_encode($response);
    }

    /// get list of material type
    public function material_type_get() {
        $response['code'] = 1;
        $response['status'] = 'success';
        $response['message'] = 'material type';
        $response['data'] = $this->common->get_material();
        echo json_encode($response);
    }

    // get price type
    public function price_type_get() {
        $response['code'] = 1;
        $response['status'] = 'success';
        $response['message'] = 'price type';
        $response['data'] = $this->common->get_price_type();
        echo json_encode($response);
    }

    public function truck_type_get() {
        $response['code'] = 1;
        $response['status'] = 'success';
        $response['message'] = 'truck type';
        $response['data'] = $this->common->get_truck_type();
        echo json_encode($response);
    }

    public function book_order(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required',
                    'job_name' => 'required',
                    'trucking_start_date' => 'required',
                    'total_duration' => 'required',
                    'material_type' => 'required',
                    'job_type' => 'required',
                    'loading_location' => 'required',
                    'delivery_location' => 'required',
//                    'loading_start_time' => 'required',
//                    'loading_end_time' => 'required',
                    'price_type' => 'required',
                    'total_price' => 'required|numeric',
                    'how_long_job' => 'required',
                    'amount_of_material' => 'required',
                    'no_of_truck_need' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                 $data = $r->all();
                if($r->job_id){               
                unset($data['job_id']);
                $message = 'job updated successfully';  
                $this->Book_order_Model->book_u($r->job_id,$data);
                }else{                
                $id = $this->Book_order_Model->book($data);
                $u_data =  User::where('id',$user_id)->first();
                $name = $u_data->first_name .' '. $u_data->last_name;
                $price_type = DB::table('price_type')->whereid($r->price_type)->first();
                $type_of_truck = DB::table('truck_type')->whereid($r->type_of_truck)->first();
                $mail_data['job_name'] = $r->job_name;
                $trucking_start_date = \Illuminate\Support\Carbon::parse(@$r->trucking_start_date);
                $mail_data['trucking_start_date'] = @$trucking_start_date->format('F d, Y');
                $mail_data['material_type'] = $r->material_type;
                $mail_data['job_type'] = $r->job_type;
                $mail_data['loading_location'] = $r->loading_location;
                $mail_data['delivery_location'] = $r->delivery_location;
                $mail_data['total_price'] = '$'.$r->total_price;
                $mail_data['price_type'] = @$price_type->name;
                $mail_data['how_long_job'] = $r->how_long_job;
                $mail_data['amount_of_material'] = $r->amount_of_material;
                $mail_data['no_of_truck_need'] = $r->no_of_truck_need;
                $mail_data['type_of_truck'] = @$type_of_truck->name;
                $mail_data['comment'] = $r->comment;
                $emailJob = (new SendEmail($this->admin_email,$name,'JBCTruckLoad : Create Job:',$mail_data,2))->delay(Carbon::now()->addSeconds(3));
                dispatch($emailJob);
                $message = 'job created successfully';
                }
           
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = $message;
                $response['data'] = $r->all();
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function book_order_history(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
                    'history_type' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                if ($r->history_type == 7) {
                    $data = $this->Book_order_Model->getOrders_wthout_status($r->user_id);
                    if ($data != '0') {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'job list';
                        $response['list'] = $data;
                    } else {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'No history found';
                        $response['list']['data'] = array();
                    }
                } else {
                    $data = $this->Book_order_Model->getOrders($r->user_id, $r->history_type);
                    if ($data != '0') {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'job list';
                        $response['list'] = $data;
                    } else {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'No history found';
                        $response['list']['data'] = array();
                    }
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }
    
    public function job_history_truck_owner(Request $r){
        $validator = Validator::make($r->all(), [
                    'driver_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                $data = $this->Book_order_Model->getOrders_truck($r->driver_id);
                if ($data != '0') {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'job list';
                        $response['list'] = $data;
                    } else {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'No history found';
                        $response['list']['data'] = array();
                    }
            }else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }
    
    public function accept_job(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
                    'job_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {

                $allowed = $this->Book_order_Model->getOrderAllowed($r->job_id);
                $order_Data = $this->Book_order_Model->getCustomerByJob($r->job_id);
                if ($allowed) {
                    
                    if(@unserialize($order_Data->no_of_truck_need)):
                        $no_of_truck = unserialize($order_Data->no_of_truck_need);
                        $no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck,1);
                        else:
                        $no_of_truck_need = @$order_Data->no_of_truck_need;    
                    endif;
                    if(@$order_Data->type_of_truck):
                        $get_truck_type = DB::table('truck_type')->whereIn('id',explode(',',$order_Data->type_of_truck))->get();
                        if($get_truck_type):
                            $truck_name = array();
                            foreach($get_truck_type as $t):
                                 $truck_name[] = $t->name;
                            endforeach;
                            $order_Data->type_of_truck =  implode(',',$truck_name);
                        endif;
                    endif;
                    $d['driver_id'] = $r->user_id;
                    $d['job_id'] = $order_Data->order_id;
                    $d['customer_id'] = $order_Data->user_id;
                    $this->Book_order_Model->order_update_accepted($r->job_id);
                    $this->Book_order_Model->order_accpted($d);
                   // $data['job_name'] = @$order_Data->job_name;
                    $u_data =  User::where('id',$user_id)->first();
                    $name = @$u_data->first_name .' '. @$u_data->last_name;
                    $data['Job Name'] = @$order_Data->job_name;
                    $trucking_start_date = \Illuminate\Support\Carbon::parse(@$order_Data->trucking_start_date);
                    $data['Start Date'] = @$trucking_start_date->format('F d, Y');
                    $data['How Long is Job for ?'] = @$order_Data->how_long_job;
                    $data['Type of Material'] = @$order_Data->material_type;
                    $data['Loading Address'] = @$order_Data->loading_location;
                    $data['Delivery Address'] = @$order_Data->delivery_location;
                    $data['Price'] = '$'.@$order_Data->total_price;
                    $data['Type of Truck'] = @$order_Data->type_of_truck;
                    $data['No of Trucks Needed'] = $no_of_truck_need;
                    $emailJob = (new SendEmail($this->admin_email,$name,'JBL truck Loads - Driver Job Accepted',$data,4))->delay(Carbon::now()->addSeconds(3));
                    dispatch($emailJob);
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'Accpted successfully.';
                } else {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'job already accpted by someone.';
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function truck_owner_dashboard_list(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                $data = $this->users->getUserData($r->user_id);
               // if ($data->type != '') {
                    $get_list = $this->Book_order_Model->active_jobs_truckOwner($user_id);
                    if ($get_list) {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'Job List.';
                        $response['total_active'] = 0;
                        $response['total_complete'] = 0;
                        $response['total_ongoing'] = 0;
                        $response['list'] = $get_list;
                    } else {
                        $response['code'] = 1;
                        $response['status'] = 'success';
                        $response['message'] = 'list is empty';
                        $response['total_active'] = 0;
                        $response['total_complete'] = 0;
                        $response['total_ongoing'] = 0;
                        $response['list']['data'] = array();
                    }
//                } else {
//                    $response['code'] = 0;
//                    $response['status'] = 'failed';
//                    $response['message'] = 'please choose your truck type from profile section';
//                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function truck_owner_job_list_active(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                $get_list = $this->Book_order_Model->active_jobs_truckOwner_active($r->user_id);
                if ($get_list) {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'OnGoing Job List';
                    $response['list'] = $get_list;
                } else {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'list is empty';
                    $response['list']['data'] = array();
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function truck_owner_job_list_complete(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                $get_list = $this->Book_order_Model->active_jobs_truckOwner_complete($r->user_id);
                if ($get_list) {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'completed Job List';
                    $response['list'] = $get_list;
                } else {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'list is empty';
                    $response['list']['data'] = array();
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function mark_job_complete(Request $r) {

        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
                    'job_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $user_id = $r->input('user_id');
//            $match_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $match_id = $user_id;
            if ($match_id == $user_id) {
                $get_list = $this->Book_order_Model->mark_as_complete_job($r->job_id, $r->user_id);
                if ($get_list) {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'job completed successfully';
                } else {
                    $response['code'] = 0;
                    $response['status'] = 'failed';
                    $response['message'] = 'something went wrong';
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'login failed';
            }
        }
        echo json_encode($response);
    }

    public function job_dashboard(Request $r) {
        $validator = Validator::make($r->all(), [
                    'user_id' => 'required|numeric',
                    'user_type' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $response['code'] = 1;
            $response['status'] = 'success';
            $response['message'] = 'dashboard list';

            if ($r->user_type == 2) {
                $data = $this->users->getUserData($r->user_id);
               
                $v['active'] = $this->Book_order_Model->total_active_t($r->user_id);               
                $v['pending'] = 0;
                $v['Reject'] = $this->Book_order_Model->total_reject($r->user_id);
                $v['ongoing'] = $this->Book_order_Model->total_live_t($r->user_id);
                $v['completed'] = $this->Book_order_Model->total_completed_t($r->user_id);
                $v['all_jobs'] = $this->Book_order_Model->getOrders_truck_count($r->user_id);
            } else {
                $v['active'] = $this->Book_order_Model->total_active_c($r->user_id);
                $v['pending'] = $this->Book_order_Model->total_pending_c($r->user_id);
                $v['Reject'] = $this->Book_order_Model->total_reject($r->user_id);
                $v['ongoing'] = $this->Book_order_Model->total_live_c($r->user_id);
                $v['completed'] = $this->Book_order_Model->total_completed_c($r->user_id);
                $v['all_jobs'] = $this->Book_order_Model->total_all_count($r->user_id);
            }
            $response['data'] = $v;
        }
        echo json_encode($response);
    }

    public function terms_condition() {
        return 'terms and condition page';
    }
    
    public function job_remove(Request $r){
     $validator = Validator::make($r->all(), [
                    'job_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
        } else {
            $this->Book_order_Model->remove_job($r->job_id);
            $response['code'] = 1;
            $response['status'] = 'success';
            $response['message'] = 'job removed successfully';
        }   
        echo json_encode($response);
    }
    
    public function contact_us(){
        $contct['phone'] = "+1-832-881-0312";
        $contct['email'] = $this->admin_email;
        $res['code'] = 1;
        $res['status'] = 'success';
        $res['data'] = $contct;
        echo json_encode($res);
    }

}
