<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api_models\User_Model;
use App\api_models\signup;
use App\admin_section\Vehicle_Model;
use App\admin_section\Body_Model;
use App\admin_section\Goods_Type_Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\admin_section\OwnerGroup_Model;
use App\Jobs\SendEmail;
use Illuminate\Support\Carbon;
class Ajax_controller extends Controller {

    public function __construct() {
        $this->signup = new signup();
        $this->user_profile = new User_Model();

//        $this->vehicle_type = new Vehicle_Model();
        $this->body_type = new OwnerGroup_Model();
//        $this->good_type = new Goods_Type_Model();
        $this->book_order_model = new \App\admin_section\Book_order_Model();
        $this->notification_master = new \App\admin_section\UserNotification();
    }

    protected $user = 'users';
    protected $job_table = 'book_jobs';

    public function addAccount(Request $r) {
        $validator = Validator::make($r->all(), [
                    'email' => 'unique:users,email',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'email already exit';
        } else { 
        
        $user_type = $r->input('user_type');
        $user = User::where('email', '=', $r->input('email'))->first();
        
        if ($user === null) {
            if ($user_type == '1') {
                
                $signup_data['first_name'] = $r->input('first_name');
                $signup_data['last_name'] = $r->input('last_name');
                $signup_data['company_name'] = $r->input('company_name');
                $signup_data['email'] = $r->input('email');
                $signup_data['mobile_number'] = $r->input('mobile_number');
//                $signup_data['verified'] = 1;
                $signup_data['password'] = Hash::make($r->input('password'));
                $signup_data['user_type'] = $user_type;
               
                $user_id = $this->signup->insert_user($signup_data);
                if ($r->file('image_icon') != '') {
                    $profile_data['avatar'] = url('storage/app/' . Storage::putFile('profile_image', $r->file('image_icon')));
                }
                $profile_data['user_id'] = $user_id;
                $profile_data['address'] = $r->input('user_city');
                $profile_data['user_gender'] = $r->input('user_gender');
                $this->signup->insert_user_p($profile_data);
                if ($user_id != '') {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'signup success';
                } else {
                    $response['code'] = 0;
                    $response['status'] = 'failed';
                    $response['message'] = 'something wrong';
                }
            } else if ($user_type == '2') {
                $signup_data['first_name'] = $r->input('first_name');
                $signup_data['last_name'] = $r->input('last_name');
                $signup_data['company_name'] = $r->company_name;
                $signup_data['email'] = $r->input('email');
                $signup_data['verified'] = 1;
                $signup_data['password'] = Hash::make($r->input('password'));
                $signup_data['user_type'] = $user_type;
                 if ($r->post('mobile_number') != '') {
                    $signup_data['mobile_number'] = $r->post('mobile_number');
                }
                $signup_data['user_status'] = 1;
                $user_id = $this->signup->insert_user($signup_data);
                $profile_data['address'] = $r->input('address');
                $profile_data['cdl'] = $r->input('cdl');
                $profile_data['type'] = $r->input('type');
                $profile_data['no_of_truck'] = $r->input('no_of_truck');
                $profile_data['user_gender'] = $r->input('user_gender');
                $profile_data['office_number'] = $r->input('office_number');
                if ($r->file('image_icon') != '') {
                    $profile_data['avatar'] = url('storage/app/' . Storage::putFile('profile_image', $r->file('image_icon')));
                }
                if ($r->file('wq_image') != '') {
                    $profile_data['wq_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('wq_image')));
                }
                if ($r->file('insurance_image') != '') {
                    $profile_data['insurance_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('insurance_image')));
                }
                if ($r->file('driving_licence_image') != '') {
                    $profile_data['driving_licence_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('driving_licence_image')));
                }
                $profile_data['user_id'] = $user_id;
                $this->signup->insert_user_p($profile_data);
                if ($user_id != '') {
                    $response['code'] = 1;
                    $response['status'] = 'success';
                    $response['message'] = 'signup success';
                } else {
                    $response['code'] = 0;
                    $response['status'] = 'failed';
                    $response['message'] = 'something wrong';
                }
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'failed to perform action';
            }
        } else {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'email already exits';
        }
        }
        echo json_encode($response);
    }

    public function editAccount(Request $r) {
       
        $validator = Validator::make($r->all(), [
                    'email' => 'unique:users,email,'.base64_decode($r->user_id),
                    'no_of_truck' => 'integer|max:10000',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'Email already exit or data invalid entered..';
        } else {
        
        
        $user_type = $r->input('user_type');
        $user_id = base64_decode($r->post('user_id'));
        if ($user_type == '1') {
            $signup_data['first_name'] = $r->input('first_name');
            $signup_data['last_name'] = $r->input('last_name');
            $signup_data['company_name'] = $r->input('company_name');
            $signup_data['email'] = $r->input('email');
            $signup_data['mobile_number'] = $r->input('mobile_number');
            $signup_data['verified'] = 1;
            if ($r->input('password') != '') {
                $signup_data['password'] = Hash::make($r->input('password'));
            }

            $signup_data['user_type'] = 1;
            // print_r($signup_data);exit;
            $this->signup->update_user($user_id, $signup_data);
            if ($r->file('image_icon') != '') {
                $profile_data['avatar'] = url('storage/app/' . Storage::putFile('profile_image', $r->file('image_icon')));
            }
            $profile_data['office_number'] = $r->input('office_number');
            $profile_data['address'] = $r->input('address');
            $profile_data['user_gender'] = $r->input('user_gender');
            
            $this->signup->update_profile($user_id,$profile_data);
            if ($user_id != '') {
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = 'update success';
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'something wrong';
            }
        } else if ($user_type == '2') {
            $signup_data['first_name'] = $r->input('first_name');
            $signup_data['last_name'] = $r->input('last_name');
            $signup_data['company_name'] = $r->input('company_name');
            $signup_data['email'] = $r->input('email');
            $signup_data['verified'] = 1;
            $signup_data['mobile_number'] = $r->input('mobile_number');
            if ($r->input('password') != '') {
                $signup_data['password'] = Hash::make($r->input('password'));
            }
            $signup_data['user_type'] = 2;
            $this->signup->update_user($user_id, $signup_data);
          
            if ($r->file('image_icon') != '') {
                $profile_data['avatar'] = url('storage/app/' . Storage::putFile('profile_image', $r->file('image_icon')));
            }
            
            $profile_data['address'] = $r->input('address');
            $profile_data['type'] = implode(',',$r->input('type'));
            $profile_data['no_of_truck'] = $r->input('no_of_truck');
            $profile_data['cdl'] = $r->input('no_of_truck');
            $profile_data['office_number'] = $r->input('office_number');
            $profile_data['user_gender'] = $r->input('user_gender');
            if ($r->file('wq_image') != '') {
                    $profile_data['wq_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('wq_image')));
            }
            if ($r->file('insurance_image') != '') {
                $profile_data['insurance_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('insurance_image')));
            }
            if ($r->file('driving_licence_image') != '') {
                $profile_data['driving_licence_image'] = url('storage/app/' . Storage::putFile('driver_data', $r->file('driving_licence_image')));
            }
            $this->signup->update_profile($user_id,$profile_data);
            if ($user_id != '') {
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = 'Update Success';
            } else {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'something wrong';
            }
        } else {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'failed to perform action';
        }
        
        }
        echo json_encode($response);
    }

    public function addVehicle(Request $r) {

        $data['name'] = $r->post('vehicle_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->vehicle_type->addVehicle($data);
    }

    public function addBodytype(Request $r) {

        $data['name'] = $r->post('body_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->body_type->addBody($data);
    }

    public function addGoodtype(Request $r) {

        $data['name'] = $r->post('good_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->good_type->addGood($data);
    }

    public function editVehicle(Request $r) {
        $id = $r->input('user_id');
        $data['name'] = $r->post('vehicle_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->vehicle_type->editVehicle($data, base64_decode($id));
    }

    public function editBodytype(Request $r) {
        $id = $r->input('id');
        $data['name'] = $r->post('body_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->body_type->editBody($data, $id);
    }

    public function editGoodtype(Request $r) {
        $id = $r->input('id');
        $data['name'] = $r->post('good_name');
        if ($r->file('image_icon') != '') {
            $data['image_icon'] = Storage::putFile('image_icon', $r->file('image_icon'));
        }
        $data['status'] = $r->post('status');
        $this->good_type->editBody($data, $id);
    }

    public function deleteVehicle($id) {
        $this->vehicle_type->deleteRemove($id);
    }

    public function deleteBodytype($id) {
        $this->body_type->deleteRemove($id);
    }

    public function deleteGoodtype($id) {
        $this->good_type->deleteRemove($id);
    }

    public function deleteUserRemove($id) {
        $this->signup->delete_user($id);
    }
    public function verifyUser($id) {
        $this->signup->verify_user($id);
        $user_data = DB::table('users')->whereid($id)->first();
        if($user_data):
            $name = $user_data->first_name.' '.$user_data->last_name;
            $user_email = $user_data->email;
            $emailJob = (new SendEmail($user_email,$name,'JBCTruckLoad : Your account has been approved.',array(),7))->delay(Carbon::now()->addSeconds(3));
            dispatch($emailJob);
            $message = 'Hi {name}
Your account at JBC Truck Loads has been approved. You can now log in using your chosen email and password to see our available jobs.
Thank you,
JBC Truck Loads';
            $message = str_replace('{name}', $user_data->first_name, $message);
            $smsjob = (new \App\Jobs\SendSMS($user_data->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
            dispatch($smsjob);
        endif;
        
    }

    public function editProfile(Request $r) {
       
      //  print_r($r->all());
        $p = array();
        if ($r->post('first_name') != '') {
            $data['first_name'] = $r->post('first_name');
        }
        if ($r->post('last_name') != '') {
            $data['last_name'] = $r->post('last_name');
        }
        if ($r->post('user_city') != '') {
            $p['address'] = $r->post('address');
        }
        if ($r->post('user_gender') != '') {
            $p['user_gender'] = $r->post('user_gender');
        }
        if ($r->post('mobile_number') != '') {
            $data['mobile_number'] = $r->post('mobile_number');
        }
        if ($r->file('avatar') != '') {
            $p['avatar'] = url('storage/app/' . Storage::putFile('User_Profile', $r->file('avatar')));
        }
        
        $this->user_profile->profile_edit_u($data, Auth()->user()->id);
        if(count($p) > 0){
            $this->user_profile->profile_edit($p, Auth()->user()->id);
        }
        
    }

    public function editOrderShipping(Request $r) {
        print_r($r->all());  
     
        if ($r->post('material_type') != '') {
            $update_data['material_type'] = $r->post('material_type');
        }
        if ($r->post('job_name') != '') {
            $update_data['job_name'] = $r->post('job_name');
        }
        if ($r->post('trucking_start_date') != '') {
            $update_data['trucking_start_date'] = date('Y-m-d h:m:s',strtotime($r->post('trucking_start_date')));
        }
        if ($r->post('loading_start_time') != '') {
            $update_data['loading_start_time'] = date('Y-m-d h:m:s',strtotime($r->post('loading_start_time')));
        }
        if ($r->post('loading_end_time') != '') {
            $update_data['loading_end_time'] = date('Y-m-d h:m:s',strtotime($r->post('loading_end_time')));
        }
        if ($r->post('job_type') != '') {
            $update_data['job_type'] = $r->post('job_type');
        }
        if ($r->post('price_type') != '') {
            $update_data['price_type'] = $r->post('price_type');
        }
        if ($r->post('total_price') != '') {
            $update_data['total_price'] = $r->post('total_price');
        }
        if ($r->post('how_long_job') != '') {
            $update_data['how_long_job'] = $r->post('how_long_job');
        }
        if ($r->post('amount_of_material') != '') {
            $update_data['amount_of_material'] = $r->post('amount_of_material');
        }
        if ($r->post('no_of_truck_need') != '') {
//            $update_data['no_of_truck_need'] = serialize($r->post('no_of_truck_need'));
            $update_data['no_of_truck_need'] = $r->post('no_of_truck_need');
        }
        if ($r->post('comment') != '') {
            $update_data['comment'] = $r->post('comment');
        }
        if ($r->post('loading_location') != '') {
            $update_data['loading_location'] = $r->post('loading_location');
        }
        if ($r->post('delivery_location') != '') {
            $update_data['delivery_location'] = $r->post('delivery_location');
        }
        if ($r->post('type_of_truck') != '') {
            $update_data['type_of_truck'] = implode(',',$r->post('type_of_truck'));
        }
        if ($r->post('status') != '') {
            $update_data['status'] = $r->post('status');
        }
        print_r($update_data);
        if (!empty($update_data)) {
            $this->book_order_model->editOrder(base64_decode($r->post('id')), $update_data);
        }
    }

    public function remove_order_shipping($id) {
        DB::table($this->job_table)->whereid($id)->delete();
    }
    
    public function shiping_status($id,$status,Request $r){
         $shipment_data = DB::table($this->job_table)->whereid($id)->first();
         $notification_type = $r->notification_type;
         $email_data = array();
         $email_data['message_body'] = $r->message_body;
         if($shipment_data):
            $email_data['comment'] = $shipment_data->comment;    
         endif;
        
         if($r->assign_to_all):
             $group_data = DB::table('users')->whereuser_type(2)->whereverified(1)->get();
            
                if($group_data):
                    $users_id = array();
                   
                    foreach($group_data as $v):     
                         $message = 'Hi {name}
A new Trucking Job is available from JBC Truck Loads. Please log into the app to see details
Thanks,
JBC Truck Loads';
                           if($notification_type == 1){
                               // sending text and email
                             $emailJob = (new SendEmail($v->email,$v->company_name,'JBCTruckLoad : Assign JOB:',$email_data,1))->delay(Carbon::now()->addSeconds(3));
                             dispatch($emailJob);
                             $message = str_replace('{name}', $v->company_name, $message);
                             $smsjob = (new \App\Jobs\SendSMS($v->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                             dispatch($smsjob);
                           }elseif($notification_type == 2){
                               // sending Email onyl
                             $emailJob = (new SendEmail($v->email,$v->company_name,'JBCTruckLoad : Assign JOB:',$email_data,1))->delay(Carbon::now()->addSeconds(3));
                             dispatch($emailJob);
                           }elseif($notification_type == 3){
                               // sending sms only
                               
                             $message = str_replace('{name}', $v->company_name, $message);
                             $smsjob = (new \App\Jobs\SendSMS($v->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                             dispatch($smsjob);
                           }
                            
                    endforeach;

               $unsers_assign_this_job = implode(',',array_unique($users_id));
               $notification_type = $r->notification_type;
               if(!$notification_type):
                   $notification_type = "";
               endif;
               DB::table($this->job_table)->whereid($id)->update(array("status" => $status,'notify_type' => $notification_type,'assign_to_all' => 1));
               endif;
         endif;
        
         if($r->groups_ids):
                $groups_ids = explode(',', str_replace(['[',']','"'], '', $r->groups_ids));
                $group_data = DB::table('truck_owner_group')->whereIn('id',$groups_ids)->get();
                if($group_data):
                    $users_id = array();
                    foreach($group_data as $m):
                        
                        $data = unserialize($m->users_ids);
                        if($data):                    
                           foreach($data as $v):
                            $message = 'Hi {name}
A new Trucking Job is available from JBC Truck Loads. Please log into the app to see details
Thanks,
JBC Truck Loads';
                            array_push($users_id, $v); 
                            $m = DB::table('users')->select('email','first_name as name','company_name','mobile_number')->whereid($v)->first();
                            if($m):
                            if($notification_type == 1){
                               // sending text and email
                             $emailJob = (new SendEmail($m->email,$m->company_name,'JBCTruckLoad : Assign JOB:',$email_data,1))->delay(Carbon::now()->addSeconds(3));
                             dispatch($emailJob);
                             $message = str_replace('{name}', $m->company_name, $message);
                             $smsjob = (new \App\Jobs\SendSMS($m->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                             dispatch($smsjob);
                           }elseif($notification_type == 2){
                               // sending Email onyl
                             $emailJob = (new SendEmail($m->email,$m->company_name,'JBCTruckLoad : Assign JOB:',$email_data,1))->delay(Carbon::now()->addSeconds(3));
                             dispatch($emailJob);
                           }elseif($notification_type == 3){
                               // sending sms only
                               
                             $message = str_replace('{name}', $m->company_name, $message);
                             $smsjob = (new \App\Jobs\SendSMS($m->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                             dispatch($smsjob);
                           }    
                           // $emailJob = (new SendEmail($m->email,$m->company_name,'JBCTruckLoad : Assign JOB:',$email_data,1))->delay(Carbon::now()->addSeconds(3));
                            //dispatch($emailJob);
                            endif;
                           endforeach;

                        endif;                 
                    endforeach;

               $unsers_assign_this_job = implode(',',array_unique($users_id));
               $notification_type = $r->notification_type;
               if(!$notification_type):
                   $notification_type = "";
               endif;
               DB::table($this->job_table)->whereid($id)->update(array("status" => $status,'assign_to' => $unsers_assign_this_job,'notify_type' => $notification_type));
               endif;
         endif;
         
        
    }
    public function send_message_bulk(Request $r){
//        echo '<pre>';
//        print_r($r->all());
        $group_emails = array();
        if($r->group_id):
            $group_ids = $r->group_id;
            $get_group_data = DB::table('truck_owner_group')->whereIn('id',$group_ids)->get();
            
            if($get_group_data):
                foreach($get_group_data as $g):
                    if(@unserialize($g->users_ids)):
                        $users_in_group = unserialize($g->users_ids);
                       $get_users = DB::table('users')->whereIn('id',$users_in_group)->get();
                       if($get_users):
                           foreach($get_users as $e):
                                if(@$e->id):
                                    $group_emails[] = $e->id;
                                endif;
                           endforeach;
                       endif;
                    endif;

                endforeach;
            endif;
        endif;
        if($r->email_ids && $group_emails):
            $array_merger = array_unique(array_merge($r->email_ids,$group_emails));
            else:
            if($r->email_ids):
                $array_merger = array_unique($r->email_ids);    
                else:
                    if($group_emails):
                $array_merger = array_unique($group_emails);  
                    endif;
            endif;    
            
        endif;
        
         $notification_type = $r->notification_type;
         
         $email_data = $r->message_body;
         $email_subject = $r->message_subject;
         if(!$email_subject):
             $email_subject = "JBCTruck Loads : Message";
         endif;
         if($array_merger):
             $group_data = DB::table('users')->whereIn('id',$array_merger)->whereuser_type(2)->get();
//             print_r($group_data);exit;
        
                if($group_data):
                    $users_id = array();
                   
                    foreach($group_data as $v):     
                         $message = 'Hi {name}
{message_body}
Thank You,
JBC Truck Loads';
                        $name = @$v->company_name;
                        if(!$name):
                            $name = @$v->first_name.' '.@$v->last_name;
                        endif;                        
                           if($notification_type == 1){
                               // sending text and email
                             if($v->email){
                                $emailJob = (new SendEmail($v->email,$name,$email_subject,$email_data,9))->delay(Carbon::now()->addSeconds(3));
                                dispatch($emailJob);                                
                             }
                             $message = str_replace('{name}', $name, $message);
                             $message = str_replace('{message_body}', $r->message_body, $message);
                            if($v->mobile_number){
                                $smsjob = (new \App\Jobs\SendSMS($v->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                                dispatch($smsjob);
                            }
                            
                           }elseif($notification_type == 2){
                               // sending Email onyl
                             if($v->email){  
                                $emailJob = (new SendEmail($v->email,$name,$email_subject,$email_data,9))->delay(Carbon::now()->addSeconds(3));
                                dispatch($emailJob);
                             }
                           }elseif($notification_type == 3){
                               // sending sms only
                               
                             $message = str_replace('{name}', $name, $message);
                             $message = str_replace('{message_body}', $r->message_body, $message);
                             if($v->mobile_number){
                                $smsjob = (new \App\Jobs\SendSMS($v->mobile_number,$message))->delay(Carbon::now()->addSeconds(3));
                                dispatch($smsjob);
                             }
                           }
                            
                    endforeach;
                    endif;
                    endif;

        
         
        
    }
    public function shiping_status_admin($id,$status){
        DB::table($this->job_table)->whereid($id)->update(array("status" => $status));
    }

    public function editMasterNotification(Request $r) {
        if ($r->post('message') != '') {
            $update_data['message'] = $r->post('message');
        }
        if ($r->post('status') != '') {
            $update_data['status'] = $r->post('status');
        }

        if (!empty($update_data)) {
            $this->notification_master->editMasterNoti(base64_decode($r->post('id')), $update_data);
        }
    }

    public function editSettings(Request $r) {
//        if ($r->post('per_km_rate') != '') {
//            $update_data['per_km_rate'] = $r->post('per_km_rate');
//        }
        if ($r->post('header_title') != '') {
            $update_data['header_title'] = $r->post('header_title');
        }
        if ($r->file('site_logo') != '') {
            $update_data['site_logo'] = Storage::putFile('site_logo', $r->file('site_logo'));
        }
        if ($r->post('admin_title') != '') {
            $update_data['admin_title'] = $r->post('admin_title');
        }
        if ($r->post('email_id') != '') {
            $update_data['email_id'] = $r->post('email_id');
        }
        if ($r->post('email_pwd') != '') {
            $update_data['email_credential'] = base64_encode($r->post('email_pwd'));
        }
        if (!empty($update_data)) {
            DB::table('general_settings')->update($update_data);
        }
    }

    public function documentVerify($id) {
        $update['document_verification'] = 1;
        return DB::table('profile')->whereuser_id($id)->update($update);
    }

    public function sales_data_map() {
        return DB::table('order_booking')->selectRaw("DATE(created_at) as date,COUNT(id) as units")->groupBy('date')->get();
    }

    public function sales_data_revenue_map() {
        return DB::table('order_booking')->selectRaw("DATE(created_at) as date_revenue,SUM(estimate_cost) as amount")->groupBy('date_revenue')->get();
    }
    
    public function get_emails_by_ids(Request $r){
        if(!empty($r->email_ids)) :
        $emails =  DB::table('users')->select('id','email','first_name','last_name','company_name')->whereIn('id',$r->email_ids)->get();
        $res['code'] = 1;
        $res['data'] = $emails;
        echo json_encode($res);
        endif;
    }
    
    public function save_truck_owner_group(Request $r){      
        if($r->all()){
            $data['admin_id'] = $r->admin_id;
            $data['users_ids'] = serialize($r->email_ids);
            $data['group_name'] = $r->group_name;
            DB::table('truck_owner_group')->insert($data);
        }
    }
    public function add_to_group(Request $r){
        echo '<pre>';
        print_r($r->all());
        if($r->group_id):
            foreach($r->group_id as $g):
                $group_data = DB::table('truck_owner_group')->whereid($g)->first();
                $already_exit = unserialize(@$group_data->users_ids);
                print_r($already_exit);
                if ($already_exit):
                    if ($r->email_ids):
                        foreach ($r->email_ids as $e):
                            array_push($already_exit, $e);                            
                        endforeach;
                    endif;     
                    $update_ids = serialize(array_unique($already_exit));
                    DB::table('truck_owner_group')->whereid($g)->update(array('users_ids' => $update_ids));
                else:
                    $emails = array();
                    if ($r->email_ids):
                        foreach ($r->email_ids as $e):
                            array_push($emails, $e);                            
                        endforeach;
                    endif;     
                    $update_ids = serialize(array_unique($emails));
                    DB::table('truck_owner_group')->whereid($g)->update(array('users_ids' => $update_ids));
                endif;
            endforeach;
        endif;
    }
    
    public function edit_truck_owner_group(Request $r){
        if($r->id){
            $data['group_name'] = $r->group_name;
            $data['users_ids'] = serialize($r->users_ids);
            DB::table('truck_owner_group')->whereid(base64_decode($r->id))->update($data);
            $res['code'] = 1;
            $res['message'] = 'Group data has been updated.';
        }
        else{
            $res['code'] = 0;
            $res['message'] = 'something went wrong';
        }
        echo json_encode($res);
    }
    public function get_truck_owner_group(){
        $data = DB::table('truck_owner_group')->get();
       
        if($data):
            foreach($data as $g):
                $usersId = @unserialize($g->users_ids);
                if($usersId):
                    $g->inList = DB::table('users')->whereIn('id',$usersId)->get();
                    else:
                    $g->inList = "";    
                endif;                
            endforeach;
        endif;
        $res['code'] = 1;
        $res['data'] = $data;
        echo json_encode($res);
    }
    
}
