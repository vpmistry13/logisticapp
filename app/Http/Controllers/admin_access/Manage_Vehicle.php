<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin_section\Vehicle_Model;

class Manage_Vehicle extends Controller
{   
    public function __construct() {
        $this->vehicle_model = new Vehicle_Model();
    }

    public function index(){    
       return view('admin_section.vehicle.VehicleManagment');
    }
    
    public function list_json(){
        return $this->vehicle_model->getListJSON();
    }
    
    public function editVehicle($id){
        $data['user_id'] = $id;
        $data['vehicle_data'] = $this->vehicle_model->getByid(base64_decode($id));
        return view('admin_section.vehicle.EditVehicle',$data);
    }
    
    public function addVehicle(){
        return view('admin_section.vehicle.AddVehicle');
    }
}
