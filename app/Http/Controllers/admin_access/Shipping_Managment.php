<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api_models\User_Model;
use App\admin_section\Book_order_Model;
use Illuminate\Support\Facades\Config;
use App\admin_section\Goods_Type_Model;
use App\admin_section\Vehicle_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class Shipping_Managment extends Controller
{
    public function __construct() {
        $this->middleware('auth:web');
        $this->user_model =  new User_Model();
        $this->book_order_model = new Book_order_Model();
        $this->limit = Config::get('app.limit');
        $this->goods_type = new Goods_Type_Model();
        $this->vehicle_type = new Vehicle_Model();
        $this->common = new \App\Models\common();
    }
    
    public function shipping_list_complete(){
        $data['list_type'] = 'order_shipping_list';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_create(){
        $data['vehicle_type'] = $this->common->get_truck_type();
        $data['price_type'] = $this->common->get_price_type();
        $data['list_type'] = 'create_shipping';
        echo view('admin_section.shipping.CreateShipping',$data);
    }
    public function shipping_create_job(Request $r){
          
          $validator = Validator::make($r->all(), [
                    'job_name' => 'required',
//                    'total_duration' => 'required',
                    'job_type' => 'required',
                    'material_type' => 'required',
                    'loading_location' => 'required',
                    'delivery_location' => 'required',
                    'price_type' => 'required',
                    'total_price' => 'required',
                    'type_of_truck' => 'required',
                    'how_long_job' => 'required',
        ]);
        if ($validator->fails()) {
             return Redirect::back()->with('error', 'something went wrong');
        } else {
            $s['user_id'] = Auth::user()->id;
            $s['job_name'] = $r->job_name;
            $s['material_type'] = $r->material_type;
            $s['trucking_start_date'] = $r->trucking_start_date;
            if($r->total_duration):
            $s['total_duration'] = $r->total_duration;
                else:
            $s['total_duration'] = 0;    
            endif;
            
            $s['job_type'] = $r->job_type;
            $s['price_type'] = $r->price_type;
            $s['total_price'] = $r->total_price;
            $s['comment'] = $r->comment;
            $s['loading_location'] = $r->loading_location;
            $s['delivery_location'] = $r->delivery_location;
            $s['type_of_truck'] = $r->type_of_truck;
//            $s['no_of_truck_need'] = serialize($r->no_of_truck_need);
            $s['no_of_truck_need'] = $r->no_of_truck_need;
            $s['how_long_job'] = $r->how_long_job;
            $s['type_of_truck'] = implode(',',$r->type_of_truck);
            $this->book_order_model->book($s);
            return Redirect::back()->with('success', 'Your Job Created.');
        }
    }
    public function shipping_list_all(){
        $data['list_type'] = 'order_shipping_list_all';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_list_active(){
        $data['list_type'] = 'order_shipping_active_list';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_list_live(){
        $data['list_type'] = 'order_shipping_live_list';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_list_pending(){
        $data['list_type'] = 'order_shipping_pending_list';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_list_reject(){
        $data['list_type'] = 'order_shipping_upcoming_list';
        echo view('admin_section.shipping.shippingManagment',$data);
    }    
    public function shipping_list_json(){
        return $this->book_order_model->getOrderListJSON($this->limit);
    }    
    public function shipping_list_all_json(){
        return $this->book_order_model->getOrderListAllJSON($this->limit);
    }    
    public function shipping_active_list_json(){
        return $this->book_order_model->getOrderActiveListJSON($this->limit);
    }    
    public function shipping_live_list_json(){
        return $this->book_order_model->getOrderLiveListJSON($this->limit);
    }    
    public function shipping_pending_list_json(){
        return $this->book_order_model->getOrderPendingListJSON($this->limit);
    }    
    public function shipping_upcoming_list_json(){
        return $this->book_order_model->getOrderUpcomingListJSON($this->limit);
    }    
    
    public function editShipping($id){
        $data['id'] = $id;
        $data['shipping_data'] = $this->book_order_model->getOrder(base64_decode($id));
        $data['vehicle_type'] = $this->common->get_truck_type();
        $data['price_type'] = $this->common->get_price_type();
        echo view('admin_section.shipping.EditShipping',$data);
    }
}
