<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class General_settings extends Controller
{
   
    protected $general_settings = 'general_settings';


    public function getSettings(){
       $data['list'] = DB::table($this->general_settings)->get();
       return view('admin_section.settings.SettingManagment',$data);
    }
    public function editSettings(){
        $data['list'] = DB::table($this->general_settings)->get();
        return view('admin_section.settings.EditSettings',$data);
    }
}
