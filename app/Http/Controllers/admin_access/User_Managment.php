<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api_models\User_Model;
use Illuminate\Support\Facades\Config;
use App\Models\common;

class User_Managment extends Controller
{
    public function __construct() {
        $this->middleware('auth:web');
        $this->common = new common();
        $this->limit = Config::get('app.limit');
        $this->user_model =  new User_Model();
    }
    
    public function customer_list(){
        echo view('admin_section.customer.CustomerManagment');
    }
    public function customer_list_json(){
        return $this->user_model->getCustomerListJSON($this->limit);
    }
    
    public function add_customer(){
        
        echo view('admin_section.customer.AddCustomer');
    }
    
    public function edit_customer($id){
        $data['user_id'] = $id;
        $data['customer_data'] = $this->user_model->getUser(base64_decode($id));
        echo view('admin_section.customer.EditCustomer',$data);
    }
    
    public function edit_driver($id){
        $data['user_id'] = $id;
        $data['customer_data'] = $this->user_model->getUser(base64_decode($id));
        $data['type_of_truck'] = $this->common->get_truck_type();
        echo view('admin_section.driver.EditDriver',$data);
    }
    
    public function driver_list(){
        $data['driver_list'] = $this->user_model->getDriverList();
        echo view('admin_section.driver.DriverManagment',$data);
    }
    
    public function driver_list_json(){
        return $this->user_model->getDriverListJSON($this->limit);
    }
    
    public function add_driver(){
        $data['type_of_truck'] = $this->common->get_truck_type();
        echo view('admin_section.driver.AddDriver',$data);
    }
}
