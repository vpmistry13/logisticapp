<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Manage_Body extends Controller
{
    public function __construct() {
        $this->body_model =  new \App\admin_section\Body_Model();
    }
    
    public function index(){     
       return view('admin_section.vehicle.BodyManagment');
    }
    
    public function list_json(){
        return $this->body_model->getListJSON();
    }
    
    public function editBody($id){
        $data['id'] = $id;
        $data['vehicle_data'] = $this->body_model->getByid(base64_decode($id));
        return view('admin_section.vehicle.EditBody',$data);
    }
    
    public function addBody(){
        return view('admin_section.vehicle.AddBody');
    }
}
