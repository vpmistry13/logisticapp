<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin_section\Goods_Type_Model;

class Manage_Goods_Type extends Controller
{
    public function __construct() {
        $this->goods_type_model =  new Goods_Type_Model();
    }
    
    public function index(){     
       return view('admin_section.vehicle.GoodTypeManagment');
    }
    
    public function goodsList(){
        return $this->goods_type_model->getListJSON();
    }
    
    public function editGoodsType($id){
        $data['id'] = $id;
        $data['vehicle_data'] = $this->goods_type_model->getByid(base64_decode($id));
        return view('admin_section.vehicle.EditGoodType',$data);
    }
    
    public function addGoodsType(){
        return view('admin_section.vehicle.AddGoodType');
    }
}
