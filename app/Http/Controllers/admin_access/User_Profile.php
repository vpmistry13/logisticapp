<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api_models\User_Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class User_Profile extends Controller
{
    public function __construct() {
        $this->middleware('auth:web');
        $this->user_model =  new User_Model();
    }
    
    public function index(Request $r){
        $data['info'] = $this->user_model->getUserProfile(Auth()->user()->id);
      
        echo view('admin_section.profile.profileEdit',$data);
    }
    public function editProfile(Request $r){
        if($r->post('full_name') != ''){
            $data['full_name'] = $r->post('full_name');
        }
        if($r->post('user_city') != ''){
            $data['user_city'] = $r->post('user_city');
        }
        if($r->post('user_gender') != ''){
            $data['user_gender'] = $r->post('user_gender');
        }
        if($r->post('mobile_number') != ''){
           $data['mobile_number'] = $r->post('mobile_number'); 
        }        
        if($r->file('avatar') != ''){
            $data['avatar'] = Storage::files('User_Profile',$r->file('avatar'));
        }        
        print_r($data);exit;
        $this->user_model->profile_edit($data,Auth()->user()->id);
    }
   
}
