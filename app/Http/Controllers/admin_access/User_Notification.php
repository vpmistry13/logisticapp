<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin_section\UserNotification;

class User_Notification extends Controller
{
    //
    public function __construct() {
        $this->user_notification = new UserNotification();
    }
    
    public function index(){
        return view('admin_section.notification.NotificationManagment');
    }
    public function getListJSON(){
        return $this->user_notification->getJSON();
    }
    public function editNotification($id){
        $data['id'] = $id;
        $data['notification_data'] = $this->user_notification->notification_master(base64_decode($id));
        return view('admin_section.notification.EditNotification',$data);
    }
}
