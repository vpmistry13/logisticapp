<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Dashboard extends Controller
{
    protected $users = 'users';
    protected $book_jobs = 'book_jobs';
    public function __construct() {
        $this->middleware('auth:web');
        
    }
    
    public function index(){   
       $data['total_users'] = DB::table($this->users)->whereuser_type(1)->count();
       $data['total_drivers'] = DB::table($this->users)->whereuser_type(2)->count();;
       $data['total_complete'] = DB::table($this->book_jobs)->wherestatus(4)->count();
       $data['total_active'] = DB::table($this->book_jobs)->wherestatus(1)->count();
       $data['total_pending'] = DB::table($this->book_jobs)->wherestatus(0)->count();
       $data['total_live'] = DB::table($this->book_jobs)->whereIn('status',[1,3])->count();
       $data['total_reject'] = DB::table($this->book_jobs)->wherestatus(3)->count();
       $data['total_all'] = DB::table($this->book_jobs)->whereNotIn('status',[2])->count();
       echo view('admin_section.dashboard.index',$data); 
    }
    
}
