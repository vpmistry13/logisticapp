<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin_section\OwnerGroup_Model;

class TruckOwnerGroup extends Controller
{
   public function __construct() {
       $this->ownerGroupModel = new OwnerGroup_Model();
   }
   
   public function index(){
       return view('admin_section.truck_owner_group.DriverGroupManagment');
   }
   public function editGroup($group_id){
       $data['group_data'] = $this->ownerGroupModel->getGroup(base64_decode($group_id));
       $data['group_id'] = $group_id;
       if(@$data['group_data']->users_ids){
           $users_ids = $data['group_data']->users_ids;
           $data['users_in_group'] = $this->ownerGroupModel->getUsersEmails(unserialize($users_ids));
       }
       return view('admin_section.truck_owner_group.EditGroup',$data);
   }

   public function get_group_list_json(){
       return $this->ownerGroupModel->get_group_list();
   }
}
