<?php

namespace App\Http\Controllers\admin_access;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\admin_section\ReportModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Excel;
use Illuminate\Support\Facades\DB;
use App\Exports\UsersExport;
use App\Exports\JobsExport;
class ReportManagment extends Controller
{
   public function __construct() {
       $this->report = new ReportModel();
   } 
    
   public function index(){
       $data['report_data'] = $this->report->getReport();
       return view('admin_section.report.reportManagment',$data);
   }
   
   public function generate_report(Request $r){
       $validator = Validator::make($r->all(), [
                    'report_type' => 'required|numeric',
                    'from_date' => 'required',
                    'to_date' => 'required',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
            return Redirect::back()->with('error', 'something went wrong');
        } else {
           
            $response['code'] = 1;
            $response['status'] = 'success';
            $gen_data['report_type'] = $r->report_type;
            $gen_data['from_date'] = $r->from_date;
            $gen_data['to_date'] = $r->to_date;
            
          
            
            if($r->report_type == 3):
                $status = $r->status;
                $filename = 'jobs_report_'.Date('y-m-d h:m:s').'.xlsx';
                if($status):
                    Excel::store(new JobsExport($status,$r->from_date,$r->to_date),$filename);
                    else:
                    Excel::store(new JobsExport(0,$r->from_date,$r->to_date),$filename);
                endif;                
            else:
                $filename = 'users_report_'.Date('y-m-d h:m:s').'.xlsx';    
                Excel::store(new UsersExport($r->report_type,$r->from_date,$r->to_date),$filename);                
            endif;
            
            $gen_data['report_url'] = $filename;
            $this->report->generate_report($gen_data);
            return Redirect::back()->with('success', 'Your Report Generated Successfully.');
            
        }
   }
   
   public function remove_report(Request $r){
       $validator = Validator::make($r->all(), [
                    'report_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'required parameter';
            return Redirect::back()->with('error', 'something went wrong');
        } else {
            $response['code'] = 1;
            $response['status'] = 'success';
            $this->report->generate_report_remove($r->report_id);
            return Redirect::back()->with('success', 'Your Report has been removed.');
            
        }
   }
}
