<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function getResetToken(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
        ]);

        if (!$validator->fails()) {
            $user = User::where('email', $request->input('email'))->first();
           
            if ($user['email'] != '') {
                $this->sendResetLinkEmail($request);
                $response['code'] = 1;
                $response['status'] = 'success';
                $response['message'] = 'A reset link has been sent to your mailbox.';
            }
            else
            {
                $response['code'] = 0;
                $response['status'] = 'failed';
                $response['message'] = 'user not associate with this email';
            }
            
        } else {
            $response['code'] = 0;
            $response['status'] = 'failed';
            $response['message'] = 'something is wrong';            
        }
        echo json_encode($response);
    }
}
