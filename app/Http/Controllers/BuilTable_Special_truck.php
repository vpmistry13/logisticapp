<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuilTable_Special_truck {

    public static function  build_table($array, $condition = 0) {
        if($condition == 1){
            if($array):
                $html = "";
                foreach ($array as $key => $value) {
                  
                 $html .= $key. ':' .$value[0] .', ';
                   
                }
                return $html;
            endif;
        }
        else{
        // start table
        $html = '<table border="1" style="background-color:lightblue;">';
        // header row

        foreach ($array as $key => $value) {
            $html .= '<tr>';
            $html .= '<th>' . ucfirst(str_replace('_','',htmlspecialchars($key))) . '</th><td>' . htmlspecialchars($value[0]) . '</td>';
            $html .= '</tr>';
        }



        $html .= '</table>';
        return $html;
    }
    }

}
