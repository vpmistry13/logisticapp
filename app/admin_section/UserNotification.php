<?php

namespace App\admin_section;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UserNotification extends Model {

    //
    protected $user_notification = 'user_notification';
    protected $notification_master = 'notification_master';

    public function set($data) {
        DB::table($this->user_notification)->insert($data);
    }

    public function editMasterNoti($id,$data){
        DB::table($this->notification_master)->whereid($id)->update($data);
    }
    
    public function get($user_id, $limit) {
        $q = DB::table($this->user_notification)->whereuser_id($user_id)->wherestatus(0)->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getJSON() {
        $q = DB::table($this->notification_master)->wherestatus(1)->get();
       return Datatables::of($q)
            ->addColumn('action', function ($user) {
                return '<a href="'. url('admin/notification/edit/'.base64_encode($user->id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return remove_notification('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    public function read($id) {
        $status['status'] = 1;
        DB::table($this->user_notification)->whereid($id)->update($status);
    }

    public function clearAll($param) {
        DB::table($this->user_notification)->whereuser_id($param)->delete();
    }

    public function clearOnes($param) {
        DB::table($this->user_notification)->whereid($param)->delete();
    }

    public function notification_master($id) {
        $q = DB::table($this->notification_master)->select('message','status')->whereid($id)->get();
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }

}
