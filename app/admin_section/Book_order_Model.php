<?php

namespace App\admin_section;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\common;

class Book_order_Model extends Model {

    //
    protected $book_order = 'book_jobs';
    protected $order_managment = 'order_managment';
    protected $profile = 'profile';
    protected $vehicle_type = 'truck_type';
    protected $users = 'users';
    protected $price_type ='price_type';
    
    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->common = new common();
        
    }

    public function book($data) {
        return DB::table($this->book_order)->insertGetId($data);
    }

    public function editOrder($id,$data){
        return DB::table($this->book_order)->whereid($id)->update($data);
    }
    
    public function getOrder($id) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*','ob.loading_location as pickup','ob.delivery_location as destination')->where('ob.id', $id)->get();
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }

    public function getCountTotalCompleted() {
        return DB::table($this->book_order . ' as ob')->select('ob.*', 'ob.id as order_booking_id', 'om.*', 'p.*', 'v.name as vehicle_type', 'b.name as body_type', 'g.name as goods_type_id')->join($this->order_managment . ' as om', 'om.trip_id', '=', 'ob.id')->join($this->profile . ' as p', 'p.user_id', '=', 'om.user_id')->leftJoin($this->vehicle_type . ' as v', 'v.id', '=', 'ob.vehicle_type_id')->leftJoin($this->body_type . ' as b', 'b.id', '=', 'p.body_type')->leftJoin($this->goods_type . ' as g', 'g.id', '=', 'ob.goods_type_id')->where('om.status', '4')->count();
    }

    public function getOrderList($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*', 'ob.id as order_booking_id', 'om.*', 'p.*', 'v.name as vehicle_type', 'b.name as body_type', 'g.name as goods_type_id')->join($this->order_managment . ' as om', 'om.trip_id', '=', 'ob.id')->join($this->profile . ' as p', 'p.user_id', '=', 'om.user_id')->leftJoin($this->vehicle_type . ' as v', 'v.id', '=', 'ob.vehicle_type_id')->leftJoin($this->body_type . ' as b', 'b.id', '=', 'p.body_type')->leftJoin($this->goods_type . ' as g', 'g.id', '=', 'ob.goods_type_id')->where('om.status', '4')->paginate($limit);
        if (count($q) > 0) {
            return $q;
        } else {
            return 0;
        }
    }
    public function getOrderListJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','ob.id as order_booking_id','p.name as price_type')->leftJoin($this->price_type.' as p','p.id','=','ob.price_type')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->where('ob.status', '4')->get();
         if($q):
            foreach($q as $v):
                if(@unserialize($v->no_of_truck_need)):
                  $no_of_truck = unserialize($v->no_of_truck_need);
                  $v->no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck);
              endif;
              if(@$v->trucking_start_date):
                  $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                  $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                  endif;
               if(@$v->created_at):
                    $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                    $v->created_at = $created_at->format('F d, Y h:m:s'); 
                  endif;
            endforeach;
        endif;       
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
                return '<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('order_booking_id', '{{$order_booking_id}}')
            ->make(true);
    }
    public function getOrderListAllJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*','ob.status as job_status',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','ob.id as order_booking_id','p.name as price_type')->leftJoin($this->price_type.' as p','p.id','=','ob.price_type')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->whereNotIn('ob.status',[2])->get();
        if($q):
            foreach($q as $v):
                if(@unserialize($v->no_of_truck_need)):
                  $no_of_truck = unserialize($v->no_of_truck_need);                  
                  $v->no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck);
                 endif;
                 if($v->trucking_start_date):
                  $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                  $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                  endif;
                  if(@$v->created_at):
                    $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                    $v->created_at = $created_at->format('F d, Y h:m:s'); 
                  endif;
            endforeach;
        endif;       
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
                $job_status = $user->job_status;
                if($job_status == 4):
                    $completed = 'selected';
                    elseif($job_status == 3):
                     $ongoing = 'selected';
                    elseif($job_status == 2):
                     $reject = 'selected';                      
                    elseif($job_status == 1):
                     $active = 'selected';                      
                    elseif($job_status == 0):
                     $newjob = 'selected';                      
                endif;
                return '<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-md btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-md btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</button>&nbsp<input type="hidden" id="job_id" value="'.$user->order_booking_id.'"/><select class="form-control" id="change_job_status" data-job-id="'.$user->order_booking_id.'" name="change_job_status">'
                        .  '<option value="4" '.@$completed .'>Completed</option>
                            <option value="0" '.@$newjob .'>New Job</option>                            
                            <option value="3" '. @$ongoing.'>Ongoing</option>
                            <option value="1" '. @$active.'>Active</option>
                            <option value="2" '.@$reject .'>Reject</option>'
                        . '</select>';
            })
            ->editColumn('order_booking_id', '{{$order_booking_id}}')
            ->make(true);
    }
    public function getOrderActiveListJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->where('ob.status', '1')->get();
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
                return '<button type="button" onclick="return order_shipping_complete('.$user->order_booking_id.')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-ok"></i> Mark as Complete</button>&nbsp;<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('order_booking_id', 'ID: {{$order_booking_id}}')
            ->make(true);
    }
    public function getOrderLiveListJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','p.name as price_type')->leftJoin($this->price_type.' as p','p.id','=','ob.price_type')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->whereIn('ob.status', ['1','3'])->get();
        if($q):
            foreach($q as $v):
                if(@unserialize($v->no_of_truck_need)):
                  $no_of_truck = unserialize($v->no_of_truck_need);
                  $v->no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck);
                 endif;
                 if(@$v->trucking_start_date):
                  $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                  $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                  endif;
                 if(@$v->created_at):
                    $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                    $v->created_at = $created_at->format('F d, Y h:m:s'); 
                  endif;
            endforeach;
        endif;  
        
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
                return '<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('order_booking_id', 'ID: {{$order_booking_id}}')
            ->make(true);
    }
    public function getOrderPendingListJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','p.name as price_type')->leftJoin($this->price_type.' as p','p.id','=','ob.price_type')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->where('ob.status', '0')->get();
         if($q):
            foreach($q as $v):
                if(@unserialize($v->no_of_truck_need)):
                  $no_of_truck = unserialize($v->no_of_truck_need);
                  $v->no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck);
                 endif;
                 if(@$v->trucking_start_date):
                  $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                  $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                  endif;
                 if(@$v->created_at):
                    $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                    $v->created_at = $created_at->format('F d, Y h:m:s'); 
                  endif;
            endforeach;
        endif;  
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
//                return '<button type="button" onclick="return order_shipping_allow('.$user->order_booking_id.')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-ok"></i> Allow</button>&nbsp;<button type="button" onclick="return order_shipping_reject('.$user->order_booking_id.')" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-remove"></i> Reject</button>&nbsp;<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
                return '<button type="button" onclick="return order_shipping_allow('.$user->order_booking_id.')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-ok"></i> Send Job</button>&nbsp;<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('order_booking_id', 'ID: {{$order_booking_id}}')
            ->make(true);
    }
    public function getOrderUpcomingListJSON($limit) {
        $q = DB::table($this->book_order . ' as ob')->select('ob.*',DB::raw('(select GROUP_CONCAT(name SEPARATOR ",") from truck_type where find_in_set(truck_type.id, ob.type_of_truck)) as type_of_truck'),'ob.loading_location as pickup','ob.delivery_location as destination','u.company_name', 'ob.id as order_booking_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','p.name as price_type')->leftJoin($this->price_type.' as p','p.id','=','ob.price_type')->leftJoin($this->users.' as u','u.id','=','ob.user_id')->where('ob.status', '2')->get();
        if($q):
            foreach($q as $v):
                if(@unserialize($v->no_of_truck_need)):
                  $no_of_truck = unserialize($v->no_of_truck_need);
                  $v->no_of_truck_need = \App\Http\Controllers\BuilTable_Special_truck::build_table($no_of_truck);
                 endif;
                 if(@$v->trucking_start_date):
                  $trucking_start_date = \Illuminate\Support\Carbon::parse($v->trucking_start_date);
                  $v->trucking_start_date = $trucking_start_date->format('F d, Y');
                  endif;
                  if(@$v->created_at):
                    $created_at = \Illuminate\Support\Carbon::parse($v->created_at);
                    $v->created_at = $created_at->format('F d, Y h:m:s'); 
                  endif;
            endforeach;
        endif;  
        return Datatables::of($q)
            ->addColumn('action', function ($user) {
                return '<button type="button" onclick="return order_shipping_allow('.$user->order_booking_id.')" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-ok"></i> Allow</button>&nbsp;<a href="'. url('admin/shipping/EditShipping/'.base64_encode($user->order_booking_id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return order_shipping_remove('.$user->order_booking_id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('order_booking_id', 'ID: {{$order_booking_id}}')
            ->make(true);
    }

}
