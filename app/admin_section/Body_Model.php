<?php

namespace App\admin_section;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class Body_Model extends Model
{
    protected $vehicle = 'body_type';
    public function addBody($data){
        return DB::table($this->vehicle)->insertGetId($data);
    }
    
    public function getByid($id){
        $q = DB::table($this->vehicle)->whereid($id)->get();
        if(count($q) > 0){
            return $q;
        }else{
            return 0;
        }
    }
    
    public function deleteRemove($id){
        return DB::table($this->vehicle)->whereid($id)->delete();
    }
    
    public function editBody($data,$id){
        return DB::table($this->vehicle)->whereid($id)->update($data);
    }
    
    public function getList(){
        return DB::table($this->vehicle)->get();
    }
    
    public function getListJSON(){         
        return Datatables::of(DB::table($this->vehicle)->get())
            ->addColumn('action', function ($user) {
                return '<a href="'. url('admin/vehicle_body/editBody/'.base64_encode($user->id)).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return vehicle_body_data_remove('.$user->id.')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }
}
