<?php

namespace App\admin_section;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReportModel extends Model
{
    protected $table = 'reports';    
    protected $users = 'users';
    public function getReport(){
        return DB::table($this->table)->orderbyDesc('id')->get();
    }
    public function generate_report($d){
        return DB::table($this->table)->insert($d);
    }
    public function generate_report_remove($id){
        return DB::table($this->table)->whereid($id)->delete();
    }
    public function getTruck_data(){
        return DB::table($this->users)->get();
    }
}
