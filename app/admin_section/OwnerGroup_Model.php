<?php

namespace App\admin_section;

use Illuminate\Database\Eloquent\Model;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class OwnerGroup_Model extends Model {

    protected $group_table = 'truck_owner_group';
    protected $users = 'users';

    public function get_group_list() {
        $q = DB::table($this->group_table)->get();
//        foreach ($q as $v) {
//            $v->users_ids = unserialize($v->users_ids);
//            if ($v->users_ids) {
//                $extra_Q = DB::table($this->users)->whereIn('id', $v->users_ids)->pluck('email');
//                $v->emails = $extra_Q;
//                $array_Emails = "";
//                foreach ($v->emails as $e){
//                    $array_Emails .= $e." ";
//                }
//                $v->emails = $array_Emails;
//                // $v->emails = json_encode($v->emails);
//            }
//        }
        return Datatables::of($q)
//                        ->addColumn('emails', function ($users) {                           
//                            if (@$users->emails) {
//                                return $users->emails;
//                            }
//                        })
                        ->addColumn('action', function ($user) {
                            return '<a href="' . url('admin/truck_owner_group/editGroup/' . base64_encode($user->id)) . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;<button type="button" onclick="return vehicle_body_data_remove(' . $user->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Remove</a>';
                        })
                        ->editColumn('id', 'ID: {{$id}}')
                        ->make(true);
    }
    
    public function deleteRemove($id){
        return DB::table($this->group_table)->whereid($id)->delete();
    }
    public function getGroup($id){
        return DB::table($this->group_table)->whereid($id)->first();
    }
    
    public function getUsersEmails($ids){
        if($ids):
            return DB::table($this->users.' as u')->select('id','email','company_name','first_name','last_name')->whereIn('id',$ids)->get();
        endif;        
    }
}
