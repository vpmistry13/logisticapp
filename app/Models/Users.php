<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    private $primary_table = 'users';
    private $profile = 'user_profile';
    
    public function insert_data($data){
        return DB::table($this->primary_table)->insertGetId($data);
    }
    
    public function update_user_profile($id,$data){
        return DB::table($this->primary_table)->whereid($id)->update($data);
    }
    public function update_user_profile_t($id,$data){
         $get = DB::table($this->profile)->whereuser_id($id)->get();       
        if (count($get) > 0) {
            $q = DB::table($this->profile)->whereuser_id($id)->update($data);
        } else {
            $data['user_id'] = $id;           
            $q = DB::table($this->profile)->insert($data);
        }
        return $q;
    }
    public function insert_profile($p){
        return DB::table($this->profile)->insert($p);
    }
    public function getUser($id)
    {
        $data = DB::table($this->primary_table.' as u')->leftJoin($this->profile.' as p', 'p.user_id','=','u.id')->where('u.id',$id)->get();
        if(count($data) > 0){
            return $data;
        }else{
           return 0;
        }       
    }
    public function getUserData($id)
    {
        return DB::table($this->primary_table.' as u')->select('u.id','u.first_name','u.last_name',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'u.email','u.company_name','u.mobile_number','u.user_token','u.user_type','u.created_at','u.updated_at','p.type','p.no_of_truck','p.cdl','p.avatar','p.address','p.office_number','p.user_gender','p.driving_licence_image','p.wq_image','p.insurance_image')->leftJoin($this->profile.' as p', 'p.user_id','=','u.id')->where('u.id',$id)->first();
          
    }
    
    public function auth_login($email)
    {
        $data = DB::table($this->primary_table.' as u')->select('u.id as user_id',DB::raw('CONCAT(u.first_name," ",u.last_name) as full_name'),'company_name','email','mobile_number','user_type','address','u.created_at','u.updated_at')->leftJoin($this->profile.' as p','p.user_id','=','u.id')->where('email',$email)->get();
        return $data;
    }
    public function update_token($d,$w){
        return DB::table($this->primary_table)->whereid($w)->update($d);
        
    }
    
    public function email_exit($d){
       return DB::table($this->primary_table)->whereemail($d)->count();
    }
}
