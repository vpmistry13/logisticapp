<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class common extends Model
{
    protected $material_type = 'material_type';
    protected $price_type = 'price_type';
    protected $truck_type = 'truck_type';
    public function get_material(){
        return DB::table($this->material_type)->wherestatus(1)->get();
    }
    public function get_price_type(){
        return DB::table($this->price_type)->wherestatus(1)->get();
    }
    public function get_truck_type(){
        return DB::table($this->truck_type)->wherestatus(1)->get();
    }
}
