<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('email-test', function(){
  
    $to = 'vishalmistri93@gmail.com';
    $name = 'vishalmistri93@gmail.com';
    $subject = 'vishalmistri93@gmail.com';
    $data['message_body'] = 'This is Final Test Message From Web Root.';
    dispatch_now(new App\Jobs\SendEmail($to, $name, $subject,$data,6));
//  return view('email.welcomeMailTruck',$data);
   // dd('done');
});
Route::get('sms-test', function(){
  
    $to = '+919099011665';
    $name = 'vishalmistri93@gmail.com';
    $subject = 'Vishal Test SMS';
    $data['name'] = 'Vishal';
    dispatch_now(new App\Jobs\SendSMS($to,$subject));
    //return "yes";
    
 // return view('email.welcomeMailTruck',$data);
   // dd('done');
});
Route::group(['as' => 'admin_access', 'namespace' => 'admin_access'], function () {

    Route::get('admin', function() {
        return redirect(url('admin/login'));
    })->name('admin');

    Route::get('admin/dashboard', 'Dashboard@index');
    Route::get('admin/login', function(Request $r) {
        $logged_id = $r->session()->get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
        if ($logged_id == '') {
            return view('admin_section.login.login');
        } else {
            return redirect(url('admin/dashboard'));
        }
    });

    //customer Actions   
    Route::get('admin/customer/list', 'User_Managment@customer_list');
    Route::get('admin/customer/list_json', 'User_Managment@customer_list_json');
    Route::get('admin/customer/addAccount', 'User_Managment@add_customer');
    Route::get('admin/customer/EditAccount/{id}', 'User_Managment@edit_customer');

    // driver Actions
    Route::get('admin/driver/list', 'User_Managment@driver_list');
    Route::get('admin/driver/list_json', 'User_Managment@driver_list_json');
    Route::get('admin/driver/addAccount', 'User_Managment@add_driver');
    Route::get('admin/driver/EditAccount/{id}', 'User_Managment@edit_driver');

    // ajax action
    Route::post('admin/Ajax_controller/addAccount', 'Ajax_controller@addAccount');
    Route::post('admin/Ajax_controller/add_to_group', 'Ajax_controller@add_to_group');
    Route::post('admin/Ajax_controller/edit_group_', 'Ajax_controller@edit_truck_owner_group');
    Route::get('admin/Ajax_controller/get_group_list', 'Ajax_controller@get_truck_owner_group');
    Route::post('admin/Ajax_controller/editAccount', 'Ajax_controller@editAccount');
    Route::post('admin/Ajax_controller/addVehicle', 'Ajax_controller@addVehicle');
    Route::post('admin/Ajax_controller/editVehicle', 'Ajax_controller@editVehicle');
    Route::post('admin/Ajax_controller/editGoodstype', 'Ajax_controller@editGoodtype');
    Route::post('admin/Ajax_controller/deleteVehicle/{id}', 'Ajax_controller@deleteVehicle');
    Route::get('admin/Ajax_controller/deleteUserRemove/{id}', 'Ajax_controller@deleteUserRemove');
    Route::get('admin/Ajax_controller/verifyTruckOwner/{id}', 'Ajax_controller@verifyUser');
    Route::post('admin/Ajax_controller/addBodytype', 'Ajax_controller@addBodytype');
    Route::post('admin/Ajax_controller/addGoodtype', 'Ajax_controller@addGoodtype');
    Route::post('admin/Ajax_controller/editBodytype', 'Ajax_controller@editBodytype');
    Route::post('admin/Ajax_controller/deleteBodytype/{id}', 'Ajax_controller@deleteBodytype');
    Route::post('admin/Ajax_controller/deleteGoodtype/{id}', 'Ajax_controller@deleteGoodtype');
    Route::post('admin/Ajax_controller/editProfile', 'Ajax_controller@editProfile');
    Route::post('admin/Ajax_controller/editOrderShipping', 'Ajax_controller@editOrderShipping');
    Route::get('admin/Ajax_controller/getCustomer_data','Ajax_controller@getCustomerData');
    Route::post('admin/Ajax_controller/editNotification','Ajax_controller@editMasterNotification');
    Route::post('admin/Ajax_controller/editSettings','Ajax_controller@editSettings');
    Route::get('admin/Ajax_controller/sales','Ajax_controller@sales_data_map');
    Route::get('admin/Ajax_controller/sales_revenue','Ajax_controller@sales_data_revenue_map');
    Route::post('admin/Ajax_controller/get_emails_by_id','Ajax_controller@get_emails_by_ids');
    Route::post('admin/Ajax_controller/save_group','Ajax_controller@save_truck_owner_group');
    Route::post('admin/Ajax_controller/remove_shipping_order/{id}','Ajax_controller@remove_order_shipping');
    Route::post('admin/Ajax_controller/shiping_status/{id}/{status}','Ajax_controller@shiping_status');
    Route::post('admin/Ajax_controller/shiping_status_admin/{id}/{status}','Ajax_controller@shiping_status_admin');
    Route::post('admin/Ajax_controller/documentVerify/{id}','Ajax_controller@documentVerify');
    Route::post('admin/Ajax_controller/send_message_bulk','Ajax_controller@send_message_bulk');
    // profile
    Route::get('admin/User_Profile/edit','User_Profile@index');
    
    //General Settings
    Route::get('admin/General_setting/list','General_settings@getSettings');
    Route::get('admin/setting/edit/{id}','General_settings@editSettings');
    
    // Manage vehicle action
    Route::get('admin/vehicle/addVehicle','Manage_Vehicle@addVehicle');
    Route::get('admin/vehicle_body/addBody','Manage_Body@addBody');
    Route::get('admin/vehicle/editVehicle/{id}','Manage_Vehicle@editVehicle');
    Route::get('admin/vehicle_body/editBody/{id}','Manage_Body@editBody');
    Route::get('admin/truck_owner_group/editGroup/{id}','TruckOwnerGroup@editGroup');
    Route::get('admin/vehicle_body/list_json','Manage_Body@list_json');
    Route::get('admin/vehicle/list','Manage_Vehicle@index');
    Route::get('admin/vehicle/list_json','Manage_Vehicle@list_json');
    Route::get('admin/vehicle_body/list','Manage_Body@index');
    Route::get('admin/goods_type/addGoodsType','Manage_Goods_Type@addGoodsType');
    Route::get('admin/goods_type/editGoodsType/{id}','Manage_Goods_Type@editGoodsType');
    Route::get('admin/goods_type/list','Manage_Goods_Type@index');
    Route::get('admin/goods_type/list_json','Manage_Goods_Type@goodsList');
    
    // Shipping actions
    Route::get('admin/shiping/list_all','Shipping_Managment@shipping_list_all');
    Route::get('admin/shiping/create','Shipping_Managment@shipping_create');
    Route::post('admin/shiping/create_job','Shipping_Managment@shipping_create_job')->name('create_job');
    Route::get('admin/shiping/list_all_json','Shipping_Managment@shipping_list_all_json');
    Route::get('admin/shiping/list_complete','Shipping_Managment@shipping_list_complete');
    Route::get('admin/shiping/list_active','Shipping_Managment@shipping_list_active');
    Route::get('admin/shiping/list_live','Shipping_Managment@shipping_list_live');
    Route::get('admin/shiping/list_pending','Shipping_Managment@shipping_list_pending');
    Route::get('admin/shiping/list_rejects','Shipping_Managment@shipping_list_reject');
    Route::get('admin/shiping/list_json','Shipping_Managment@shipping_list_json');
    Route::get('admin/shiping/list_active_json','Shipping_Managment@shipping_active_list_json');
    Route::get('admin/shiping/list_pending_json','Shipping_Managment@shipping_pending_list_json');
    Route::get('admin/shiping/list_live_json','Shipping_Managment@shipping_live_list_json');
    Route::get('admin/shiping/list_upcoming_json','Shipping_Managment@shipping_upcoming_list_json');
    Route::get('admin/shipping/EditShipping/{id}','Shipping_Managment@editShipping');
    
    // Notification Master
    Route::get('admin/notification/list','User_Notification@index');
    Route::get('admin/notification/edit/{id}','User_Notification@editNotification');
    Route::get('admin/notification/list_json','User_Notification@getListJSON');
    
    
    // Truck owner group actions
    Route::get('admin/truck_owner_group','TruckOwnerGroup@index');
    Route::get('admin/group/list_json','TruckOwnerGroup@get_group_list_json');
    
    // Report actions in admin
    Route::get('admin/report','ReportManagment@index');
    Route::post('admin/generate_report','ReportManagment@generate_report');
    Route::post('admin/generate_report_removed','ReportManagment@remove_report');
    
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin', function() {
        return redirect(url('admin/login'));
    })->name('admin');
