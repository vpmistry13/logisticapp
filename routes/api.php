<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api', 'namespace' => 'Api','middleware' => ['web']], function () {
    
    //basic operations
    Route::post('ws_login', 'Webservices@login');
    Route::post('ws_logout', 'Webservices@logout');
    Route::post('ws_signup', 'Webservices@signup');
    Route::get('ws_get_contact_us','Webservices@contact_us');
    Route::post('ws_edit_profile','Webservices@profile_edit');
    Route::post('ws_check_email_exit', 'Webservices@email_exit');
    Route::post('ws_get_profile', 'Webservices@profile_get');
    Route::get('ws_get_material_type', 'Webservices@material_type_get');
    Route::get('ws_get_price_type', 'Webservices@price_type_get');
    Route::get('ws_get_truck_type', 'Webservices@truck_type_get');
    Route::post('ws_make_shipment','Webservices@book_order');
    Route::post('ws_get_shipment_history','Webservices@book_order_history');
    Route::post('ws_accept_job','Webservices@accept_job');
    Route::post('ws_job_list','Webservices@truck_owner_dashboard_list');
    Route::post('ws_truck_owner_job_list_active','Webservices@truck_owner_job_list_active');
    Route::post('ws_truck_owner_job_list_complete','Webservices@truck_owner_job_list_complete');
    Route::post('ws_mark_complete_job','Webservices@mark_job_complete');
    Route::post('ws_dashboard','Webservices@job_dashboard');
    Route::get('ws_terms_condition','Webservices@terms_condition');
    Route::post('ws_job_list_truck_owner','Webservices@job_history_truck_owner');
    Route::post('ws_remove_job','Webservices@job_remove');
});

Route::post('ws_reset_password', 'Auth\ForgotPasswordController@getResetToken');